
CREATE SCHEMA alm
go



CREATE TABLE alm.almacenes
( 
	iAlmacenId           int IDENTITY ( 1,1 ) ,
	iAlmacenPrincipalId  int  NULL ,
	cAlmacenNombre       varchar(150)  NULL ,
	iEstadoId            int  NULL ,
	dtAlmacenFechaSis    datetime  NULL ,
	cAlmacenEquipoSis    varchar(50)  NULL ,
	cAlmacenIpSis        varchar(15)  NULL ,
	cAlmacenOpenUsr      varchar(1)  NULL ,
	cAlmacenMacNicSis    varchar(35)  NULL ,
	cAlmacenUsuarioSis   varchar(50)  NULL ,
	iAlmacenResponsablePersId int  NULL ,
	iDependFilId         int  NOT NULL 
)
go



ALTER TABLE alm.almacenes
	ADD  PRIMARY KEY  CLUSTERED (iAlmacenId ASC)
go



CREATE TABLE alm.bienes
( 
	iBienesId            int IDENTITY ( 1,1 ) ,
	cBienesNombre        varchar(200)  NULL ,
	SIGA_GRUPO_BIEN      varchar(2)  NULL ,
	SIGA_CLASE_BIEN      varchar(2)  NULL ,
	SIGA_FAMILIA_BIEN    varchar(4)  NULL ,
	SIGA_ITEM_BIEN       varchar(4)  NULL ,
	nBienesSaldo         numeric(19,7)  NULL 
)
go



ALTER TABLE alm.bienes
	ADD  PRIMARY KEY  CLUSTERED (iBienesId ASC)
go



CREATE TABLE alm.config
( 
	iConfigId            int IDENTITY ( 1,1 ) ,
	cConfigAnioActivo    varchar(4)  NULL ,
	dtConfigFechaCierreAnio datetime  NULL ,
	bConfigAnioCerrado   bit  NULL ,
	bConfigEstado        bit  NULL ,
	cConfigNombreEntidad varchar(200)  NULL ,
	cConfigRUCEntidad    varchar(11)  NULL ,
	cConfigLogoEntidad   varchar(100)  NULL ,
	cConfigVersion       varchar(10)  NULL ,
	cConfigCopyRight     varchar(max)  NULL ,
	cConfigCodigoEntidad varchar(8)  NULL 
)
go



ALTER TABLE alm.config
	ADD  PRIMARY KEY  CLUSTERED (iConfigId ASC)
go



CREATE TABLE alm.detalles_notas_pedidos
( 
	iDetNotaPedidoId     int IDENTITY ( 1,1 ) ,
	iNotaPedidoId        int  NULL ,
	SIGA_SEC_ORDEN       numeric(4)  NULL ,
	SIGA_SEC_ITEM        numeric(4)  NULL ,
	dtDetNotaPedidoFechaSis datetime  NULL ,
	cDetNotaPedidoEquipoSis varchar(50)  NULL ,
	cDetNotaPedidoIpSis  varchar(15)  NULL ,
	cDetNotaPedidoOpenUsr varchar(1)  NULL ,
	cDetNotaPedidoMacNicSis varchar(35)  NULL ,
	cDetNotaPedidoUsuarioSis varchar(50)  NULL ,
	iTiposUsoId          int  NULL ,
	iBienesId            int  NULL 
)
go



ALTER TABLE alm.detalles_notas_pedidos
	ADD  PRIMARY KEY  CLUSTERED (iDetNotaPedidoId ASC)
go



CREATE TABLE alm.estados
( 
	iEstadoId            int IDENTITY ( 1,1 ) ,
	cEstadoNombre        varchar(50)  NULL ,
	dtEstadoFechaSis     datetime  NULL ,
	cEstadoEquipoSis     varchar(50)  NULL ,
	cEstadoIpSis         varchar(15)  NULL ,
	cEstadoOpenUsr       varchar(1)  NULL ,
	cEstadoMacNicSis     varchar(35)  NULL ,
	cEstadoUsuarioSis    varchar(50)  NULL 
)
go



ALTER TABLE alm.estados
	ADD  PRIMARY KEY  CLUSTERED (iEstadoId ASC)
go



CREATE TABLE alm.estados_almacen
( 
	iEstadoAlmacenId     int IDENTITY ( 1,1 ) ,
	dtEstadoAlmacenFecha datetime  NULL ,
	cEstadoAlmacenMotivo varchar(max)  NULL ,
	iEstadoId            int  NULL ,
	iAlmacenId           int  NULL ,
	dtEstadoAlmacenFechaSis datetime  NULL ,
	cEstadoAlmacenEquipoSis varchar(50)  NULL ,
	cEstadoAlmacenIpSis  varchar(15)  NULL ,
	cEstadoAlmacenOpenUsr varchar(1)  NULL ,
	cEstadoAlmacenMacNicSis varchar(35)  NULL ,
	cEstadoAlmacenUsuarioSis varchar(50)  NULL 
)
go



ALTER TABLE alm.estados_almacen
	ADD  PRIMARY KEY  CLUSTERED (iEstadoAlmacenId ASC)
go



CREATE TABLE alm.estados_nota
( 
	iEstadoNotaId        int IDENTITY ( 1,1 ) ,
	cEstadoNotaNombre    varchar(50)  NULL ,
	dtEstadoNotaFechaSis datetime  NULL ,
	cEstadoNotaEquipoSis varchar(50)  NULL ,
	cEstadoNotaIpSis     varchar(15)  NULL ,
	cEstadoNotaOpenUsr   varchar(1)  NULL ,
	cEstadoNotaMacNicSis varchar(35)  NULL ,
	cEstadoNotaUsuarioSis varchar(50)  NULL 
)
go



ALTER TABLE alm.estados_nota
	ADD  PRIMARY KEY  CLUSTERED (iEstadoNotaId ASC)
go



CREATE TABLE alm.lugares
( 
	iLugaresId           int IDENTITY ( 1,1 ) ,
	cLugaresNombre       varchar(100)  NULL ,
	cLugaresDescripcion  varchar(max)  NULL ,
	iAlmacenId           int  NULL 
)
go



ALTER TABLE alm.lugares
	ADD  PRIMARY KEY  CLUSTERED (iLugaresId ASC)
go



CREATE TABLE alm.movimientos
( 
	iMovimientosId       int IDENTITY ( 1,1 ) ,
	nDetNotaPedidoCantidad numeric(19,7)  NULL ,
	nKardexEntrada       numeric(19,7)  NULL ,
	nKardexSalida        numeric(19,7)  NULL ,
	nKardexSaldo         numeric(19,7)  NULL ,
	iDetNotaPedidoId     int  NULL ,
	dtMovimientosFecha   datetime  NULL ,
	idLugaresIdOrigen    int  NULL ,
	iLugaresIdDestino    int  NULL ,
	iTipoMovimientoId    int  NULL ,
	iNotaPedidoSolicitantePersId int  NOT NULL ,
	iNotaPedidoChoferPersId int  NULL ,
	cValeCombPersBrevete varchar(15)  NULL ,
	cValeCombLugarDestino varchar(50)  NULL ,
	cValeCombMotivoServicio varchar(max)  NULL ,
	cValeCombPlacaVeh    varchar(15)  NULL ,
	iTipoVehId           int  NULL 
)
go



ALTER TABLE alm.movimientos
	ADD  PRIMARY KEY  CLUSTERED (iMovimientosId ASC)
go



CREATE TABLE alm.notas_pedidos
( 
	iNotaPedidoId        int IDENTITY ( 1,1 ) ,
	cNotaPedidoNumero    varchar(15)  NULL ,
	dtNotaPedidoFecha    datetime  NULL ,
	cNotaPedidoGlosa     varchar(max)  NULL ,
	iEstadoNotaId        int  NULL ,
	SIGA_ANO_EJE         numeric(4)  NULL ,
	SIGA_SEC_EJEC        numeric(6)  NULL ,
	cNotaPedidoOBS       varchar(max)  NULL ,
	cKardexDocumento     varchar(150)  NULL ,
	iTipoDocId           int  NULL ,
	dtNotaPedidoFechaSis datetime  NULL ,
	cNotaPedidoEquipoSis varchar(50)  NULL ,
	cNotaPedidoIpSis     varchar(15)  NULL ,
	cNotaPedidoOpenUsr   varchar(1)  NULL ,
	cNotaPedidoMacNicSis varchar(35)  NULL ,
	cNotaPedidoUsuarioSis varchar(50)  NULL ,
	cNotaPedidoAlmacenOrigenOtro varchar(200)  NULL ,
	cNotaPedidoAlmacenDestinoOtro varchar(200)  NULL ,
	SIGA_NRO_ORDEN       numeric(7)  NULL ,
	SIGA_TIPO_BIEN       varchar(1)  NULL ,
	SIGA_TIPO_PPTO       varchar(2)  NULL ,
	SIGA_TIPO_MOVIMTO    varchar(1)  NULL ,
	SIGA_TIPO_TRANSAC    varchar(2)  NULL ,
	iLugarIdOrigen       int  NULL ,
	iLugarIdDestino      int  NULL 
)
go



ALTER TABLE alm.notas_pedidos
	ADD  PRIMARY KEY  CLUSTERED (iNotaPedidoId ASC)
go



CREATE TABLE alm.OC_logistica
( 
	ANO_EJE              numeric(4)  NULL ,
	SEC_EJEC             numeric(6)  NULL ,
	NRO_ORDEN            numeric(7)  NULL ,
	TIPO_BIEN            varchar(1)  NULL ,
	TIPO_PPTO            numeric(2)  NULL ,
	SEC_ORDEN            numeric(2)  NULL ,
	SEC_ITEM             numeric(4)  NULL ,
	dtFechaNotificacion  datetime  NULL ,
	iPlazoEntrega        numeric(5)  NULL ,
	iOCLogisticaId       int IDENTITY ( 1,1 ) 
)
go



ALTER TABLE alm.OC_logistica
	ADD  PRIMARY KEY  CLUSTERED (iOCLogisticaId ASC)
go



CREATE TABLE alm.tipo_movimientos
( 
	iTipoMovimientoId    int IDENTITY ( 1,1 ) ,
	cTipoMovimientoNombre varchar(50)  NULL ,
	cTipoMovimientoUsuarioSis varchar(50)  NULL ,
	dtTipoMovimientoFechaSis datetime  NULL ,
	cTipoMovimientoEquipoSis varchar(50)  NULL ,
	cTipoMovimientoIpSis varchar(15)  NULL ,
	cTipoMovimientoOpenUsr varchar(1)  NULL ,
	cTipoMovimientoMacNicSis varchar(35)  NULL 
)
go



ALTER TABLE alm.tipo_movimientos
	ADD  PRIMARY KEY  CLUSTERED (iTipoMovimientoId ASC)
go



CREATE TABLE alm.tipos_documentos
( 
	iTipoDocId           int  NOT NULL ,
	cTipoDocUsuarioSis   varchar(50)  NULL ,
	dtTipoDocFechaSis    datetime  NULL ,
	cTipoDocEquipoSis    varchar(50)  NULL ,
	cTipoDocIpSis        varchar(15)  NULL ,
	cTipoDocOpenUsr      varchar(1)  NULL ,
	cTipoDocMacNicSis    varchar(35)  NULL 
)
go



ALTER TABLE alm.tipos_documentos
	ADD  PRIMARY KEY  CLUSTERED (iTipoDocId ASC)
go



CREATE TABLE alm.tipos_uso
( 
	iTiposUsoId          int IDENTITY ( 1,1 ) ,
	cTiposUsoNombre      varchar(50)  NULL 
)
go



ALTER TABLE alm.tipos_uso
	ADD  PRIMARY KEY  CLUSTERED (iTiposUsoId ASC)
go



CREATE TABLE alm.tipos_vehiculos
( 
	iTipoVehId           int IDENTITY ( 1,1 ) ,
	cTipoVehNombre       varchar(50)  NULL ,
	dtTipoVehFechaSis    datetime  NULL ,
	cTipoVehEquipoSis    varchar(50)  NULL ,
	cTipoVehIpSis        varchar(15)  NULL ,
	cTipoVehOpenUsr      varchar(1)  NULL ,
	cTipoVehMacNicSis    varchar(35)  NULL ,
	cTipoVehUsuarioSis   varchar(50)  NULL 
)
go



ALTER TABLE alm.tipos_vehiculos
	ADD  PRIMARY KEY  CLUSTERED (iTipoVehId ASC)
go




ALTER TABLE alm.almacenes
	ADD  FOREIGN KEY (iEstadoId) REFERENCES alm.estados(iEstadoId)
		ON DELETE NO ACTION
		ON UPDATE NO ACTION
go




ALTER TABLE alm.almacenes
	ADD  FOREIGN KEY (iAlmacenPrincipalId) REFERENCES alm.almacenes(iAlmacenId)
		ON DELETE NO ACTION
		ON UPDATE NO ACTION
go




ALTER TABLE alm.detalles_notas_pedidos
	ADD  FOREIGN KEY (iNotaPedidoId) REFERENCES alm.notas_pedidos(iNotaPedidoId)
		ON DELETE NO ACTION
		ON UPDATE NO ACTION
go




ALTER TABLE alm.detalles_notas_pedidos
	ADD  FOREIGN KEY (iTiposUsoId) REFERENCES alm.tipos_uso(iTiposUsoId)
		ON DELETE NO ACTION
		ON UPDATE NO ACTION
go




ALTER TABLE alm.detalles_notas_pedidos
	ADD  FOREIGN KEY (iBienesId) REFERENCES alm.bienes(iBienesId)
		ON DELETE NO ACTION
		ON UPDATE NO ACTION
go




ALTER TABLE alm.estados_almacen
	ADD  FOREIGN KEY (iEstadoId) REFERENCES alm.estados(iEstadoId)
		ON DELETE NO ACTION
		ON UPDATE NO ACTION
go




ALTER TABLE alm.estados_almacen
	ADD  FOREIGN KEY (iAlmacenId) REFERENCES alm.almacenes(iAlmacenId)
		ON DELETE NO ACTION
		ON UPDATE NO ACTION
go




ALTER TABLE alm.lugares
	ADD  FOREIGN KEY (iAlmacenId) REFERENCES alm.almacenes(iAlmacenId)
		ON DELETE NO ACTION
		ON UPDATE NO ACTION
go




ALTER TABLE alm.movimientos
	ADD  FOREIGN KEY (iDetNotaPedidoId) REFERENCES alm.detalles_notas_pedidos(iDetNotaPedidoId)
		ON DELETE NO ACTION
		ON UPDATE NO ACTION
go




ALTER TABLE alm.movimientos
	ADD  FOREIGN KEY (idLugaresIdOrigen) REFERENCES alm.lugares(iLugaresId)
		ON DELETE NO ACTION
		ON UPDATE NO ACTION
go




ALTER TABLE alm.movimientos
	ADD  FOREIGN KEY (iLugaresIdDestino) REFERENCES alm.lugares(iLugaresId)
		ON DELETE NO ACTION
		ON UPDATE NO ACTION
go




ALTER TABLE alm.movimientos
	ADD  FOREIGN KEY (iTipoMovimientoId) REFERENCES alm.tipo_movimientos(iTipoMovimientoId)
		ON DELETE NO ACTION
		ON UPDATE NO ACTION
go




ALTER TABLE alm.movimientos
	ADD  FOREIGN KEY (iTipoVehId) REFERENCES alm.tipos_vehiculos(iTipoVehId)
		ON DELETE NO ACTION
		ON UPDATE NO ACTION
go




ALTER TABLE alm.notas_pedidos
	ADD  FOREIGN KEY (iEstadoNotaId) REFERENCES alm.estados_nota(iEstadoNotaId)
		ON DELETE NO ACTION
		ON UPDATE NO ACTION
go




ALTER TABLE alm.notas_pedidos
	ADD  FOREIGN KEY (iTipoDocId) REFERENCES alm.tipos_documentos(iTipoDocId)
		ON DELETE NO ACTION
		ON UPDATE NO ACTION
go




ALTER TABLE alm.notas_pedidos
	ADD  FOREIGN KEY (iLugarIdOrigen) REFERENCES alm.lugares(iLugaresId)
		ON DELETE NO ACTION
		ON UPDATE NO ACTION
go




ALTER TABLE alm.notas_pedidos
	ADD  FOREIGN KEY (iLugarIdDestino) REFERENCES alm.lugares(iLugaresId)
		ON DELETE NO ACTION
		ON UPDATE NO ACTION
go


