
DROP TABLE alm.estados_almacen
go



DROP TABLE alm.config
go



DROP TABLE alm.OC_logistica
go



DROP TABLE alm.vales_combustible
go



DROP TABLE alm.movimientos
go



DROP TABLE alm.SIGA_Detalles_Movimientos_Almacen
go



DROP TABLE alm.estados_nota
go



DROP TABLE alm.tipo_movimientos
go



DROP TABLE alm.lugares
go



DROP TABLE alm.almacenes
go



DROP TABLE alm.estados
go



DROP TABLE alm.tipos_vehiculos
go



CREATE TABLE alm.almacenes
( 
	iAlmacenId           int IDENTITY ( 1,1 ) ,
	iAlmacenPrincipalId  int  NULL ,
	cAlmacenNombre       varchar(500)  NULL ,
	iEstadoId            int  NULL ,
	iAlmacenResponsablePersId int  NULL ,
	iDependFilId         int  NOT NULL ,
	SIGA_ALMACEN         varchar(3)  NULL ,
	cAlmacenesUsuarioSis varchar(50)  NULL ,
	dtAlmacenesFechaSis  datetime  NULL ,
	cAlmacenesEquipoSis  varchar(50)  NULL ,
	cAlmacenesIpSis      varchar(15)  NULL ,
	cAlmacenesOpenUsr    varchar(1)  NULL ,
	cAlmacenesMacNicSis  varchar(35)  NULL 
)
go



ALTER TABLE alm.almacenes
	ADD  PRIMARY KEY  CLUSTERED (iAlmacenId ASC)
go



CREATE TABLE alm.config
( 
	iConfigId            int IDENTITY ( 1,1 ) ,
	cConfigAnioActivo    varchar(4)  NULL ,
	dtConfigFechaCierreAnio datetime  NULL ,
	bConfigAnioCerrado   bit  NULL ,
	bConfigEstado        bit  NULL ,
	cConfigNombreEntidad varchar(200)  NULL ,
	cConfigRUCEntidad    varchar(11)  NULL ,
	cConfigLogoEntidad   varchar(100)  NULL ,
	cConfigVersion       varchar(10)  NULL ,
	cConfigCopyRight     varchar(max)  NULL ,
	cConfigCodigoEntidad varchar(8)  NULL 
)
go



ALTER TABLE alm.config
	ADD  PRIMARY KEY  CLUSTERED (iConfigId ASC)
go



CREATE TABLE alm.estados
( 
	iEstadoId            int IDENTITY ( 1,1 ) ,
	cEstadoNombre        varchar(50)  NULL ,
	dtEstadoFechaSis     datetime  NULL ,
	cEstadoEquipoSis     varchar(50)  NULL ,
	cEstadoIpSis         varchar(15)  NULL ,
	cEstadoOpenUsr       varchar(1)  NULL ,
	cEstadoMacNicSis     varchar(35)  NULL ,
	cEstadoUsuarioSis    varchar(50)  NULL 
)
go



ALTER TABLE alm.estados
	ADD  PRIMARY KEY  CLUSTERED (iEstadoId ASC)
go



CREATE TABLE alm.estados_almacen
( 
	iEstadoAlmacenId     int IDENTITY ( 1,1 ) ,
	dtEstadoAlmacenFecha datetime  NULL ,
	cEstadoAlmacenMotivo varchar(max)  NULL ,
	iEstadoId            int  NULL ,
	iAlmacenId           int  NULL ,
	dtEstadoAlmacenFechaSis datetime  NULL ,
	cEstadoAlmacenEquipoSis varchar(50)  NULL ,
	cEstadoAlmacenIpSis  varchar(15)  NULL ,
	cEstadoAlmacenOpenUsr varchar(1)  NULL ,
	cEstadoAlmacenMacNicSis varchar(35)  NULL ,
	cEstadoAlmacenUsuarioSis varchar(50)  NULL 
)
go



ALTER TABLE alm.estados_almacen
	ADD  PRIMARY KEY  CLUSTERED (iEstadoAlmacenId ASC)
go



CREATE TABLE alm.estados_nota
( 
	iEstadoNotaId        int IDENTITY ( 1,1 ) ,
	cEstadoNotaNombre    varchar(50)  NULL ,
	dtEstadoNotaFechaSis datetime  NULL ,
	cEstadoNotaEquipoSis varchar(50)  NULL ,
	cEstadoNotaIpSis     varchar(15)  NULL ,
	cEstadoNotaOpenUsr   varchar(1)  NULL ,
	cEstadoNotaMacNicSis varchar(35)  NULL ,
	cEstadoNotaUsuarioSis varchar(50)  NULL 
)
go



ALTER TABLE alm.estados_nota
	ADD  PRIMARY KEY  CLUSTERED (iEstadoNotaId ASC)
go



CREATE TABLE alm.lugares
( 
	iLugaresId           int IDENTITY ( 1,1 ) ,
	cLugaresNombre       varchar(500)  NULL ,
	cLugaresDescripcion  varchar(max)  NULL ,
	iAlmacenId           int  NULL ,
	cLugaresDireccion    varchar(100)  NULL ,
	cLugaresUsuarioSis   varchar(50)  NULL ,
	dtLugaresFechaSis    datetime  NULL ,
	cLugaresEquipoSis    varchar(50)  NULL ,
	cLugaresIpSis        varchar(15)  NULL ,
	cLugaresOpenUsr      varchar(1)  NULL ,
	cLugaresMacNicSis    varchar(35)  NULL ,
	iAlmacenResponsablePersId int  NULL 
)
go



ALTER TABLE alm.lugares
	ADD  PRIMARY KEY  CLUSTERED (iLugaresId ASC)
go



CREATE TABLE alm.movimientos
( 
	iMovimientosId       int IDENTITY ( 1,1 ) ,
	nMovimientosCantidad numeric(19,7)  NULL ,
	nKardexEntrada       numeric(19,7)  NULL ,
	nKardexSalida        numeric(19,7)  NULL ,
	nKardexSaldo         numeric(19,7)  NULL ,
	dtMovimientosFecha   datetime  NULL ,
	iLugaresIdOrigen     int  NULL ,
	iLugaresIdDestino    int  NULL ,
	iTipoMovimientoId    int  NULL ,
	iMovimientosSolicitantePersId int  NOT NULL ,
	iEstadoNotaId        int  NULL ,
	cMovimientosObservacion varchar(max)  NULL ,
	cMovimimentosGlosa   varchar(max)  NULL ,
	iMovimientosRecepcionaPersId int  NULL ,
	dtMovimientosFechaRepcion datetime  NULL ,
	cMovimientosUsuarioSis varchar(50)  NULL ,
	dtMovimientosFechaSis datetime  NULL ,
	cMovimientosEquipoSis varchar(50)  NULL ,
	cMovimientosIpSis    varchar(15)  NULL ,
	cMovimientosOpenUsr  varchar(1)  NULL ,
	cMovimientosMacNicSis varchar(35)  NULL ,
	iSIGADetalleId       int  NULL 
)
go



ALTER TABLE alm.movimientos
	ADD  PRIMARY KEY  CLUSTERED (iMovimientosId ASC)
go



CREATE TABLE alm.OC_logistica
( 
	ANO_EJE              numeric(4)  NULL ,
	SEC_EJEC             numeric(6)  NULL ,
	NRO_ORDEN            numeric(7)  NULL ,
	TIPO_BIEN            varchar(1)  NULL ,
	TIPO_PPTO            numeric(2)  NULL ,
	SEC_ORDEN            numeric(2)  NULL ,
	SEC_ITEM             numeric(4)  NULL ,
	dtFechaNotificacion  datetime  NULL ,
	iPlazoEntrega        numeric(5)  NULL ,
	iOCLogisticaId       int IDENTITY ( 1,1 ) 
)
go



ALTER TABLE alm.OC_logistica
	ADD  PRIMARY KEY  CLUSTERED (iOCLogisticaId ASC)
go



CREATE TABLE alm.SIGA_Detalles_Movimientos_Almacen
( 
	iSIGADetalleId       int IDENTITY ( 1,1 ) ,
	SIGA_NRO_ORDEN_OC    numeric(7)  NULL ,
	cOCEstado            varchar(1)  NULL ,
	cOCObservacion       varchar(max)  NULL ,
	dtOCFechaNotificacion datetime  NULL ,
	nOCPlazoEntrega      numeric(5)  NULL ,
	SIGA_ANO_EJE_PEDIDO  numeric(4)  NULL ,
	SIGA_TIPO_BIEN_PEDIDO varchar(1)  NULL ,
	SIGA_TIPO_PEDIDO     char(1)  NULL ,
	SIGA_NRO_PEDIDO      char(5)  NULL ,
	SIGA_ANO_EJE_OC      numeric(4)  NULL ,
	SIGA_SEC_EJEC        numeric(6)  NULL ,
	SIGA_TIPO_BIEN_OC    varchar(1)  NULL ,
	SIGA_TIPO_PPTO_OC    numeric(2)  NULL ,
	cSIGADetalleUsuarioSis varchar(50)  NULL ,
	dtSIGADetalleFechaSis datetime  NULL ,
	cSIGADetalleEquipoSis varchar(50)  NULL ,
	cSIGADetalleIpSis    varchar(15)  NULL ,
	cSIGADetalleOpenUsr  varchar(1)  NULL ,
	cSIGADetalleMacNicSis varchar(35)  NULL ,
	SIGA_ALMACEN         varchar(3)  NULL ,
	SIGA_SEC_ALMACEN     varchar(3)  NULL ,
	SIGA_TIPO_MOVIMTO    varchar(1)  NULL ,
	SIGA_TIPO_TRANSAC    numeric(2)  NULL ,
	SIGA_TIPO_PPTO       numeric(1)  NULL ,
	SIGA_NRO_MOVIMTO     numeric(5)  NULL ,
	SIGA_SECUENCIA       numeric(2)  NULL ,
	SIGA_SEC_DETALLE     numeric(5)  NULL ,
	SIGA_SEC_LOTE        numeric(2)  NULL ,
	SIGA_ANO_EJE_MOVIMIENTO numeric(4)  NULL ,
	SIGA_EMPLEADO_PEDIDO varchar(20)  NULL ,
	iAlmacenId           int  NULL 
)
go



ALTER TABLE alm.SIGA_Detalles_Movimientos_Almacen
	ADD  PRIMARY KEY  CLUSTERED (iSIGADetalleId ASC)
go



CREATE TABLE alm.tipo_movimientos
( 
	iTipoMovimientoId    int IDENTITY ( 1,1 ) ,
	cTipoMovimientoNombre varchar(50)  NULL ,
	cTipoMovimientoUsuarioSis varchar(50)  NULL ,
	dtTipoMovimientoFechaSis datetime  NULL ,
	cTipoMovimientoEquipoSis varchar(50)  NULL ,
	cTipoMovimientoIpSis varchar(15)  NULL ,
	cTipoMovimientoOpenUsr varchar(1)  NULL ,
	cTipoMovimientoMacNicSis varchar(35)  NULL ,
	cTipoMovimientoTransaccion int  NULL ,
	cTipoMovimientoAbreviacion varchar(20)  NULL ,
	cTipoMovimientoTipoId char(1)  NULL 
)
go



ALTER TABLE alm.tipo_movimientos
	ADD  PRIMARY KEY  CLUSTERED (iTipoMovimientoId ASC)
go



CREATE TABLE alm.tipos_vehiculos
( 
	iTipoVehId           int IDENTITY ( 1,1 ) ,
	cTipoVehNombre       varchar(50)  NULL ,
	dtTipoVehFechaSis    datetime  NULL ,
	cTipoVehEquipoSis    varchar(50)  NULL ,
	cTipoVehIpSis        varchar(15)  NULL ,
	cTipoVehOpenUsr      varchar(1)  NULL ,
	cTipoVehMacNicSis    varchar(35)  NULL ,
	cTipoVehUsuarioSis   varchar(50)  NULL 
)
go



ALTER TABLE alm.tipos_vehiculos
	ADD  PRIMARY KEY  CLUSTERED (iTipoVehId ASC)
go



CREATE TABLE alm.vales_combustible
( 
	iValesId             int IDENTITY ( 1,1 ) ,
	cValesNumero         varchar(10)  NULL ,
	dtValesFecha         datetime  NULL ,
	cValeCombPersBrevete varchar(15)  NULL ,
	cValeCombLugarDestino varchar(50)  NULL ,
	cValeCombMotivoServicio varchar(max)  NULL ,
	cValeCombPlacaVeh    varchar(15)  NULL ,
	iTipoVehId           int  NULL ,
	iValesAutorizacionPersId int  NULL ,
	dtValesAutorizacion  datetime  NULL ,
	iMovimientosId       int  NULL ,
	iValesChofer         int  NULL ,
	cValesUsuarioSis     varchar(50)  NULL ,
	dtValesFechaSis      datetime  NULL ,
	cValesEquipoSis      varchar(50)  NULL ,
	cValesIpSis          varchar(15)  NULL ,
	cValesOpenUsr        varchar(1)  NULL ,
	cValesMacNicSis      varchar(35)  NULL 
)
go



ALTER TABLE alm.vales_combustible
	ADD  PRIMARY KEY  CLUSTERED (iValesId ASC)
go




ALTER TABLE alm.almacenes
	ADD  FOREIGN KEY (iEstadoId) REFERENCES alm.estados(iEstadoId)
		ON DELETE NO ACTION
		ON UPDATE NO ACTION
go




ALTER TABLE alm.almacenes
	ADD  FOREIGN KEY (iAlmacenPrincipalId) REFERENCES alm.almacenes(iAlmacenId)
		ON DELETE NO ACTION
		ON UPDATE NO ACTION
go




ALTER TABLE alm.estados_almacen
	ADD  FOREIGN KEY (iEstadoId) REFERENCES alm.estados(iEstadoId)
		ON DELETE NO ACTION
		ON UPDATE NO ACTION
go




ALTER TABLE alm.estados_almacen
	ADD  FOREIGN KEY (iAlmacenId) REFERENCES alm.almacenes(iAlmacenId)
		ON DELETE NO ACTION
		ON UPDATE NO ACTION
go




ALTER TABLE alm.lugares
	ADD  FOREIGN KEY (iAlmacenId) REFERENCES alm.almacenes(iAlmacenId)
		ON DELETE NO ACTION
		ON UPDATE NO ACTION
go




ALTER TABLE alm.movimientos
	ADD  FOREIGN KEY (iLugaresIdOrigen) REFERENCES alm.lugares(iLugaresId)
		ON DELETE NO ACTION
		ON UPDATE NO ACTION
go




ALTER TABLE alm.movimientos
	ADD  FOREIGN KEY (iLugaresIdDestino) REFERENCES alm.lugares(iLugaresId)
		ON DELETE NO ACTION
		ON UPDATE NO ACTION
go




ALTER TABLE alm.movimientos
	ADD  FOREIGN KEY (iTipoMovimientoId) REFERENCES alm.tipo_movimientos(iTipoMovimientoId)
		ON DELETE NO ACTION
		ON UPDATE NO ACTION
go




ALTER TABLE alm.movimientos
	ADD  FOREIGN KEY (iEstadoNotaId) REFERENCES alm.estados_nota(iEstadoNotaId)
		ON DELETE NO ACTION
		ON UPDATE NO ACTION
go




ALTER TABLE alm.movimientos
	ADD  FOREIGN KEY (iSIGADetalleId) REFERENCES alm.SIGA_Detalles_Movimientos_Almacen(iSIGADetalleId)
		ON DELETE NO ACTION
		ON UPDATE NO ACTION
go




ALTER TABLE alm.SIGA_Detalles_Movimientos_Almacen
	ADD  FOREIGN KEY (iAlmacenId) REFERENCES alm.almacenes(iAlmacenId)
		ON DELETE NO ACTION
		ON UPDATE NO ACTION
go




ALTER TABLE alm.vales_combustible
	ADD  FOREIGN KEY (iTipoVehId) REFERENCES alm.tipos_vehiculos(iTipoVehId)
		ON DELETE NO ACTION
		ON UPDATE NO ACTION
go




ALTER TABLE alm.vales_combustible
	ADD  FOREIGN KEY (iMovimientosId) REFERENCES alm.movimientos(iMovimientosId)
		ON DELETE NO ACTION
		ON UPDATE NO ACTION
go


