INSERT INTO alm.estados (cEstadoNombre) VALUES ('ACTIVO')
INSERT INTO alm.estados (cEstadoNombre) VALUES ('SUSPENDIDO')
INSERT INTO alm.estados (cEstadoNombre) VALUES ('BAJA')
---------
INSERT INTO alm.almacenes 
(cAlmacenNombre, iEstadoId,iDependFilId,SIGA_ALMACEN,iAlmacenResponsablePersId) 
VALUES ('PRINCIPAL',1,14,'000',6217)
----------------
INSERT INTO alm.almacenes 
(cAlmacenNombre, iEstadoId,iDependFilId,SIGA_ALMACEN,iAlmacenResponsablePersId,iAlmacenPrincipalId) 
select cDepenNombre,1,iDepenId,NULL,NULL,1 from grl.dependencias where iDepenId <> 14
-------------
INSERT INTO alm.lugares (iAlmacenId,cLugaresNombre)
select iAlmacenId, cAlmacenNombre from alm.almacenes
---------------
INSERT INTO alm.SIGA_Detalles_Movimientos_Almacen 
(
SIGA_SEC_EJEC,
SIGA_ANO_EJE_PEDIDO,
SIGA_TIPO_BIEN_PEDIDO,
SIGA_TIPO_PEDIDO,
SIGA_NRO_PEDIDO,
nOCPlazoEntrega,
dtOCFechaNotificacion,
SIGA_ANO_EJE_OC,
SIGA_TIPO_BIEN_OC,
SIGA_TIPO_PPTO_OC,
SIGA_NRO_ORDEN_OC,
------------
SIGA_ANO_EJE_MOVIMIENTO,
SIGA_ALMACEN,
SIGA_SEC_ALMACEN,
SIGA_TIPO_MOVIMTO,
SIGA_TIPO_TRANSAC,
SIGA_TIPO_PPTO,
SIGA_NRO_MOVIMTO,
SIGA_SECUENCIA,
SIGA_SEC_DETALLE,
SIGA_SEC_LOTE,
SIGA_EMPLEADO_PEDIDO
)
SELECT 
	-- persona.iPersonasId,
	PED.SEC_EJEC,
	PED.ANO_EJE,
	PED.TIPO_BIEN,
	PED.TIPO_PEDIDO,
	SUBSTRING(oraq.DOCUM_REFERENCIA,8,5) as NRO_PEDIDO,	
	oraq.PLAZO_ENTREGA,
	oraq.FECHA_ORDEN,
	oraq.ANO_EJE,
	oraq.TIPO_BIEN,
	oraq.TIPO_PPTO,
	mva.NRO_ORDEN,
	dma.ANO_EJE,
	dma.ALMACEN,
	dma.SEC_ALMACEN,
	dma.TIPO_MOVIMTO,
	dma.TIPO_TRANSAC,
	dma.TIPO_PPTO,
	dma.NRO_MOVIMTO,
	dma.SECUENCIA,
	dma.SEC_DETALLE,
	dal.SEC_LOTE,
	PED.EMPLEADO
	FROM (((
	SIGA_ACTUAL.dbo.SIG_DETALLE_MOVIM_ALMACEN dma
	LEFT JOIN SIGA_ACTUAL.dbo.SIG_DETALLE_ALMACEN_LOTES dal 
		ON 
		dal.ANO_EJE = dma.ANO_EJE 
		and dal.ALMACEN = dma.ALMACEN 
		and dal.SEC_ALMACEN = dma.SEC_ALMACEN 
		and dal.TIPO_MOVIMTO = dma.TIPO_MOVIMTO
		and dal.TIPO_TRANSAC = dma.TIPO_TRANSAC
		and dal.TIPO_PPTO = dma.tipo_ppto
		and dal.NRO_MOVIMTO = dma.NRO_MOVIMTO
		and dal.SECUENCIA = dma.SECUENCIA
		and dal.SEC_DETALLE = dma.SEC_DETALLE
	INNER JOIN SIGA_ACTUAL.dbo.SIG_MOVIM_ALMACEN mva
		ON 
		mva.ANO_EJE = dma.ANO_EJE 
		and mva.ALMACEN = dma.ALMACEN 
		and mva.SEC_ALMACEN = dma.SEC_ALMACEN 
		and mva.TIPO_MOVIMTO = dma.TIPO_MOVIMTO
		and mva.TIPO_TRANSAC = dma.TIPO_TRANSAC
		and mva.TIPO_PPTO = dma.tipo_ppto
		and mva.NRO_MOVIMTO = dma.NRO_MOVIMTO
	)
	LEFT JOIN SIGA_ACTUAL.dbo.SIG_ORDEN_ADQUISICION oraq
	ON
		oraq.NRO_ORDEN = mva.NRO_ORDEN
		AND oraq.ANO_EJE =  mva.ANO_EJE
		AND oraq.TIPO_BIEN = mva.TIPO_BIEN
		and oraq.TIPO_PPTO = dma.tipo_ppto)
	LEFT JOIN SIGA_ACTUAL.dbo.SIG_PEDIDOS PED 
	ON 
		PED.NRO_PEDIDO = SUBSTRING(oraq.DOCUM_REFERENCIA,8,10) 
		AND oraq.ANO_EJE = PED.ANO_EJE
		AND PED.TIPO_BIEN = oraq.TIPO_BIEN)
	-- LEFT JOIN alm.personas persona ON persona.cPersDNI = PED.EMPLEADO
	WHERE 
		dma.ANO_EJE > 2018 
		AND dma.TIPO_BIEN = 'B'
		and dma.TIPO_MOVIMTO IN ('I','A')
		and mva.NRO_ORDEN IS NOT NULL