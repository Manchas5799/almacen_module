USE [UNAM_2020]
GO
/****** Object:  StoredProcedure [alm].[Sp_INS_almacenes]    Script Date: 9/09/2020 14:19:18 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
ALTER PROCEDURE [alm].[Sp_INS_almacenes]
	@_iAlmacenId INTEGER,
	@_cAlmacenNombre varchar(150),
	@_iEstadoId INTEGER,
	@_iAlmacenPrincipalId INTEGER,
	@_iDependFilId INTEGER,
	@_iAlmacenResponsablePersId INTEGER,

	@_iCredId INTEGER,
	@_cEquipoSis VARCHAR(50),
	@_cIpSis VARCHAR(15),
	@_cMacNicSis VARCHAR(35)
	/*
	*/
AS
/*
* Insertar Nuevo ALMACEN
*/

BEGIN TRANSACTION
	SET NOCOUNT ON
	DECLARE @cUsuarioSis VARCHAR(50)	
	SELECT @cUsuarioSis=c.cCredUsuario FROM seg.credenciales AS c WHERE c.iCredId=@_iCredId	
	
	DECLARE @cMensaje VARCHAR(MAX)

	SET @_cAlmacenNombre=RTRIM(LTRIM(@_cAlmacenNombre))

	
	IF COALESCE(@_cAlmacenNombre,'') = ''
		BEGIN
			SET @cMensaje='Debe especificar el nombre del Almacén.'
			RAISERROR (@cMensaje,18,1,1)
			GOTO ErrorCapturado
		END
	IF @_cAlmacenNombre IS NOT NULL
		BEGIN
			IF EXISTS(SELECT iAlmacenId FROM alm.almacenes WHERE UPPER(RTRIM(LTRIM(cAlmacenNombre))) COLLATE SQL_Latin1_General_Cp1_CI_AI=UPPER(RTRIM(LTRIM(@_cAlmacenNombre))))
				BEGIN
					SET @cMensaje='El Almacen '+UPPER(RTRIM(LTRIM(@_cAlmacenNombre)))+' ya se encuentra registrado, verifique por favor...'
					RAISERROR (@cMensaje,18,1,1)
					GOTO ErrorCapturado					
				END
		END

	
	DECLARE @iAlmacenId INTEGER,@iCodigoError INTEGER
	set @_cAlmacenNombre = UPPER(@_cAlmacenNombre)

	INSERT alm.Almacenes
	(cAlmacenNombre, iEstadoId, iAlmacenPrincipalId, iDependFilId, iAlmacenResponsablePersId,
	cAlmacenesUsuarioSis, dtAlmacenesFechaSis, cAlmacenesEquipoSis, cAlmacenesIpSis, cAlmacenesOpenUsr, cAlmacenesMacNicSis)
	VALUES ( @_cAlmacenNombre,@_iEstadoId, @_iAlmacenPrincipalId, @_iDependFilId, @_iAlmacenResponsablePersId,
		/*Campos de auditoria*/
		@cUsuarioSis, GETDATE(), @_cEquipoSis, @_cIpSis, 'N', @_cMacNicSis
	)

	SELECT @iAlmacenId=@@IDENTITY,@iCodigoError=@@ERROR

	IF @@ERROR<>0 GOTO ErrorCapturado
	
	COMMIT TRANSACTION
	SELECT 1 AS iResult,@iAlmacenId AS iAlmacenId
	RETURN @iAlmacenId

ErrorCapturado:
	ROLLBACK TRANSACTION
	SELECT 0 AS iResult
	RETURN 0
	/********************************************************************************************************************************************/