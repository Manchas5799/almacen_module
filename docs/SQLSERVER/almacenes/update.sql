USE [UNAM_2020]
GO
/****** Object:  StoredProcedure [alm].[Sp_UPD_almacenes]    Script Date: 9/09/2020 14:31:20 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
ALTER PROCEDURE [alm].[Sp_UPD_almacenes]
	@_iAlmacenId INTEGER,
	@_cAlmacenNombre varchar(150),
	@_iEstadoId INTEGER,
	@_iAlmacenPrincipalId INTEGER,
	@_iDependFilId INTEGER,
	@_iAlmacenResponsablePersId INTEGER,
	
	@_iCredId INTEGER,
	@_cEquipoSis VARCHAR(50),
	@_cIpSis VARCHAR(15),
	@_cMacNicSis VARCHAR(35)
	/*
	*/
AS
/*
* Actualización de Almacenes
*/

BEGIN TRANSACTION
	SET NOCOUNT ON
	DECLARE @cUsuarioSis VARCHAR(50)	
	SELECT @cUsuarioSis=c.cCredUsuario FROM seg.credenciales AS c WHERE c.iCredId=@_iCredId	
	IF @@ERROR<>0 GOTO ErrorCapturado

	DECLARE @cMensaje VARCHAR(MAX)

	SET @_cAlmacenNombre=UPPER(RTRIM(LTRIM(@_cAlmacenNombre)))

	UPDATE alm.Almacenes
	SET cAlmacenNombre = @_cAlmacenNombre,
		iEstadoId = @_iEstadoId ,
		iAlmacenPrincipalId = @_iAlmacenPrincipalId ,
		iDependFilId = @_iDependFilId ,
		iAlmacenResponsablePersId = @_iAlmacenResponsablePersId ,

		/*Campos de auditoria*/
		cAlmacenesUsuarioSis=@cUsuarioSis,
		dtAlmacenesFechaSis=GETDATE(),
		cAlmacenesEquipoSis=@_cEquipoSis,
		cAlmacenesIpSis=@_cIpSis,
		cAlmacenesOpenUsr='E',
		cAlmacenesMacNicSis=@_cMacNicSis
	WHERE iAlmacenId=@_iAlmacenId
	IF @@ERROR<>0 GOTO ErrorCapturado
	
	COMMIT TRANSACTION
	SELECT 1 AS iResult,@_iAlmacenId AS iAlmacenId
	RETURN @_iAlmacenId
ErrorCapturado:
	ROLLBACK TRANSACTION
	SELECT 0 AS iResult
	RETURN 0