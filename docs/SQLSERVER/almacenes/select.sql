USE [UNAM_2020]
GO
/****** Object:  StoredProcedure [alm].[Sp_SEL_Almacenes]    Script Date: 9/09/2020 13:55:28 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
ALTER PROCEDURE [alm].[Sp_SEL_Almacenes]
@_iDependFilId INTEGER =-1
AS
BEGIN
	IF(@_iDependFilId>0)
	BEGIN
		SET NOCOUNT ON
		SELECT DISTINCT al.[iAlmacenId] as id
		  ,al.[cAlmacenNombre] as nombre
		  ,al.[iEstadoId] as idestado
		  , es.[cEstadoNombre] as estado
		  ,al.[iAlmacenPrincipalId] as idalmacenprincipal
		  ,al.[iAlmacenResponsablePersId] as idresponsable
		  ,dep.iFilId as idfilial
		  , pe.cPersPaterno + ' ' + pe.cPersMaterno +', ' +pe.cPersNombre as responsable
		  , fi.cFilAbrev as filial
		  ,al1.cAlmacenNombre as almdepende
		  ,dep.cDepenNombre as nombredependencia
		  ,al.iDependFilId as iddependencia
		  ,(select iLugaresId as lugar_id, cLugaresNombre as lugar_nombre, cLugaresDescripcion as lugar_descripcion,cLugaresDireccion as lugar_direccion 
			from alm.lugares where iAlmacenId=al.iAlmacenId for json auto) as lugares
	  FROM [alm].[almacenes] al
	  LEFT JOIN alm.estados es ON al.iEstadoId = es.iEstadoId
	  LEFT JOIN grl.personas pe ON al.iAlmacenResponsablePersId = pe.iPersId
	  LEFT JOIN alm.almacenes al1 ON al.iAlmacenPrincipalId = al1.iAlmacenId
	  LEFT JOIN grl.dependencias dep ON al.iDependFilId = dep.iDepenId
	  LEFT JOIN grl.filiales fi ON dep.iFilId = fi.iFilId 
	  LEFT JOIN alm.lugares alug ON al.iAlmacenId = alug.iAlmacenId
	  WHERE al.iDependFilId=@_iDependFilId
	  ORDER BY al.iAlmacenId ASC
	END 
	ELSE
	BEGIN
		SET NOCOUNT ON
		SELECT DISTINCT al.[iAlmacenId] as id
		  ,al.[cAlmacenNombre] as nombre
		  ,al.[iEstadoId] as idestado
		  , es.[cEstadoNombre] as estado
		  ,al.[iAlmacenPrincipalId] as idalmacenprincipal
		  ,al.[iAlmacenResponsablePersId] as idresponsable
		  ,dep.iFilId as idfilial
		  , pe.cPersPaterno + ' ' + pe.cPersMaterno +', ' +pe.cPersNombre as responsable
		  , fi.cFilAbrev as filial
		  ,al1.cAlmacenNombre as almdepende
		  ,dep.cDepenNombre as nombredependencia
		  ,al.iDependFilId as iddependencia
	  FROM [alm].[almacenes] al
	  LEFT JOIN alm.estados es ON al.iEstadoId = es.iEstadoId
	  LEFT JOIN grl.personas pe ON al.iAlmacenResponsablePersId = pe.iPersId
	  LEFT JOIN alm.almacenes al1 ON al.iAlmacenPrincipalId = al1.iAlmacenId
	  LEFT JOIN grl.dependencias dep ON al.iDependFilId = dep.iDepenId
	  LEFT JOIN grl.filiales fi ON dep.iFilId = fi.iFilId
	  ORDER BY al.iAlmacenId ASC
	END
END