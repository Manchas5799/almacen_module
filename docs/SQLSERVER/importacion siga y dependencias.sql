USE UNAM_2020
GO
DELETE FROM alm.oc_pedidos
DELETE FROM alm.personas
DELETE FROM alm.lugares
DELETE FROM alm.almacenes
TRUNCATE TABLE alm.almacenes
SELECT * FROM alm.almacenes
select * from alm.personas

-- INSERTAR ESTADOS
INSERT INTO alm.estados (cEstadoNombre) VALUES ('ACTIVO')
INSERT INTO alm.estados (cEstadoNombre) VALUES ('SUSPENDIDO')
INSERT INTO alm.estados (cEstadoNombre) VALUES ('BAJA')
SELECT * FROM alm.estados

-- insertar almacen principal 
-- INSERT INTO alm.almacenes 
-- (cAlmacenNombre, iEstadoId,iDependFilId,SIGA_ALMACEN,iAlmacenResponsablePersId) 
-- VALUES ('PRINCIPAL',1,13,'000',6217)
INSERT INTO alm.almacenes 
(cAlmacenNombre, iEstadoId,iDependFilId,SIGA_ALMACEN,iAlmacenResponsablePersId) 
VALUES ('PRINCIPAL',1,14,'000',6217)
-- INSERTAR ALMACENES
ALTER TABLE alm.almacenes ALTER COLUMN cAlmacenNombre varchar(MAX)
	-- obtener informacion de jim
	-- select * from grl.personas where cPersNombre LIKE 'JIM%'
	-------------------
INSERT INTO alm.almacenes 
(cAlmacenNombre, iEstadoId,iDependFilId,SIGA_ALMACEN,iAlmacenResponsablePersId,iAlmacenPrincipalId) 
select cDepenNombre,1,iDepenId,NULL,NULL,619 from grl.dependencias where iDepenId <> 14
-- select cDepenNombre,1,iDepenId,NULL,NULL,1 from grl.dependencias where iDepenId <> 13

--- INSERTAR PERSONAS
ALTER TABLE alm.personas ALTER COLUMN iPersDNI varchar(8)
ALTER TABLE alm.personas ALTER COLUMN cPersonasCargo varchar(250)

insert into alm.personas(cPersDNI,cPersonasCargo,iAlmacenId)
select persona.cPersDocumento, cargo.cCargNombre , almacen.iAlmacenId
FROM 
	seg.credenciales_dependencias cd LEFT JOIN 
	seg.credenciales cred ON  cd.iCredId = cred.iCredId LEFT JOIN
	grl.personas persona ON  cred.iPersId = persona.iPersId  LEFT JOIN
	rhh.cargos cargo ON cd.iCargId = cargo.iCargId LEFT JOIN 
	alm.almacenes almacen ON cd.iDepenId = almacen.iDependFilId ORDER BY almacen.iAlmacenId


--- IMPORTAR PEDIDOS DEL 2020 DESDE SIGA A NUESTRA TABLAS DE PEDIDOS
USE UNAM_2020
GO
INSERT INTO alm.oc_pedidos (iPersonasId, SIGA_ANO_EJE,SIGA_SEC_EJEC,SIGA_TIPO_BIEN,SIGA_TIPO_PEDIDO,SIGA_NRO_PEDIDO)
select 
	persona.iPersonasId,
	pedido.ANO_EJE,
	pedido.SEC_EJEC,
	pedido.TIPO_BIEN,
	pedido.TIPO_PEDIDO,
	pedido.NRO_PEDIDO from 
	SIGA_ACTUAL.dbo.SIG_PEDIDOS pedido INNER JOIN alm.personas persona ON persona.cPersDNI = pedido.EMPLEADO 
	WHERE pedido.ANO_EJE = 2020 

-- select * from alm.oc_pedidos

--- REGISTRAR LUGARES DESDE ALMACENES
ALTER TABLE alm.lugares ALTER COLUMN cLugaresNombre varchar(255)

INSERT INTO alm.lugares (iAlmacenId,cLugaresNombre)
select iAlmacenId, cAlmacenNombre from alm.almacenes