ALTER PROCEDURE [alm].[Sp_SEL_pedidos]
@_iAlmacenId INTEGER
AS
BEGIN
	select
	aop.SIGA_ANO_EJE as ano_eje,
	aop.SIGA_SEC_EJEC as sec_ejec,
	aop.SIGA_TIPO_BIEN as tipo_bien,
	aop.SIGA_TIPO_PEDIDO as tipo_pedido,
	aop.SIGA_NRO_PEDIDO as nro_pedido,
	sip.MOTIVO_PEDIDO as motivo_pedido,
	sip.FECHA_PEDIDO as fecha_pedido,
    sip.EMPLEADO as empleado,
	gper.cPersNombre + ' ' + gper.cPersPaterno + ' ' + gper.cPersMaterno as nombre_empleado
	--almp.iAlmacenId as almacen
	from alm.oc_pedidos aop
	left join SIGA_ACTUAL.dbo.SIG_PEDIDOS sip
	on aop.SIGA_ANO_EJE=sip.ANO_EJE
	AND aop.SIGA_SEC_EJEC=sip.SEC_EJEC
	AND aop.SIGA_TIPO_BIEN=sip.TIPO_BIEN
	AND aop.SIGA_TIPO_PEDIDO=sip.TIPO_PEDIDO
	AND aop.SIGA_NRO_PEDIDO=sip.NRO_PEDIDO
	LEFT JOIN  alm.personas almp ON almp.iPersonasId = aop.iPersonasId
	LEFT JOIN grl.personas gper ON almp.cPersDNI = gper.cPersDocumento
	where sip.EMPLEADO IN (SELECT cPersDNI 
    FROM alm.personas 
    WHERE iAlmacenId = @_iAlmacenId
    ) AND aop.iPersonasId  IN (SELECT iPersonasId 
    FROM alm.personas 
    WHERE iAlmacenId = @_iAlmacenId
    ) 
END