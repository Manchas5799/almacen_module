USE UNAM_2020
GO
INSERT INTO alm.estados (cEstadoNombre) VALUES ('ACTIVO')
INSERT INTO alm.estados (cEstadoNombre) VALUES ('SUSPENDIDO')
INSERT INTO alm.estados (cEstadoNombre) VALUES ('BAJA')
---------
INSERT INTO alm.almacenes 
(cAlmacenNombre, iEstadoId,iDependFilId,SIGA_ALMACEN,iAlmacenResponsablePersId) 
VALUES ('PRINCIPAL',1,14,'000',6217)
----------------
INSERT INTO alm.almacenes 
(cAlmacenNombre, iEstadoId,iDependFilId,SIGA_ALMACEN,iAlmacenResponsablePersId,iAlmacenPrincipalId) 
select cDepenNombre,1,iDepenId,NULL,NULL,1 from grl.dependencias where iDepenId <> 14
-------------
INSERT INTO alm.lugares (iAlmacenId,cLugaresNombre)
select iAlmacenId, cAlmacenNombre from alm.almacenes
------------- movimientos
USE SIGA_ACTUAL
GO
INSERT INTO UNAM_2020.alm.SIGA_Movimientos_Almacen 
(
SIGA_SEC_EJEC,
SIGA_ANO_EJE,
SIGA_ALMACEN,
SIGA_SEC_ALMACEN,
SIGA_TIPO_MOVIMTO,
SIGA_TIPO_TRANSAC,
SIGA_TIPO_PPTO,
SIGA_NRO_MOVIMTO,
dtOCFechaNotificacion,
nOCPlazoEntrega,
cOCEstado
)
SELECT
	mva.SEC_EJEC,
	mva.ANO_EJE,
	mva.ALMACEN,
	mva.SEC_ALMACEN,
	mva.TIPO_MOVIMTO,
	mva.TIPO_TRANSAC,
	mva.TIPO_PPTO,
	mva.NRO_MOVIMTO,
	oraq.FECHA_ORDEN,
	oraq.PLAZO_ENTREGA,
	oraq.ESTADO
FROM 
	SIG_MOVIM_ALMACEN mva
	INNER JOIN SIGA_ACTUAL.dbo.SIG_ORDEN_ADQUISICION oraq
	ON
		oraq.NRO_ORDEN = mva.NRO_ORDEN
		AND oraq.ANO_EJE =  mva.ANO_EJE
		AND oraq.TIPO_BIEN = mva.TIPO_BIEN
		and oraq.TIPO_PPTO = mva.TIPO_PPTO
WHERE 
	mva.ANO_EJE > 2018
	AND mva.TIPO_BIEN = 'B'
	ORDER BY mva.NRO_MOVIMTO
---------------------------- AHORA DETALLES
USE UNAM_2020
GO
INSERT INTO alm.SIGA_Detalles_Movimientos_Almacen
(
iSIGAMovimId,
SIGA_SECUENCIA,
SIGA_SEC_DETALLE,
SIGA_SEC_LOTE,
SIGA_GRUPO_BIEN,
SIGA_CLASE_BIEN,
SIGA_FAMILIA_BIEN,
SIGA_ITEM_BIEN,
SIGA_CLASIFICADOR,
SIGA_MAYOR,
SIGA_SUB_CUENTA)
SELECT 
mva.iSIGAMovimId,
det.SECUENCIA,
det.SEC_DETALLE,
dal.SEC_LOTE,
det.GRUPO_BIEN,
det.CLASE_BIEN,
det.FAMILIA_BIEN,
det.ITEM_BIEN,
det.CLASIFICADOR,
det.MAYOR,
det.SUB_CTA
FROM 
SIGA_ACTUAL.dbo.SIG_DETALLE_MOVIM_ALMACEN det
INNER JOIN alm.SIGA_Movimientos_Almacen mva
ON 
det.sec_ejec = mva.SIGA_SEC_EJEC AND
det.ANO_EJE = mva.SIGA_ANO_EJE AND
det.ALMACEN = mva.SIGA_ALMACEN AND
det.SEC_ALMACEN = mva.SIGA_SEC_ALMACEN AND
det.TIPO_MOVIMTO = mva.SIGA_TIPO_MOVIMTO AND
det.TIPO_TRANSAC =mva.SIGA_TIPO_TRANSAC AND
det.TIPO_PPTO = mva.SIGA_TIPO_PPTO AND
det.NRO_MOVIMTO = mva.SIGA_NRO_MOVIMTO
LEFT JOIN SIGA_ACTUAL.dbo.SIG_DETALLE_ALMACEN_LOTES dal 
		ON 
		dal.ANO_EJE = det.ANO_EJE 
		and dal.ALMACEN = det.ALMACEN 
		and dal.SEC_ALMACEN = det.SEC_ALMACEN 
		and dal.TIPO_MOVIMTO = det.TIPO_MOVIMTO
		and dal.TIPO_TRANSAC = det.TIPO_TRANSAC
		and dal.TIPO_PPTO = det.tipo_ppto
		and dal.NRO_MOVIMTO = det.NRO_MOVIMTO
		and dal.SECUENCIA = det.SECUENCIA
		and dal.SEC_DETALLE = det.SEC_DETALLE
ORDER BY 
mva.iSIGAMovimId