import { Injectable } from '@angular/core';

@Injectable({
  providedIn: 'root',
})
export class ModalsService {
    private modals: any[] = [];
    add(modal: any) {
        // ADICOINAR modal to array of active modals
        this.modals.push(modal);
    }

    remove(id: string) {
        // REMOVER modal from array of active modals
        this.modals = this.modals.filter(x => x.id !== id);
    }

    open(id: string,opciones:Object) {
        // ABRIR modal specified by id
        const modal = this.modals.find(x => x.id === id);
        console.log(modal)
        modal.open(opciones);
    }

    close(id: string) {
        // CERRAR modal specified by id
        const modal = this.modals.find(x => x.id === id);
        modal.close();
    }
}