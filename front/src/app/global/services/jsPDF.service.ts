import { Injectable } from "@angular/core";

import { environment } from "../../../environments/environment";
import * as jsPDF from "jspdf";
import "jspdf-autotable";

import { formatDate } from "@angular/common";

@Injectable({
  providedIn: "root",
})
export class JsPDFService {
  institucion = {
    nombre: environment.entidad.nombre,
    abreviacion: environment.entidad.abrev,
  };
  modulo = {
    nombre: environment.aplicacion.nombre,
    version: environment.aplicacion.version,
  };
  pagReport: number = 1;
  espaciado: number = 8;
  nroRegXPag: number = 25;
  margenes;
  columnasArray;
  columnasArrayPorcentajes;
  filtrosArray;
  titulo;
  cabecerasTabla;
  datosTabla;
  columnaStyles;
  doc: jsPDF;

  constructor() {
    // this.doc = new jsPDF(this.configDoc)
  }
  crearDoc(orientacion) {
    if (orientacion != "p") {
      this.doc = new jsPDF({
        orientation: "landscape",
        unit: "mm",
        format: "A4",
      });
      this.margenes = {
        Izq: 30,
        Der: 270,
        Sup: 30,
        Inf: 195,
      };
      this.nroRegXPag = 17;
    } else {
      this.doc = new jsPDF({
        orientation: "p",
        unit: "mm",
        format: "A4",
      });
      this.margenes = {
        Izq: 30,
        Der: 195,
        Sup: 30,
        Inf: 270,
      };
      this.nroRegXPag = 25;
    }
  }
  generaPDF(rep, esAutoTabla = 0, datosTablaExtra = null) {
    this.titulo = rep.titulo;

    this.crearDoc(rep.orientacion);
    if (esAutoTabla == 0) {
      this.generarPDFManual(rep);
    } else {
      this.columnaStyles = rep.columnaStyles;
      this.repAutoTabla(rep, datosTablaExtra);
    }
  }
  generarPDFManual(rep) {
    this.columnasArray = Object.entries(rep.cabeceras); //Convertir a Array las Cabeceras
    this.columnasArrayPorcentajes = Object.entries(rep.cabeceras);
    this.filtrosArray = Object.entries(rep.filtros); //Convertir a Array los Filtros

    this.calcularInciarColEn(); // preparar CUANTAS COLUMNAS HAY QUE MOSTRAR

    this.repCabecera();
    //this.repAutoTabla(rep)
    this.repPie();
    //this.repTabla(rep.datos)
    this.repContenido(rep);
    this.doc.save(this.titulo + ".pdf");
  }

  repCabecera(pagina = 1, tipotitulo = null) {
    this.doc.setTextColor(0, 0, 150);
    this.doc.setFontSize(10);

    /* Insertamos el logo */
    let logo = new Image();
    //if(pagina==1){
    logo.src = "assets/images/logo.png";
    this.doc.addImage(logo, "PNG", 22, 5, 18, 20);
    // }

    this.doc.text(
      this.margenes.Izq + 10,
      this.margenes.Sup - 15,
      this.institucion.nombre
    );
    this.doc.setFontSize(14);
    this.doc.setTextColor(0, 0, 0);
    this.doc.text(
      this.margenes.Izq + 10,
      this.margenes.Sup - 10,
      tipotitulo === 1 ? this.titulo : "REPORTE DE " + this.titulo
    );
    this.doc.setTextColor(150, 0, 0);
    this.doc.setFontSize(8);
    this.doc.text(
      this.margenes.Der,
      this.margenes.Sup - 15,
      "Versión: " + this.modulo.version,
      "right"
    );
    this.doc.setFontSize(10);
    this.doc.text(
      this.margenes.Der,
      this.margenes.Sup - 10,
      this.modulo.nombre,
      "right"
    );
    this.doc.setLineWidth(0.25);
    this.doc.line(
      this.margenes.Izq + 10,
      this.margenes.Sup - 7,
      this.margenes.Der,
      this.margenes.Sup - 7
    );
  }
  calcularInciarColEn() {
    let ancho = this.margenes.Der - this.margenes.Izq;
    let suma = 0;
    this.columnasArray[0][0] = this.margenes.Izq;
    for (let index = 1; index < this.columnasArray.length; index++) {
      const element = this.columnasArray[index - 1];
      this.columnasArray[index][0] =
        this.columnasArray[index - 1][0] + ancho * (element[1].ancho / 100);
    }
  }
  repMostrarTitulosTabla(cabeceras) {
    let yCabecera = this.margenes.Sup + 13;
    this.doc.setLineWidth(0.5);
    this.doc.line(
      this.margenes.Izq,
      this.margenes.Sup + 8,
      this.margenes.Der,
      this.margenes.Sup + 8
    );
    //let espaciado  = this.margenes.Izq
    for (let index = 0; index < this.columnasArray.length; index++) {
      const element = this.columnasArray[index];
      this.doc.setFontSize(11);
      this.doc.setFontType("bold");
      this.doc.text(element[0], yCabecera, element[1].titulo);
      this.doc.setFontSize(9);
      this.doc.setFontType("normal");
      this.doc.text(
        element[0],
        yCabecera + 5,
        'Filtro: "' + this.filtrosArray[index][1] + '"'
      );
    }
    this.doc.line(
      this.margenes.Izq,
      yCabecera + 7,
      this.margenes.Der,
      yCabecera + 7
    );
  }
  repMostrarDatos(rep) {
    let espaciado: number = 8;
    let yDatos: number = 55;

    let i: number = 0;

    rep.datos.forEach((dato) => {
      let registro = Object.entries(dato);
      /* Verificamos CAMBIO DE PÁGINA */
      if (i > 0 && i % this.nroRegXPag == 0) {
        this.doc.addPage();
        i = 0;
        this.repCabecera();
        this.repCabeceraContenido(rep);
        this.repPie();
      }
      this.doc.setFontType("normal");
      this.doc.setFontSize(10);
      this.doc.setTextColor(0, 0, 0);
      for (let index = 0; index < registro.length; index++) {
        //const element = registro[index];
        this.doc.text(
          this.columnasArray[index][0],
          yDatos + i * espaciado,
          registro[index][1] + ""
        );
      }
      i++;
    });

    this.doc.line(
      this.margenes.Izq,
      yDatos + i * espaciado,
      this.margenes.Der,
      yDatos + i * espaciado
    );
  }
  repContenido(rep) {
    this.repCabeceraContenido(rep);

    this.repMostrarDatos(rep);
  }
  repCabeceraContenido(rep) {
    let texto;
    this.doc.setTextColor(0, 0, 0);
    this.doc.setFontType("bold");
    this.doc.setFontSize(14);
    this.doc.text(
      this.doc.internal.pageSize.width / 2,
      this.margenes.Sup,
      "REPORTE DE " + this.titulo,
      "center"
    );
    texto = this.doc.getTextDimensions("REPORTE DE " + rep.titulo);
    console.log(texto);
    this.repMostrarTitulosTabla(rep.cabecera);
  }
  repPie(pag = 1) {
    let toDay = Date.now();
    let yDown: number = this.margenes.Inf;
    let toDayString = "";
    toDayString = formatDate(toDay, "dd/MM/yyyy hh:mm:ss a", "es-ES");
    this.doc.setFontSize(10);
    this.doc.setLineWidth(0.25);
    this.doc.line(this.margenes.Izq - 10, yDown, this.margenes.Der, yDown);
    this.doc.setTextColor(255, 0, 0);
    this.doc.text(
      this.margenes.Izq - 10,
      yDown + 5,
      "Fecha Impresión: " + toDayString
    );
    this.doc.text(
      this.margenes.Der,
      yDown + 5,
      "Pág. " + pag + " de " + this.doc.internal.getNumberOfPages(),
      "right"
    );
  }

  repTabla(datos) {
    let specialElementHandlers = {
      "#bypassme": function (element, renderer) {
        return true;
      },
    };

    let margins = {
      top: 30,
      bottom: 40,
      left: 30,
      width: 1100,
    };
    let source = this.convertir2tableHTML(datos);
    this.doc.fromHTML(
      source, // HTML string or DOM elem ref.
      margins.left, // x coord
      margins.top,
      {
        // y coord
        width: margins.width, // max width of content on PDF
        elementHandlers: specialElementHandlers,
      }
    );
    this.repEncabezadoPie();
    this.doc.save("Test.pdf");
  }

  convertir2tableHTML(datos: any = []) {
    let strTable = "<table id='tableReport' style='font-size:7px; width=100%'>";
    strTable += "<tr>"; //Armamos las cabeceras de las COLUMNAS
    for (let index = 0; index < this.columnasArrayPorcentajes.length; index++) {
      const element = this.columnasArrayPorcentajes[index];
      strTable +=
        "<td style='width:" +
        element[0] +
        "%; height: 70px;'>" +
        element[1].titulo +
        "</td>";
    }
    strTable += "</tr>";
    let strRow = "";
    if (datos.length > 0) {
      datos.forEach((dato) => {
        let registro = Object.entries(dato);
        /* Verificamos CAMBIO DE PÁGINA */
        strRow = "<tr>";
        for (var i = 0; i < registro.length; i++) {
          strRow += "<td style='height: 70px;'>" + registro[i][1] + "</td>";
        }
        strRow += "</tr>";
        strTable += strRow;
      });
      //strTable += "</table>";
    }
    strTable += "</table>";
    return strTable;
  }
  repEncabezadoPie(datosExtra = null, tipotitulo = null) {
    var number_of_pages = this.doc.internal.getNumberOfPages();
    var pdf_pages = this.doc.internal.pages;
    //var myFooter = "Footer info"
    for (var i = 1; i < pdf_pages.length; i++) {
      // We are telling our this.doc that we are now working on this page
      this.doc.setPage(i);
      this.repCabecera(i, tipotitulo);
      if (datosExtra !== null) {
        this.repCabeceraTablaDatos(datosExtra);
      }
      this.repPie(i);
      // The 10,200 value is only for A4 landscape. You need to define your own for other page sizes
      //this.doc.text(myFooter, 10, 200)
    }
  }
  repCabeceraTablaDatos(datosExtra) {
    this.doc.autoTable({
      // margin: { top: 30 },
      startY: 30,
      headStyles: { fillColor: [0, 128, 255] }, // Purple
      styles: {
        lineWidth: 0.3,
        lineColor: 150,
        valign: "middle",
        fontSize: 8,
      },
      columnStyles: datosExtra.columnaStyles,
      head: datosExtra.headers,
      body: datosExtra.data,
    });
  }
  repAutoTabla(rep, datosExtra = null) {
    // Reporte con AutoTabla OK
    this.cabecerasTabla = rep.cabeceras;
    this.datosTabla = rep.datos;
    this.doc.autoTable({
      margin: { top: datosExtra !== null ? 50 : 25, bottom: 25 },
      headStyles: { fillColor: [0, 128, 255] }, // Purple
      styles: { lineWidth: 0.3, lineColor: 150, valign: "middle", fontSize: 8 },
      columnStyles: this.columnaStyles,
      head: this.cabecerasTabla,
      body: this.datosTabla,
    });
    this.repEncabezadoPie(datosExtra, rep.tipotitulo || null);
    this.doc.setProperties({
      title: rep.titulo + ".pdf",
    });
    // STREAM EN NUEVA VENTA DEL DOCUMENTO
    window.open(this.doc.output("bloburl"));
    // this.doc.save(rep.titulo+'.pdf')
  }
}
