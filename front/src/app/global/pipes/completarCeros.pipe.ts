import { Pipe, PipeTransform } from "@angular/core";
import { DomSanitizer, SafeHtml } from "@angular/platform-browser";

@Pipe({
  name: "completar_ceros",
})
export class CompletarCeros implements PipeTransform {
  constructor(private _sanitizer: DomSanitizer) {}

  transform(datos: number, cantidad: number): SafeHtml {
    if (!datos) {
      return [];
    }
    if (cantidad === 0) {
      return datos;
    }
    let tamDato = datos.toString().length;
    let ceros = ``;
    for (let i = 0; i < cantidad - tamDato; i++) {
      ceros += "0";
    }
    ceros = `${ceros}${datos}`;
    return this._sanitizer.bypassSecurityTrustHtml(ceros);
  }
}
