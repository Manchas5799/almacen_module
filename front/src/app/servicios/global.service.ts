import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { FormControl, FormGroup ,Validators } from '@angular/forms';
import { environment } from './../../environments/environment';
import swal from 'sweetalert2'
import { LocalStoreService } from "../../../src/app/shared/services/local-store.service";
import { Router } from "@angular/router";
import { ToastrService } from 'ngx-toastr';
import { QueryService } from './../servicios/query.services'
@Injectable({
  providedIn: 'root'
})
export class GlobalService {
  private baseUrl: string;
  private baseUrlCrystal: string;
  constructor(
    private http: HttpClient, 
    private store: LocalStoreService,
    private router: Router,
    private toastr: ToastrService,
    private query:QueryService,
  ) {
    this.baseUrl = environment.rutas.backEnd;
    this.baseUrlCrystal = environment.rutas.creport;

   }
  errorEstatusServ(error){
    let resp={}
    switch(error['status'])
    {
      case 0:
        resp = {
          mensaje:'Sin conexión',
          status:error['status'],
          style:'error',
        } 
        // swal.fire(
        //   'Error!',
        //   'Estatus: '+resp['status']+' - '+resp['mensaje'],
        //   resp['style']
        // )          
      this.toastr.warning('Estatus: '+resp['status']+' - '+resp['mensaje'],'Importante');      
      break;
      case 200:
        resp = {
          mensaje:error['error']['text'],
          status:error['status'],
          style:'warning',
        }      
        // swal.fire(
        //   'Error!',
        //   'Estatus: '+resp['status']+' - '+resp['mensaje'],
        //   resp['style']
        // )     
        this.toastr.warning('Estatus: '+resp['status']+' - '+resp['mensaje'],'Importante');              
      break;
      case 401:
        resp = {
          mensaje:'Sesión Caducada',
          status:error['status'],
          style:'error',
        } 
        //this.authenticated = false;
        this.store.clear()
        this.router.navigateByUrl("/sessions/signin");

      break;

      case 404:
        resp = {
          mensaje:'Página solicitada no encontrada.',
          status:error['status'],
          style:'error',
        } 
        // swal.fire(
        //   'Error!',
        //   'Estatus: '+resp['status']+' - '+resp['mensaje'],
        //   resp['style']
        // )    
        this.toastr.warning('Estatus: '+resp['status']+' - '+resp['mensaje'],'Importante');                      
      break;
      case 405:
        resp = {
          mensaje:error['error']['message'],
          status:error['status'],
          style:'error',
        } 
        // swal.fire(
        //   'Error!',
        //   'Estatus: '+resp['status']+' - '+resp['mensaje'],
        //   resp['style']
        // )         
       this.toastr.warning('Estatus: '+resp['status']+' - '+resp['mensaje'],'Importante');              
      break;      
      case 500:
        resp = {
          //SQLSTATE
          mensaje:error['error']['message'].substring(71,error['error']['message'].indexOf("(SQL:")),          
          status:error['status'],
          style:'error',
        }  
        // swal.fire(
        //   'Error!',
        //   'Estatus: '+resp['status']+' - '+resp['mensaje'],
        //   resp['style']
        // ) 
        this.toastr.warning('Estatus: '+resp['status']+' - '+resp['mensaje'],'Importante');              
      break;
      case 'timeout':
        resp = {
          mensaje:'Tiempo de espera agotado',
          status:error['status'],
          style:'error',
        } 
        // swal.fire(
        //   'Error!',
        //   'Estatus: '+resp['status']+' - '+resp['mensaje'],
        //   resp['style']
        // )        
        this.toastr.warning('Estatus: '+resp['status']+' - '+resp['mensaje'],'Importante');                      
      break;
      case 'parsererror':
        resp = {
          mensaje:'El análisis JSON solicitado falló.',
          status:error['status'],
          style:'error',
        } 
        // swal.fire(
        //   'Error!',
        //   'Estatus: '+resp['status']+' - '+resp['mensaje'],
        //   resp['style']
        // )  
        this.toastr.warning('Estatus: '+resp['status']+' - '+resp['mensaje'],'Importante');                     
      break;
      case 'abort':
        resp = {
          mensaje:'Solicitud abortada.',
          status:error['status'],
          style:'error',
        } 
        // swal.fire(
        //   'Error!',
        //   'Estatus: '+resp['status']+' - '+resp['mensaje'],
        //   resp['style']
        // ) 
        this.toastr.warning('Estatus: '+resp['status']+' - '+resp['mensaje'],'Importante');                      
      break;
      default:
        resp = {
          mensaje:'Error no conocido',
          status:'desconocido',
          style:'error',
        } 
        // swal.fire(
        //   'Error!',
        //   'Estatus: '+resp['status']+' - '+resp['mensaje'],
        //   resp['style']
        // ) 
        this.toastr.warning('Estatus: '+resp['status']+' - '+resp['mensaje'],'Importante');                      
      break 
    }
    //return resp;
  }
  validateAllFormFields(formGroup: FormGroup):void {         
    Object.keys(formGroup.controls).forEach(field => {  
      const control = formGroup.get(field);            
      if (control instanceof FormControl) {             
        control.markAsTouched({ onlySelf: true });
      } else if (control instanceof FormGroup) {        
        this.validateAllFormFields(control);            
      }
    });
  } 

  rptPDFCrystal(modulo,nameReport,typeReport,id,ids,parameters={}):void{
    switch(typeReport)
    {
      case 'pdf':
        let url =  `${this.baseUrlCrystal}?module=${modulo}&nameReport=${nameReport}&typeReport=${typeReport}&id=${id}&ids=${ids}&p1=${parameters['p1']}&p2=${parameters['p2']}&p3=${parameters['p3']}`
        this.formatScreen(url)
      break;
      case 'excel':
        this.query.rptEXCEL(modulo,nameReport,typeReport,id,ids).subscribe(
          data=>{
            var FileSaver = require('file-saver');
              var blob = new Blob([data], {type: 'application/vnd.ms-excel'});
              FileSaver.saveAs(blob, nameReport+'.xlsx');
          },
          error=>{
            swal.fire(
              'Error!',
              'Estatus: '+error['status']+' - '+error['message'],
              'error'
              )
          },            
        )        
      break;
      case 'word':
        this.http.get(
          `${this.baseUrlCrystal}?module=${modulo}&nameReport=${nameReport}&typeReport=${typeReport}&id=${id}&ids=${ids}&p1=${parameters['p1']}&p2=${parameters['p2']}&p3=${parameters['p3']}`
        ).subscribe(
          data=>{
            this.downloadFile(data)
          },
          error =>{
            let resp = this.errorEstatusServ(error)
            swal.fire(
              'Error!',
              'Estatus: '+resp['status']+' - '+resp['mensaje'],
              resp['style']
              )            
          }
        ) 
      break;
    }
  }
  downloadFile(filePath) {
    var link = document.createElement('a');
    link.href = filePath;
    link.download = filePath.substr(filePath.lastIndexOf('/') + 1);
    link.click();
  }
  formatScreen(url)
  {
    let w = window.open('about:blank');
    w.document.open();
    w.document.write('<body style="background-color:#eeeeee"><h3 style="color:blue">Generando el PDF...espere</h3></body>')
    w.document.write(`<iframe src="${url}" style="position: fixed;top: 0px;bottom: 0px;right: 0px;width: 100%;border: none;margin: 0;padding: 0;overflow: hidden;z-index: 999999;height: 100%;"></iframe>`);
    w.document.close();
  }

}
