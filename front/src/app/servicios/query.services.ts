import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders, HttpErrorResponse } from '@angular/common/http';

//import { Observable, of } from 'rxjs';
import { catchError, retry  } from 'rxjs/operators'
import { throwError } from 'rxjs';
import { LocalService } from './local.services';
import { environment } from './../../environments/environment';
import { THIS_EXPR } from '@angular/compiler/src/output/output_ast';

@Injectable({
  providedIn: 'root',
})
export class QueryService {
  private baseUrl: string;
  private baseUrlCrystal: string;
  gestudiante;
  headers: any;
  token: any;
  ls = window.localStorage;
  httpOptions: any;
  private codEstu: string = '';
  constructor(private http: HttpClient, private local: LocalService) {
    this.baseUrl = environment.rutas.backEnd;
    this.baseUrlCrystal = environment.rutas.creport;
    
  }
  getCod() {
    // this.codEstu = this.local.getItem('codigo');
  }
  getCurricula() {
    this.getCod();
    return this.http.get(
      `${this.baseUrl}/ura/estudiante/obtenerPlanCurricularEstudiante/${this.codEstu}`,
    );
  }

  insertHead() {
    this.ls.getItem('unamToken');
    let data = this.local.getItem('unamToken');
    this.token = data['access_token'];
    if (data['access_token']) {
      this.httpOptions = {
        headers: new HttpHeaders({
          'Content-Type': 'application/json',
          Authorization: 'Bearer ' + this.token,
        }),
      };
    }
  }
  insertHeadFiles() {
    this.ls.getItem('unamToken');
    let data = this.local.getItem('unamToken');
    this.token = data['access_token'];
    if (data['access_token']) {
      this.httpOptions = {
        headers: new HttpHeaders({
          Authorization: 'Bearer ' + this.token,
        }),
      };
    }
  }
 

/*
--/**TABLAS MAESTRAS 

  selModalidadesContratos() {
    this.insertHead()
    return this.http.get(
      `${this.baseUrl}/tesoreria/tablasMaestras/modalidadesContrato/selModalidadesContratos`,this.httpOptions
    );
  }
  datModalidadesContratos(iModalidadContratoId) {
    this.insertHead()
    return this.http.get(
      `${this.baseUrl}/tesoreria/tablasMaestras/modalidadesContrato/datModalidadesContratos/${iModalidadContratoId}`,this.httpOptions
    );
  }

  savModalidadesContratos(data) {
    this.insertHead()
    return this.http.post(
      `${this.baseUrl}/tesoreria/tablasMaestras/modalidadesContrato/savModalidadesContratos`,data,this.httpOptions
    );
  }

  delModalidadesContratos(data) {
    this.insertHead()
    return this.http.post(
      `${this.baseUrl}/tesoreria/tablasMaestras/modalidadesContrato/delModalidadesContratos`,data,this.httpOptions
    );
  }




  selBancos() {
    this.insertHead()
    return this.http.get(
      `${this.baseUrl}/tesoreria/tablasMaestras/bancos/selBancos`,this.httpOptions
    );
  }
  datBancos(iBancoId) {
    this.insertHead()
    return this.http.get(
      `${this.baseUrl}/tesoreria/tablasMaestras/bancos/datBancos/${iBancoId}`,this.httpOptions
    );
  }
  savBancos(data) {
    this.insertHead()
    return this.http.post(
      `${this.baseUrl}/tesoreria/tablasMaestras/bancos/savBancos`,data,this.httpOptions
    );
  }
  delBancos(data) {
    this.insertHead()
    return this.http.post(
      `${this.baseUrl}/tesoreria/tablasMaestras/bancos/delBancos`,data,this.httpOptions
    );
  }

  selSecuenciaDocumento(data) {
    this.insertHead()
    return this.http.post(
      `${this.baseUrl}/tesoreria/globales/selSecuenciaDocumento`,data,this.httpOptions
    );
  }

  

  /**FILTRO CARTAS FIANZA 
  selPeriodos(iEntId) {
    this.insertHead()
    return this.http.get(
      `${this.baseUrl}/tesoreria/cartasFianza/selPeriodos/${iEntId}`,this.httpOptions
    );
  }
  selMeses(iEntId,iCartaFianzaCustodiaYear) {
    this.insertHead()
    return this.http.get(
      `${this.baseUrl}/tesoreria/cartasFianza/selMeses/${iEntId}/${iCartaFianzaCustodiaYear}`,this.httpOptions
    );
  }
  selCriterioBusqueda() {
    this.insertHead()
    return this.http.get(
      `${this.baseUrl}/tesoreria/cartasFianza/selCriterioBusqueda`,this.httpOptions
    );
  }

  selCuentasDetraccion(data) {
    this.insertHead()
    return this.http.post(
      `${this.baseUrl}/tesoreria/tablasMaestras/cuentasDetraccion/selCuentasDetraccion`,data,this.httpOptions
    );
  }

  datCuentasDetraccion(iIdPersona) {
    this.insertHead()
    return this.http.get(
      `${this.baseUrl}/tesoreria/tablasMaestras/cuentasDetraccion/datCuentasDetraccion/${iIdPersona}`,this.httpOptions
    );
  }

  savCuentasDetraccion(data) {
    this.insertHead()
    return this.http.post(
      `${this.baseUrl}/tesoreria/tablasMaestras/cuentasDetraccion/savCuentasDetraccion`,data,this.httpOptions
    );
  }

  delCuentasDetraccion(data) {
    this.insertHead()
    return this.http.post(
      `${this.baseUrl}/tesoreria/tablasMaestras/cuentasDetraccion/delCuentasDetraccion`,data,this.httpOptions
    );
  }

  importCuentasDetraccion(data) {
    this.insertHeadFiles()
    return this.http.post(
      `${this.baseUrl}/tesoreria/tablasMaestras/cuentasDetraccion/importCuentasDetraccion`,data,this.httpOptions
    );
  }
  
  /**FIN FILTRO 
  /**COMBOS INICIALES FORMULARIO CARTA FIANZA
  selTipos() {
    this.insertHead()
    return this.http.get(
      `${this.baseUrl}/tesoreria/cartasFianza/selTipos`,this.httpOptions
    );
  }
  selClases() {
    this.insertHead()
    return this.http.get(
      `${this.baseUrl}/tesoreria/cartasFianza/selClases`,this.httpOptions
    );
  }
  selEstados() {
    this.insertHead()
    return this.http.get(
      `${this.baseUrl}/tesoreria/cartasFianza/selEstados`,this.httpOptions
    );
  }
  selFases() {
    this.insertHead()
    return this.http.get(
      `${this.baseUrl}/tesoreria/cartasFianza/selFases`,this.httpOptions
    );
  }

    /**FIN COMBOS FORMULARIO 
  /**PETICIONES CARTAS FIANZA 

  selCartasFianza(data) {
   /* this.insertHead()
    return this.http.post(
      `${this.baseUrl}/tesoreria/cartasFianza/registro/selCartasFianza`,data,this.httpOptions
    );
    
  }
  datCartasFianza(iCartaFianzaId) {
    this.insertHead()
    return this.http.get(
      `${this.baseUrl}/tesoreria/cartasFianza/registro/datCartasFianza/${iCartaFianzaId}`,this.httpOptions
    );
  }
  savCartasFianza(data) {
    //this.insertHead()
    this.insertHeadFiles()
    return this.http.post(
      `${this.baseUrl}/tesoreria/cartasFianza/registro/savCartasFianza`,data,this.httpOptions
    );
  }
  delCartasFianza(data) {
    this.insertHead()
    return this.http.post(
      `${this.baseUrl}/tesoreria/cartasFianza/registro/delCartasFianza`,data,this.httpOptions
    );
  }
  dowCartasFianza(data){
    this.insertHead()
    this.httpOptions.responseType = 'blob'
    return this.http.post(`${this.baseUrl}/tesoreria/cartasFianza/registro/dowCartasFianza`,data, this.httpOptions);
  }



  selContratoPS(data) {
    this.insertHead()
    return this.http.post(
      `${this.baseUrl}/tesoreria/cartasFianza/registro/contratoPS/selContratoPS`,data,this.httpOptions
    );
  }

  selCriterioBusquedaContratoPS() {
    this.insertHead()
    return this.http.get(
      `${this.baseUrl}/tesoreria/cartasFianza/registro/contratoPS/selCriterioBusquedaContratoPS`,this.httpOptions
    );
  }

  selPeriodoConvocatoriaPS(cSec_ejec) {
    this.insertHead()
    return this.http.get(
      `${this.baseUrl}/tesoreria/cartasFianza/registro/contratoPS/selPeriodoConvocatoriaPS/${cSec_ejec}`,this.httpOptions
    );
  }  

  selPersonaProveedor(data) {
    this.insertHead()
    return this.http.post(
      `${this.baseUrl}/tesoreria/cartasFianza/registro/personaProveedor/selPersonaProveedor`,data,this.httpOptions
    );
  }
  selPersonaProveedorGeneral(data) {
    this.insertHead()
    return this.http.post(
      `${this.baseUrl}/tesoreria/globales/persona/selPersonaProveedorGeneral`,data,this.httpOptions
    );
  }

 */
  rptPDF(nameReport,id,ids,paper){
    let url = `${this.baseUrl}/rptPDF/${nameReport}/${paper}/${id}/${ids}`
    this.formatScreen(url)
  }
  
  rptEXCEL(module,nameReport,typeReport,id,ids) {
    this.insertHead()
    this.httpOptions.responseType = 'blob'
    return this.http.get(
      `${this.baseUrl}/rptEXCEL/${nameReport}/${ids}`,this.httpOptions
      );
    }
  /*
  // rptPDFCrystal(modulo,nameReport,id,ids){
  //   this.insertHead()
  //   //this.httpOptions.responseType = 'blob'
  //   return this.http.post(
  //     `${this.baseUrlCrystal}?module=${modulo}&nameReport=${nameReport}&id=${id}&ids=${ids}`,this.httpOptions
  //     );
  // }

  
  

/**FIN CARTAS FIANZA 
/**DETALLE CARTAS FIANZA 
selDetalleCartasFianza(iCartaFianzaId) {
  this.insertHead()
  return this.http.get(
    `${this.baseUrl}/tesoreria/cartasFianza/detalle/selDetalleCartasFianza/${iCartaFianzaId}`,this.httpOptions
  );
}
datDetalleCartasFianza(iCartaFianzaDetId) {
  this.insertHead()
  return this.http.get(
    `${this.baseUrl}/tesoreria/cartasFianza/detalle/datDetalleCartasFianza/${iCartaFianzaDetId}`,this.httpOptions
  );
}

savDetalleCartasFianza(data) {
  this.insertHeadFiles()
  return this.http.post(
    `${this.baseUrl}/tesoreria/cartasFianza/detalle/savDetalleCartasFianza`,data,this.httpOptions
  );
}
delDetalleCartasFianza(data) {
  this.insertHead()
  return this.http.post(
    `${this.baseUrl}/tesoreria/cartasFianza/detalle/delDetalleCartasFianza`,data,this.httpOptions
  );
}


selFaseSIAF(data) {
  this.insertHead()
  return this.http.post(
    `${this.baseUrl}/tesoreria/globales/selFaseSIAF`,data,this.httpOptions
  );
}

selSumCiclo(data) {
  this.insertHead()
  return this.http.post(
    `${this.baseUrl}/tesoreria/globales/selSumCiclo`,data,this.httpOptions
  );
}

selExpedienteClasificador(data) {
  this.insertHead()
  return this.http.post(
    `${this.baseUrl}/tesoreria/globales/selExpedienteClasificador`,data,this.httpOptions
  );
}
selExpedienteClasificadorDatos(data) {
  this.insertHead()
  return this.http.post(
    `${this.baseUrl}/tesoreria/globales/selExpedienteClasificadorDatos`,data,this.httpOptions
  );
}
selExpedienteSecuenciaFuncional(data) {
  this.insertHead()
  return this.http.post(
    `${this.baseUrl}/tesoreria/globales/selExpedienteSecuenciaFuncional`,data,this.httpOptions
  );
}

selExpedienteCartasFianza(data) {
  this.insertHead()
  return this.http.post(
    `${this.baseUrl}/tesoreria/cartasFianza/expediente/selExpedienteCartasFianza`,data,this.httpOptions
  );
}

updFechaVencimientoCartaFianza(data) {
  this.insertHead()
  return this.http.post(
    `${this.baseUrl}/tesoreria/cartasFianza/updFechaVencimientoCartaFianza`,data,this.httpOptions
  );
}

/**FIN DETALLE CARTA FIANZA 
/**DETRACCIONES MASIVAS 
selDetraccionesMasivas(data) {
  this.insertHead()
  return this.http.post(
    `${this.baseUrl}/tesoreria/detraccionesMasivas/registro/selDetraccionesMasivas`,data,this.httpOptions
  );
}

savDetraccionesMasivas(data) {
  this.insertHead()
  return this.http.post(
    `${this.baseUrl}/tesoreria/detraccionesMasivas/registro/savDetraccionesMasivas`,data,this.httpOptions
  );
}
savDetraccionesMasivasDetalles(data) {
  this.insertHead()
  return this.http.post(
    `${this.baseUrl}/tesoreria/detraccionesMasivas/detalle/savDetraccionesMasivasDetalles`,data,this.httpOptions
  );
}

selDetraccionesMasivasDetalles(data) {
  this.insertHead()
  return this.http.post(
    `${this.baseUrl}/tesoreria/detraccionesMasivas/detalle/selDetraccionesMasivasDetalles`,data,this.httpOptions
    );
  }


datDetraccionesMasivas(iDetraccAdquirId) {
  this.insertHead()
  return this.http.get(
    `${this.baseUrl}/tesoreria/detraccionesMasivas/registro/datDetraccionesMasivas/${iDetraccAdquirId}`,this.httpOptions
  );
}

datDetraccionesMasivasDetalles(iDetraccProveedId) {
  this.insertHead()
  return this.http.get(
    `${this.baseUrl}/tesoreria/detraccionesMasivas/detalle/datDetraccionesMasivasDetalles/${iDetraccProveedId}`,this.httpOptions
  );
}

delDetraccionesMasivas(data) {
  this.insertHead()
  return this.http.post(
    `${this.baseUrl}/tesoreria/detraccionesMasivas/registro/delDetraccionesMasivas`,data,this.httpOptions
  );
}

delDetraccionesMasivasDetalles(data) {
  this.insertHead()
  return this.http.post(
    `${this.baseUrl}/tesoreria/detraccionesMasivas/detalle/delDetraccionesMasivasDetalles`,data,this.httpOptions
  );
}


selPeriodosDetracciones() {
  this.insertHead()
  return this.http.get(
    `${this.baseUrl}/tesoreria/detraccionesMasivas/selPeriodosDetracciones`,this.httpOptions
  );
}
selMesesDetracciones() {
  this.insertHead()
  return this.http.get(
    `${this.baseUrl}/tesoreria/detraccionesMasivas/selMesesDetracciones`,this.httpOptions
  );
}
selEstadoDetracciones() {
  this.insertHead()
  return this.http.get(
    `${this.baseUrl}/tesoreria/detraccionesMasivas/selEstadoDetracciones`,this.httpOptions
  );
}

selServSujDetracciones() {
  this.insertHead()
  return this.http.get(
    `${this.baseUrl}/tesoreria/detraccionesMasivas/selServSujDetracciones`,this.httpOptions
  );
}

selOperSujDetracciones() {
  this.insertHead()
  return this.http.get(
    `${this.baseUrl}/tesoreria/detraccionesMasivas/selOperSujDetracciones`,this.httpOptions
  );
}

selTipoDocDetracciones() {
  this.insertHead()
  return this.http.get(
    `${this.baseUrl}/tesoreria/detraccionesMasivas/selTipoDocDetracciones`,this.httpOptions
  );
}

selPeriodoEjecucionSIAF(cSec_ejec) {
  this.insertHead()
  return this.http.get(
    `${this.baseUrl}/tesoreria/detraccionesMasivas/importacionSIAF/selPeriodoEjecucionSIAF/${cSec_ejec}`,this.httpOptions
  );
}

selExpedientesFasesDetraccion(data) {
  this.insertHead()
  return this.http.post(
    `${this.baseUrl}/tesoreria/detraccionesMasivas/importacionSIAF/selExpedientesFasesDetraccion`,data,this.httpOptions
  );
}
savExpedientesFasesDetraccion(data) {
  this.insertHead()
  return this.http.post(
    `${this.baseUrl}/tesoreria/detraccionesMasivas/importacionSIAF/savExpedientesFasesDetraccion`,data,this.httpOptions
  );
}

generateTXTSunatDepositoMasivo(data) {
  this.insertHead()
  // this.httpOptions.responseType = 'blob'
  return this.http.post(
    `${this.baseUrl}/tesoreria/detraccionesMasivas/generacionTXT/generateTXTSunatDepositoMasivo`,data,this.httpOptions
  );
}

dowTXTSunatDepositoMasivo(data) {
  this.insertHead()
  this.httpOptions.responseType = 'blob'
  return this.http.post(
    `${this.baseUrl}/tesoreria/detraccionesMasivas/generacionTXT/dowTXTSunatDepositoMasivo`,data,this.httpOptions
  );
}


/**FIN DETRACCIONES 
/**COMPROBANTES DE PAGO 
    /**REGISTRO 
    savComprobantesPago(data) {
      this.insertHead()
      return this.http.post(
        `${this.baseUrl}/tesoreria/comprobantesPago/registroCP/savComprobantesPago`,data,this.httpOptions
      );
    }


    datComprobantesPago(iTramId) {
      this.insertHead()
      return this.http.get(
        `${this.baseUrl}/tesoreria/comprobantesPago/registroCP/datComprobantesPago/${iTramId}`,this.httpOptions
      );
    }

    importDatosSIAFComprobantesPago(data) {
      this.insertHead()
      return this.http.post(
        `${this.baseUrl}/tesoreria/comprobantesPago/registroCP/importDatosSIAFComprobantesPago`,data,this.httpOptions
      );
    }
    delComprobantesPago(data) {
      this.insertHead()
      return this.http.post(
        `${this.baseUrl}/tesoreria/comprobantesPago/registroCP/delComprobantesPago`,data,this.httpOptions
      );
    }

    savComprobantesPagoMultiple(data) {
      this.insertHead()
      return this.http.post(
        `${this.baseUrl}/tesoreria/comprobantesPago/registroCP/savComprobantesPagoMultiple`,data,this.httpOptions
      );
    }

    /**FIN REGISTRO 
    /** FILTRO COMPROBANTES 
    selTiposComprobantes() {
      this.insertHead()
      return this.http.get(
        `${this.baseUrl}/tesoreria/comprobantesPago/selTiposComprobantes`,this.httpOptions
      );
    }
    selPeriodosCP() {
      this.insertHead()
      return this.http.get(
        `${this.baseUrl}/tesoreria/comprobantesPago/selPeriodosCP`,this.httpOptions
      );
    }

    selMesesCP() {
      this.insertHead()
      return this.http.get(
        `${this.baseUrl}/tesoreria/comprobantesPago/selMesesCP`,this.httpOptions
      );
    }
    selCriterioCP() {
      this.insertHead()
      return this.http.get(
        `${this.baseUrl}/tesoreria/comprobantesPago/selCriterioCP`,this.httpOptions
      );
    }

    
    selComprobantesPago(data) {
      this.insertHead()
      return this.http.post(
        `${this.baseUrl}/tesoreria/comprobantesPago/registroCP/selComprobantesPago`,data,this.httpOptions
      );
    }



    
    /** FIN FILTRO COMPROBANTES 
    /**TIPOS CHEQUERAS 
    selTiposChequeras() {
      this.insertHead()
      return this.http.get(
        `${this.baseUrl}/tesoreria/comprobantesPago/tiposChequeras/selTiposChequeras`,this.httpOptions
      );
    }
    datTiposChequeras(iTipoChequeraId) {
      this.insertHead()
      return this.http.get(
        `${this.baseUrl}/tesoreria/comprobantesPago/tiposChequeras/datTiposChequeras/${iTipoChequeraId}`,this.httpOptions
      );
    }

    savTiposChequeras(data) {
      this.insertHead()
      return this.http.post(
        `${this.baseUrl}/tesoreria/comprobantesPago/tiposChequeras/savTiposChequeras`,data,this.httpOptions
      );
    }

    delTiposChequeras(data) {
      this.insertHead()
      return this.http.post(
        `${this.baseUrl}/tesoreria/comprobantesPago/tiposChequeras/delTiposChequeras`,data,this.httpOptions
      );
    }
    /**FIN TIPOS CHEQUERAS */
/** FIN COMPROBANTES DE PAGO */

/** RECIBOS SALDOS 
    /**FILTRO 
    selTiposRecibosRS(){
      this.insertHead()
      return this.http.get(
        `${this.baseUrl}/tesoreria/recibosSaldos/registroRS/selTiposRecibosRS`,this.httpOptions
      );
    }  
    
    selPeriodosRS() {
      this.insertHead()
      return this.http.get(
        `${this.baseUrl}/tesoreria/recibosSaldos/registroRS/selPeriodosRS`,this.httpOptions
      );
    }

    selMesesRS() {
      this.insertHead()
      return this.http.get(
        `${this.baseUrl}/tesoreria/recibosSaldos/registroRS/selMesesRS`,this.httpOptions
      );
    }
    selCriterioRS() {
      this.insertHead()
      return this.http.get(
        `${this.baseUrl}/tesoreria/recibosSaldos/registroRS/selCriterioRS`,this.httpOptions
      );
    }

    selRecibosSaldos(data) {
      this.insertHead()
      return this.http.post(
        `${this.baseUrl}/tesoreria/recibosSaldos/registroRS/selRecibosSaldos`,data,this.httpOptions
      );
    }
    /**FIN FILTRO 
    /**REGISTRO 
    savRecibosSaldos(data){
      this.insertHead()
      return this.http.post(
        `${this.baseUrl}/tesoreria/recibosSaldos/registroRS/savRecibosSaldos`,data,this.httpOptions
      );
    }
    datRecibosSaldos(iRecId){
      this.insertHead()
      return this.http.get(
        `${this.baseUrl}/tesoreria/recibosSaldos/registroRS/datRecibosSaldos/${iRecId}`,this.httpOptions
      );
    }
    delRecibosSaldos(data){
      this.insertHead()
      return this.http.post(
        `${this.baseUrl}/tesoreria/recibosSaldos/registroRS/delRecibosSaldos`,data,this.httpOptions
      );
    }

    selTiposPagosRS(){
      this.insertHead()
      return this.http.get(
        `${this.baseUrl}/tesoreria/recibosSaldos/registroRS/selTiposPagosRS`,this.httpOptions
      );
    } 

    selConceptoReciboRS(){
      this.insertHead()
      return this.http.get(
        `${this.baseUrl}/tesoreria/recibosSaldos/registroRS/selConceptoReciboRS`,this.httpOptions
      );
    } 

    

    /**FIN REGISTRO RS */
    /**DETALLE RS

    selRecibosSaldosDetalle(iRecId){
      this.insertHead()
      return this.http.get(
        `${this.baseUrl}/tesoreria/recibosSaldos/detalleRS/selRecibosSaldosDetalle/${iRecId}`,this.httpOptions
      );
    } 

    savRecibosSaldosDetalle(data){
      this.insertHead()
      return this.http.post(
        `${this.baseUrl}/tesoreria/recibosSaldos/detalleRS/savRecibosSaldosDetalle`,data,this.httpOptions
      );
    }
    datRecibosSaldosDetalle(iRecDetId){
      this.insertHead()
      return this.http.get(
        `${this.baseUrl}/tesoreria/recibosSaldos/detalleRS/datRecibosSaldosDetalle/${iRecDetId}`,this.httpOptions
      );
    }
    delRecibosSaldosDetalle(data){
      this.insertHead()
      return this.http.post(
        `${this.baseUrl}/tesoreria/recibosSaldos/detalleRS/delRecibosSaldosDetalle`,data,this.httpOptions
      );
    }    
    
    
    
    

    /**FIN DETALLE RS */
    /**RECIBO CONCEPTOS 
    selConceptosRecibos(){
      this.insertHead()
      return this.http.get(
        `${this.baseUrl}/tesoreria/recibosSaldos/conceptosRecibos/selConceptosRecibos`,this.httpOptions
      );
    }
    savConceptosRecibos(data){
      this.insertHead()
      return this.http.post(
        `${this.baseUrl}/tesoreria/recibosSaldos/conceptosRecibos/savConceptosRecibos`,data,this.httpOptions
      );
    }
    datConceptosRecibos(iConcepRecId){
      this.insertHead()
      return this.http.get(
        `${this.baseUrl}/tesoreria/recibosSaldos/conceptosRecibos/datConceptosRecibos/${iConcepRecId}`,this.httpOptions
      );
    }
    delConceptosRecibos(data){
      this.insertHead()
      return this.http.post(
        `${this.baseUrl}/tesoreria/recibosSaldos/conceptosRecibos/delConceptosRecibos`,data,this.httpOptions
      );
    }
    selTiposRecibos(){
      this.insertHead()
      return this.http.get(
        `${this.baseUrl}/tesoreria/recibosSaldos/conceptosRecibos/selTiposRecibos`,this.httpOptions
      );
    }  

     
    /**FIN RECIBOs CONCEPTOS */
/** FIN RECIBOs SALDOS */
/**INGRESOS CONSOLIDADOS */
    /** FILTRO 
    selTiposRecibosIC(){
      this.insertHead()
      return this.http.get(
        `${this.baseUrl}/tesoreria/ingresosConsolidados/registroIC/selTiposRecibosIC`,this.httpOptions
      );
    }  
    
    selPeriodosIC() {
      this.insertHead()
      return this.http.get(
        `${this.baseUrl}/tesoreria/ingresosConsolidados/registroIC/selPeriodosIC`,this.httpOptions
      );
    }

    selMesesIC() {
      this.insertHead()
      return this.http.get(
        `${this.baseUrl}/tesoreria/ingresosConsolidados/registroIC/selMesesIC`,this.httpOptions
      );
    }
    selCriterioIC() {
      this.insertHead()
      return this.http.get(
        `${this.baseUrl}/tesoreria/ingresosConsolidados/registroIC/selCriterioIC`,this.httpOptions
      );
    }

    selIngresosConsolidados(data) {
      this.insertHead()
      return this.http.post(
        `${this.baseUrl}/tesoreria/ingresosConsolidados/registroIC/selIngresosConsolidados`,data,this.httpOptions
      );
    }
    /**FIN FILTRO */
    /**REGISTRO 
    savIngresosConsolidados(data){
      this.insertHead()
      return this.http.post(
        `${this.baseUrl}/tesoreria/ingresosConsolidados/registroIC/savIngresosConsolidados`,data,this.httpOptions
      );
    }
    datIngresosConsolidados(iRecId){
      this.insertHead()
      return this.http.get(
        `${this.baseUrl}/tesoreria/ingresosConsolidados/registroIC/datIngresosConsolidados/${iRecId}`,this.httpOptions
      );
    }
    delIngresosConsolidados(data){
      this.insertHead()
      return this.http.post(
        `${this.baseUrl}/tesoreria/ingresosConsolidados/registroIC/delIngresosConsolidados`,data,this.httpOptions
      );
    }
    selTiposDocumentoRecibo() {
      this.insertHead()
      return this.http.get(
        `${this.baseUrl}/tesoreria/ingresosConsolidados/registroIC/selTiposDocumentoRecibo`,this.httpOptions
      );
    }
    selFilialesIC() {
      this.insertHead()
      return this.http.get(
        `${this.baseUrl}/tesoreria/ingresosConsolidados/registroIC/selFilialesIC`,this.httpOptions
      );
    }    

    selPeriodoEjecucionSIAFIC(cSec_ejec) {
      this.insertHead()
      return this.http.get(
        `${this.baseUrl}/tesoreria/ingresosConsolidados/registroIC/selPeriodoEjecucionSIAFIC/${cSec_ejec}`,this.httpOptions
      );
    }    
  /**FIN REGISTRO */
        
/**FIN INGRESOS CONSOLIDADOS */
/**FUNCIONES GLOBALES 

datEntidad(iEntId) {
  this.insertHead()
  return this.http.get(
    `${this.baseUrl}/tesoreria/detraccionesMasivas/datEntidad/${iEntId}`,this.httpOptions
  );
}
*/
formatScreen(url)
{
  let w = window.open('about:blank');
  w.document.open();
  w.document.write('<body style="background-color:#eeeeee"><h3 style="color:blue">Generando el PDF...espere</h3></body>')
  w.document.write(`<iframe src="${url}" style="position: fixed;top: 0px;bottom: 0px;right: 0px;width: 100%;border: none;margin: 0;padding: 0;overflow: hidden;z-index: 999999;height: 100%;"></iframe>`);
  w.document.close();
}
/*
selEspecificasGI(data) {
  this.insertHead()
  return this.http.post(
    `${this.baseUrl}/tesoreria/globales/selPersonaEspecificasGI`,data,this.httpOptions
  );
}

datPersonaProveedor(iPersId) {
  this.insertHead()
  return this.http.get(
    `${this.baseUrl}/tesoreria/globales/persona/datPersonaProveedor/${iPersId}`,this.httpOptions
  );
}

savPersonaProveedor(data) {
  this.insertHeadFiles()
  return this.http.post(
    `${this.baseUrl}/tesoreria/globales/persona/savPersonaProveedor`,data,this.httpOptions
  );
}
delPersonaProveedor(data) {
  this.insertHead()
  return this.http.post(
    `${this.baseUrl}/tesoreria/globales/persona/delPersonaProveedor`,data,this.httpOptions
  );
}

selTipoIdentificacion() {
  this.insertHead()
  return this.http.get(
    `${this.baseUrl}/tesoreria/globales/persona/selTipoIdentificacion`,this.httpOptions
  );
}
  getMisCiclosData() {
    this.getCod();
    return this.http.get(
      `${this.baseUrl}/estudiante/matricula/obtenerSemestresAcademicosEstudiante/${this.codEstu}`,
    );
  }
  
  sendProforma(data) {
    return this.http.post(`${this.baseUrl}/estudiante/matricula/guardarProforma`, data);
  }
  getHorarioMatricula(carreraId, filialId) {
    let data = {
      carreraId: carreraId,
      filialId: filialId,
    };
    return this.http.get(`${this.baseUrl}/ura/docente/obtenerConfigHorarioCarrera`, {
      params: data,
    });
  }
  editcheck(modo, check, cod, ciclo) {
    let data = {
      modo: modo,
      check: check,
      codUniv: cod,
      cicloAcad: ciclo,
    };
    return this.http.post(`${this.baseUrl}/dbu/control/guardarActualizarChecksObu`, data);
  }
  
  getFilter() {
    this.insertHead()
    return this.http.get(
      `${this.baseUrl}/cotizaciones/getBusquedaCriterios`,this.httpOptions
    );
  }
  getRegistrosPedidos(secEjec,anioEjec,tipoBien,critId,critVariable,page) {
    this.insertHead()
    if(!critVariable){
      critVariable = null
    }
    return this.http.get(
      `${this.baseUrl}/cotizaciones/getPedidosSIGA/${secEjec}/${anioEjec}/${tipoBien}/${critId}/${critVariable}/15/${page}`,this.httpOptions
    );
  }
  getRegistrosPedidosDetalle(secEjec,anioEjec,tipoBien,tipoPedido,nroPedido) {
    this.insertHead()
    return this.http.get(
      `${this.baseUrl}/cotizaciones/getDetallesPedidosSIGA/${secEjec}/${anioEjec}/${tipoBien}/${tipoPedido}/${nroPedido}`,this.httpOptions
    );
  }
  insertPedido(data){
    this.insertHead()
    return this.http.post(`${this.baseUrl}/cotizaciones/pedidosEnLinea/insertarPedido`, data,this.httpOptions);
  }
  getBandeja(cotizadorId,page,pageSize){
    this.insertHead()
    return this.http.get(`${this.baseUrl}/cotizaciones/pedidosEnLinea/getPedidosCotizador/${cotizadorId}/${page}/${pageSize}`,this.httpOptions);
  }
  getPedidosSg(secEjec,year,critId,critVariable,page,pageSize){
    this.insertHead()
    return this.http.get(`${this.baseUrl}/cotizaciones/pedidosEnLinea/getCuadroAdquisiciones/${secEjec}/${year}/${critId}/${critVariable}/${page}/${pageSize}`,this.httpOptions);

  }
  getCriterios(){
    this.insertHead()
    return this.http.get(`${this.baseUrl}/cotizaciones/pedidosEnLinea/getCriteriosBusqueda`,this.httpOptions);
  }
  getDetallePedido(secEjec,anioEjec,tipoBien,secCuadro){
    this.insertHead()
    return this.http.get(`${this.baseUrl}/cotizaciones/pedidosEnLinea/getDetallesPedido/${secEjec}/${anioEjec}/${tipoBien}/${secCuadro}`,this.httpOptions);
  }
  getDetallePedidoCuadro(anioEjec,tipoBien,secCuadro){
    this.insertHead()
    return this.http.get(`${this.baseUrl}/cotizaciones/pedidosEnLinea/getDetallesPedidoSecCuadro/${anioEjec}/${tipoBien}/${secCuadro}`,this.httpOptions);
  }
  getDetallesAnexo(secEjec,anioEjec,tipoBien,tipoPedido,nroPedido,secuencia){
    this.insertHead()
    return this.http.get(`${this.baseUrl}/cotizaciones/pedidosEnLinea/getAnexosDetalles/${secEjec}/${anioEjec}/${tipoBien}/${tipoPedido}/${nroPedido}/${secuencia}`,this.httpOptions);
  }
  postSavePublicar(data){
    this.insertHeadFiles()
    return this.http.post(`${this.baseUrl}/cotizaciones/pedidosEnLinea/insertarPublicarPedidoSecCuadro`, data, this.httpOptions);
  }
  acCerrarPedido(data){
    this.insertHead()
    return this.http.post(`${this.baseUrl}/cotizaciones/pedidosEnLinea/cerrarPedido`, data, this.httpOptions);
  }
  getProveedoresPedido(pedidoId){
    this.insertHead()
    return this.http.get(`${this.baseUrl}/cotizaciones/getCotizacionesPedido/${pedidoId}`,this.httpOptions);
  }
  getDetallesPedido(pedidoId){
    this.insertHead()
    return this.http.get(`${this.baseUrl}/cotizaciones/pedidosEnLinea/getPedidoAndDetalles/${pedidoId}`,this.httpOptions);
  }
  getDetallesCoti(cotizaId){
    this.insertHead()
    return this.http.get(`${this.baseUrl}/cotizaciones/getDetallesCotizacion/${cotizaId}`,this.httpOptions);
  }
  actualizarPedido(data){
    this.insertHeadFiles()
    return this.http.post(`${this.baseUrl}/cotizaciones/pedidosEnLinea/actualizarPedidoPublicar`, data, this.httpOptions);
  }
  /**archivo 
  dowloadCot(data){
    this.insertHead()
    this.httpOptions.responseType = 'blob'
    return this.http.post(`${this.baseUrl}/cotizaciones/getCotizacionPDF`,data, this.httpOptions);
  }
  generateCuadro(data){
    this.insertHead()
    return this.http.post(`${this.baseUrl}/cotizaciones/cuadroComparativo/insertarCuadroComparativo`,data, this.httpOptions);
  }
  getCuadro(cuadroId){
    this.insertHead()
    return this.http.get(`${this.baseUrl}/cotizaciones/getCotizacionesCuadroComparativo/${cuadroId}`,this.httpOptions);
  }
  getCotizadores(year){
    this.insertHead()
    return this.http.get(`${this.baseUrl}/cotizaciones/selCotizadoresYear/${year}`,this.httpOptions);
  }
  getNotifi(anio,cotizadorId,page,pageSize){
    this.insertHead()
    return this.http.get(`${this.baseUrl}/cotizaciones/getCotizacionesBuenaProPorCotizador/${anio}/${cotizadorId}/${page}/${pageSize}
    `, this.httpOptions);
  }
  changeBP(data){
    this.insertHead()
    return this.http.post(`${this.baseUrl}/cotizaciones/asignarBuenaPro`,data, this.httpOptions);
  }
  accNotify(data){
    this.insertHeadFiles()
    return this.http.post(`${this.baseUrl}/sendMailNotify`,data, this.httpOptions);
  }
  reNotify(data){
    this.insertHeadFiles()
    return this.http.post(`${this.baseUrl}/reSendMailNotify`,data, this.httpOptions);
  }
  getAlmacenes(){
    this.insertHead()
    return this.http.get(`${this.baseUrl}/cotizaciones/getAlmacenes`,this.httpOptions);
  }
  */
/*
  ALMACENES
*/
  getAnios(){
    console.log('Anios....')
    this.insertHead()
    return this.http.get(`${this.baseUrl}/anios`,this.httpOptions)
    .pipe(retry(2),
    catchError(this.handleError));
  }
  /**
 * FUNCIONES GLOBALES
 * getDatosApi -> Devuelve los datos solicitados por url
 * delDatoApi -> elimina el dato con el $id, a través de la url referida
 * saveDatosApi -> guarda los datos 'data' enviados por POST a la $url
 *  */

  getDatosApi(url:string){
    this.insertHead()
    return this.http.get(
      `${this.baseUrl}${url}`,this.httpOptions
    ).pipe(retry(2),
      catchError(this.handleError));
  }

  delDatoApi(url:string,id){
    this.insertHead()
    return this.http.get(
      `${this.baseUrl}${url}/${id}`,this.httpOptions
    ).pipe(retry(2),
      catchError(this.handleError));
  }
  saveDatosApi(url:string, data){
    this.insertHead()
    return this.http.post(
      `${this.baseUrl}${url}`,data,this.httpOptions
    ).pipe(retry(2),
      catchError(this.handleError));
  }
  getDatosPostApi(url:string, data){
    this.insertHead()
    console.log(data)
    return this.http.post(
      `${this.baseUrl}${url}`,data,this.httpOptions
    ).pipe(retry(2),
      catchError(this.handleError));
  }
  handleError(error: HttpErrorResponse){
    console.log(error);
    return throwError(error);
    }
  getPdfAlmacen(url){
    this.formatScreen(url);
  }
}
