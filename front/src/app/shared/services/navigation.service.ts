import { LocalService } from "./../../servicios/local.services";
import { Injectable } from "@angular/core";
import { BehaviorSubject } from "rxjs";
export interface IMenuItem {
  id?: string;
  title?: string;
  description?: string;
  type: string; // Possible values: link/dropDown/extLink
  name?: string; // Used as display text for item and title for separator type
  state?: string; // Router state
  icon?: string; // Material icon name
  tooltip?: string; // Tooltip text
  disabled?: boolean; // If true, item will not be appeared in sidenav.
  sub?: IChildItem[]; // Dropdown items
  badges?: IBadge[];
  active?: boolean;
}
export interface IChildItem {
  id?: string;
  parentId?: string;
  type?: string;
  name: string; // Display text
  state?: string; // Router state
  icon?: string;
  sub?: IChildItem[];
  active?: boolean;
}

interface IBadge {
  color: string; // primary/accent/warn/hex color codes(#fff000)
  value: string; // Display text
}

interface ISidebarState {
  sidenavOpen?: boolean;
  childnavOpen?: boolean;
}

@Injectable({
  providedIn: "root",
})
export class NavigationService {
  public sidebarState: ISidebarState = {
    sidenavOpen: true,
    childnavOpen: false,
  };
  public menuFinal: IMenuItem[] = [];
  public almacenElegido = "";
  menuItems = new BehaviorSubject<IMenuItem[]>(this.menuFinal);
  // navigation component has subscribed to this Observable
  menuItems$ = this.menuItems.asObservable();
  defaultMenu: IMenuItem[] = [
    {
      name: "SIGA",
      description: "Herramientas del SIGA",
      type: "dropDown",
      icon: "i-Building",
      sub: [
        {
          icon: "i-Repeat2",
          name: "Movimientos",
          state: "/siga/",
          type: "link",
        },
      ],
    },
    {
      name: "Mi Almacén",
      type: "dropDown",
      icon: "i-Clothing-Store",
      sub: [
        {
          icon: "i-Dropbox",
          name: "Bienes",
          type: "link",
          state: "/internamiento/bienes/34",
        },
        {
          name: "Movimientos",
          type: "dropDown",
          icon: "i-Repeat2",
          sub: [
            {
              icon: "i-Inbox-Into",
              name: "Entradas",
              type: "dropDown",
              sub: [
                {
                  icon: "i-Computer-Secure",
                  name: "Pendientes",
                  state: "/internamiento",
                  type: "link",
                },
                {
                  icon: "i-Repeat2",
                  name: "Por NEA",
                  state: "/internamiento/neas-principal",
                  type: "link",
                },
                {
                  icon: "i-Repeat2",
                  name: "Por Devolución Externo",
                  state: "/internamiento/bienes/39",
                  type: "link",
                },
                {
                  icon: "i-Repeat2",
                  name: "Por Devolución Interna",
                  state: "/internamiento/bienes/38",
                  type: "link",
                },
              ],
            },
            {
              icon: "i-Inbox-Out",
              name: "Salidas",
              type: "dropDown",
              sub: [
                {
                  icon: "i-Computer-Secure",
                  name: "Por Consumo",
                  state: "/internamiento/bienes/34",
                  type: "link",
                },
                {
                  icon: "i-Repeat2",
                  name: "Por Préstamos Interno",
                  state: "/internamiento/bienes/35",
                  type: "link",
                },

                {
                  icon: "i-Repeat2",
                  name: "Por Préstamos Externo",
                  state: "/internamiento/bienes/36",
                  type: "link",
                },

                {
                  icon: "i-Repeat2",
                  name: "NEA",
                  state: "/internamiento/bienes/37",
                  type: "link",
                },
              ],
            },
          ],
        },
        {
          name: "Mis Movimientos",
          type: "dropDown",
          icon: "i-Sync",
          sub: [
            {
              icon: "i-Inbox-Out",
              name: "Salidas Realizadas",
              type: "link",
              state: "/internamiento/salidas",
            },
            {
              icon: "i-Inbox-Out",
              name: "NEAS Realizadas",
              type: "link",
              state: "/internamiento/salidas/neas",
            },
          ],
        },
      ],
    },

    {
      name: "Combustible",
      description: "Administración de Combustible",
      type: "dropDown",
      icon: "i-Car-2",
      sub: [
        {
          icon: "i-Door",
          name: "Generar Solicitud",
          type: "link",
          state: "/combustible/generar/34",
        },
        {
          icon: "i-Up",
          name: "Solicitudes Pendientes",
          state: "/combustible/listado-prin",
          type: "link",
        },
      ],
    },
    {
      name: "Reportes",
      description: "Reportes del Módulo Almacenes",
      type: "dropDown",
      icon: "i-Bar-Chart",
      sub: [
        {
          icon: "i-File-Pictures",
          name: "Bienes",
          state: "/reportes/bienes",
          type: "link",
          // {
          //   icon: "i-Inbox-Into",
          //   name: "Entradas",
          //   state: "/control/almacenes",
          //   type: "link",
          // },
          // {
          //   icon: "i-Paper-Plane",
          //   name: "Salidas",
          //   state: "/control/almacenes",
          //   type: "link",
          // },
          // {
          //   icon: "i-Computer-Secure",
          //   name: "Saldos",
          //   state: "/control/almacenes",
          //   type: "link",
          // },
        },
        {
          icon: "i-Money-Bag",
          name: "Específica de Gastos",
          state: "/almacen/docentes",
          type: "link",
        },
        {
          icon: "i-File-Pictures",
          name: "Por Tipo de Bien",
          type: "link",
          state: "/reportes/deprepatri",
        },
      ],
    },
    // {
    //   name: "Herramientas",
    //   description: "Herramientas del Módulo Almacenes",
    //   type: "dropDown",
    //   icon: "i-Gears",
    //   sub: [
    //     {
    //       icon: "i-Safe-Box1",
    //       name: "Pre-Cierre Mensual",
    //       state: "/control/almacenes",
    //       type: "link",
    //     },
    //     {
    //       icon: "i-Safe-Box1",
    //       name: "Cierre Anual",
    //       state: "/control/almacenes",
    //       type: "link",
    //     },
    //     {
    //       icon: "i-Safe-Box1",
    //       name: "Cierre de Almacén",
    //       state: "/control/almacenes",
    //       type: "link",
    //     },
    //   ],
    // },
    {
      name: "Tablas Gral.",
      description: "Administración de Tablas base",
      type: "dropDown",
      icon: "i-Split-Vertical",
      sub: [
        {
          icon: "i-Clothing-Store",
          name: "Almacenes",
          state: "/tablas-maestras/almacenes",
          type: "link",
        },
        // { icon: 'i-Financial',             name: 'Tipos de Gasto',     state: '/tablas-maestras/tipos_gasto',  type: 'link' },
        // { icon: 'i-Business-ManWoman',     name: 'Trabajador',         state: '/tablas-maestras/trabajadores', type: 'link' },
        {
          icon: "i-File-Horizontal-Text",
          name: "Estados",
          state: "/tablas-maestras/estados",
          type: "link",
        },
        // {
        //   icon: "i-File-Horizontal-Text",
        //   name: "Tipos Docum.",
        //   state: "/tablas-maestras/tipos_doc",
        //   type: "link",
        // },
        {
          icon: "i-File-Horizontal-Text",
          name: "Estados de Nota",
          state: "/tablas-maestras/estados_nota",
          type: "link",
        },
        {
          icon: "i-Ambulance",
          name: "Tipos de Vehículos",
          state: "/tablas-maestras/tipos_veh",
          type: "link",
        },
      ],
    },
  ];
  normalMenu: IMenuItem[] = [
    {
      name: "Mi Almacén",
      type: "dropDown",
      icon: "i-Clothing-Store",
      sub: [
        {
          icon: "i-Dropbox",
          name: "Bienes",
          type: "link",
          state: "/internamiento/bienes/34",
        },
        {
          name: "Movimientos",
          type: "dropDown",
          icon: "i-Repeat2",
          sub: [
            {
              icon: "i-Inbox-Into",
              name: "Entradas",
              type: "dropDown",
              sub: [
                {
                  icon: "i-Computer-Secure",
                  name: "Pendientes",
                  state: "/internamiento",
                  type: "link",
                },
                {
                  icon: "i-Repeat2",
                  name: "Por Devolución Interna",
                  state: "/internamiento/bienes/38",
                  type: "link",
                },
                {
                  icon: "i-Repeat2",
                  name: "Por Devolución Externo",
                  state: "/internamiento/bienes/39",
                  type: "link",
                },
              ],
            },
            {
              icon: "i-Inbox-Out",
              name: "Salidas",
              type: "dropDown",
              sub: [
                {
                  icon: "i-Computer-Secure",
                  name: "Por Consumo",
                  state: "/internamiento/bienes/34",
                  type: "link",
                },
                {
                  icon: "i-Repeat2",
                  name: "Por Préstamos Interno",
                  state: "/internamiento/bienes/35",
                  type: "link",
                },

                {
                  icon: "i-Repeat2",
                  name: "Por Préstamos Externo",
                  state: "/internamiento/bienes/36",
                  type: "link",
                },

                {
                  icon: "i-Repeat2",
                  name: "NEA",
                  state: "/internamiento/bienes/37",
                  type: "link",
                },
              ],
            },
          ],
        },
        {
          name: "Mis Movimientos",
          type: "dropDown",
          icon: "i-Sync",
          sub: [
            {
              icon: "i-Inbox-Out",
              name: "Salidas Realizadas",
              type: "link",
              state: "/internamiento/salidas",
            },
            {
              icon: "i-Inbox-Out",
              name: "NEAS Realizadas",
              type: "link",
              state: "/internamiento/salidas/neas",
            },
          ],
        },
      ],
    },
    {
      name: "Combustible",
      description: "Administración de Combustible",
      type: "dropDown",
      icon: "i-Car-2",
      sub: [
        {
          icon: "i-Door",
          name: "Generar Solicitud",
          type: "link",
          state: "/combustible/generar/34",
        },
        {
          icon: "i-Up",
          name: "Solicitudes Pendientes",
          state: "/combustible/listado-sec",
          type: "link",
        },
      ],
    },
    {
      name: "Reportes",
      description: "Reportes del Módulo Almacenes",
      type: "dropDown",
      icon: "i-Bar-Chart",
      sub: [
        {
          icon: "i-File-Pictures",
          name: "Bienes",
          state: "/reportes/bienes",
          type: "link",
          // {
          //   icon: "i-Inbox-Into",
          //   name: "Entradas",
          //   state: "/control/almacenes",
          //   type: "link",
          // },
          // {
          //   icon: "i-Paper-Plane",
          //   name: "Salidas",
          //   state: "/control/almacenes",
          //   type: "link",
          // },
          // {
          //   icon: "i-Computer-Secure",
          //   name: "Saldos",
          //   state: "/control/almacenes",
          //   type: "link",
          // },
        },
        {
          icon: "i-Money-Bag",
          name: "Específica de Gastos",
          state: "/almacen/docentes",
          type: "link",
        },
        {
          icon: "i-File-Pictures",
          name: "Por Tipo de Bien",
          state: "/reportes/deprepatri",
          type: "link",
        },
      ],
    },
    // {
    //   name: "Herramientas",
    //   description: "Herramientas del Módulo Almacenes",
    //   type: "dropDown",
    //   icon: "i-Gears",
    //   sub: [
    //     {
    //       icon: "i-Safe-Box1",
    //       name: "Pre-Cierre Mensual",
    //       state: "/control/almacenes",
    //       type: "link",
    //     },
    //     {
    //       icon: "i-Safe-Box1",
    //       name: "Cierre Anual",
    //       state: "/control/almacenes",
    //       type: "link",
    //     },
    //     {
    //       icon: "i-Safe-Box1",
    //       name: "Cierre de Almacén",
    //       state: "/control/almacenes",
    //       type: "link",
    //     },
    //   ],
    // },
  ];
  constructor(private local: LocalService) {
    const alm = this.local.getItem("almacenElegido");
    console.log(alm);
    if (alm !== null) {
      this.almacenElegido = alm.nombre;
      this.publishNavigationChange(alm.id);
    }
  }

  // sets iconMenu as default;
  publishNavigationChange(menuType) {
    let menuFinal = [];
    menuType = parseInt(menuType);
    switch (menuType) {
      case 1:
        menuFinal = this.defaultMenu;
        break;
      default:
        menuFinal = this.normalMenu;
        break;
    }
    this.menuItems.next(menuFinal);
  }
}
