import { ProfileComponent } from "./../../../../../personal/profile/profile.component";
import { Component, OnInit, ViewChild } from "@angular/core";
import { NavigationService } from "../../../../services/navigation.service";
import { SearchService } from "../../../../services/search.service";
import { AuthService } from "../../../../services/auth.service";
import { LocalService } from "./../../../../../servicios/local.services";
import { QueryService } from "./../../../../../servicios/query.services";
import { environment } from "./../../../../../../environments/environment";

import * as $ from "jquery";
import { ToastrService } from "ngx-toastr";
import { NgbModal } from "@ng-bootstrap/ng-bootstrap";
@Component({
  selector: "app-header-sidebar-large",
  templateUrl: "./header-sidebar-large.component.html",
  styleUrls: ["./header-sidebar-large.component.scss"],
})
export class HeaderSidebarLargeComponent implements OnInit {
  loading: boolean;
  nombreModulo: string;
  anios: any = [];
  txtAnio: string; // ='2020'  //actualizar de acuerdo a año vigente
  resource: string;
  notifications: any[];
  codigo: any;
  codigos: any = [];
  estado: string = "";
  data: any = {};
  nombres: string = "";
  dni: string = "";
  someUrl: string = "";
  modalidad: string = "";
  carrera: string = "";

  /** alertas */
  listaCartasFianzaAlerta: any = [];
  iOpcionFiltro: number = 5;
  iEntId: number = 1;
  iDias_enquevenceran: number = 30;
  numAlert: any;
  //// CAMBIO ALMACEN
  almacenes: any = [];
  almacen: string = "";
  @ViewChild("almacenModal", {
    static: true,
  })
  modalAlmacen;
  constructor(
    public navService: NavigationService,
    public searchService: SearchService,
    private local: LocalService,
    private query: QueryService,
    private auth: AuthService,
    private toastr: ToastrService,
    private modalService: NgbModal
  ) {
    this.resource = environment.rutas.recursos;
    this.nombreModulo = environment.aplicacion.nombre;
    this.notifications = [
      {
        icon: "i-Speach-Bubble-6",
        title: "New message",
        badge: "3",
        text: "James: Hey! are you busy?",
        time: new Date(),
        status: "primary",
        link: "/chat",
      },
    ];
  }

  ngOnInit() {
    /** alertas */
    this.listarCartasFianzaAlertas();

    this.getCodigos();

    this.getAnios();
    // this.getcodigo()
    this.getNombres();
    this.getFoto();
  }
  modalOpen(modal) {
    this.modalService.open(modal);
  }
  getAnios() {
    // this.local.setItem('cargado',false)
    this.loading = true;
    this.query.getAnios().subscribe(
      (data) => {
        this.anios = data;
        this.txtAnio = data[0].iYearId;
        this.local.setItem("anio", this.txtAnio);

        this.loading = false;
      },
      (error) => {
        console.log(error);
        this.toastr.error(
          "No se pudo recuperar los AÑOS. Contáctese con el administrador...",
          "ERROR!"
        );
      }
    );
    this.updateAnio();
  }
  updateAnio() {
    this.local.setItem("anio", this.txtAnio);
    console.log("Anio: " + this.txtAnio);
  }
  //// CAMBIAR ALMACEN
  cambiarAlmacen() {
    this.obtenerAccesos();
    this.modalService.open(this.modalAlmacen, {
      backdrop: "static",
      keyboard: false,
    });
  }
  elegirAlmacen($event) {
    console.log($event);
    this.navService.almacenElegido = $event.value.nombre;
    this.navService.publishNavigationChange($event.value.id);
    this.local.setItem("almacenElegido", $event.value);
    this.modalService.dismissAll();
    location.reload();
  }
  obtenerAccesos() {
    this.almacenes = this.local.getItem("almacenes");
  }
  /// FIN ALMACENES
  listarCartasFianzaAlertas() {
    /*  let data = {
        iOpcionFiltro:this.iOpcionFiltro,
        iEntId:this.iEntId,
        iDias_enquevenceran:this.iDias_enquevenceran, 
      }
      this.query.selCartasFianza(data).subscribe(
        data => {
            this.listaCartasFianzaAlerta = data
            this.numAlert=this.listaCartasFianzaAlerta.length
          },
        error =>{
          this.toastr.warning(error['error']['message'].substring(71,error['error']['message'].indexOf("(SQL:")),'Importante');
        }
      )
  */
  }
  limpiarAlerta() {
    this.numAlert = "";
  }

  toggelSidebar() {
    const state = this.navService.sidebarState;
    if (state.childnavOpen && state.sidenavOpen) {
      return (state.childnavOpen = false);
    }
    if (!state.childnavOpen && state.sidenavOpen) {
      return (state.sidenavOpen = false);
    }
    if (!state.sidenavOpen && !state.childnavOpen) {
      state.sidenavOpen = true;
      setTimeout(() => {
        state.childnavOpen = true;
      }, 50);
    }
  }
  modCodigo() {
    this.local.setItem("codigo", this.codigo);
    window.location.reload();
  }
  getcodigo() {
    this.codigo = this.local.getItem("codigo");
    this.getFoto();
    if (this.codigo == null || this.codigo == undefined) {
      if (this.codigos.length == 0) {
        this.codigo = this.codigos[0].cod;
        this.modalidad = this.codigos[0].mod;
        this.carrera = this.codigos[0].carrera;
        this.estado = this.codigos[0].estado;
      } else {
        let band = 0;
        for (let x in this.codigos) {
          if (this.codigos[x].iClasificId == 3) {
            this.codigo = this.codigos[x].cod;
            this.modalidad = this.codigos[x].mod;
            this.carrera = this.codigos[0].carrera;
            this.estado = this.codigos[0].estado;
            band++;
            break;
          }
        }
        if (band == 0) {
          this.codigo = this.codigos[0].cod;
          this.modalidad = this.codigos[0].mod;
          this.carrera = this.codigos[0].carrera;
          this.estado = this.codigos[0].estado;
        }
      }
    } else {
      for (let j in this.codigos) {
        if (this.codigos[j].cod == this.codigo) {
          // console.log(this.codigos[j].mod)
          this.modalidad = this.codigos[j].mod;
          this.carrera = this.codigos[j].carrera;
          this.estado = this.codigos[j].estado;
        }
      }
    }
    this.local.setItem("codigo", this.codigo);
  }
  getCodigos() {
    this.data = this.local.getItem("userInfo");
    for (let x in this.data["estudiante"]) {
      this.codigos.push({
        cod: this.data["estudiante"][x].cEstudCodUniv,
        iClasificId: this.data["estudiante"][x].iClasificId,
        mod: this.data["estudiante"][x].cModalDsc,
        carrera: this.data["estudiante"][x].cCarreraDsc,
        estado: this.data["estudiante"][x].cClasificDsc,
      });
    }
  }
  signout() {
    this.auth.signout();
  }
  getNombres() {
    let data = this.local.getItem("userInfo");
    if (data != undefined || data != null) {
      this.nombres =
        data["grl_persona"]["cPersNombre"] +
        "  " +
        data["grl_persona"]["cPersPaterno"];
      this.dni = data["grl_persona"]["cPersDocumento"];
      // this.getFoto()
    }
  }
  getFoto() {
    // this.query.getFoto()
    // .subscribe(
    //   data=>{
    //     this.someUrl = this.resource + data['data'].foto
    //   }
    // )
  }
  updateUrl() {
    this.someUrl = "./assets/images/iconos/student.png";
  }
}
