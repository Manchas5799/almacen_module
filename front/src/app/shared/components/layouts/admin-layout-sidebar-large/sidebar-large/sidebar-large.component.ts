import { Component, OnInit, HostListener } from '@angular/core';
import { NavigationService, IMenuItem, IChildItem } from '../../../../services/navigation.service';
import { Router, NavigationEnd } from '@angular/router';
import { LocalService } from './../../../../../servicios/local.services'
import { filter } from 'rxjs/operators';
import { Utils } from '../../../../utils';
import * as $ from 'jquery';
@Component({
  selector: 'app-sidebar-large',
  templateUrl: './sidebar-large.component.html',
  styleUrls: ['./sidebar-large.component.scss']
})
export class SidebarLargeComponent implements OnInit {

    selectedItem: IMenuItem;
	nav: IMenuItem[];
	perfiles:any = []

	constructor(
		public router: Router,
		private local:LocalService,
		public navService: NavigationService
	) { }

	ngOnInit() {
		this.updateSidebar();
		// CLOSE SIDENAV ON ROUTE CHANGE
		this.router.events.pipe(filter(event => event instanceof NavigationEnd))
			.subscribe((routeChange) => {
				this.closeChildNav();
				if (Utils.isMobile()) {
					this.navService.sidebarState.sidenavOpen = false;
				}
			});

		this.navService.menuItems$
			.subscribe((items) => {
                this.nav = items;
				this.setActiveFlag();
			});
		this.getPerfiles()
	}
	getPerfiles(){
		let perfil
		let preData = this.local.getItem('userInfo')
		preData['seg_credenciales_perfiles'].forEach(e => {
			perfil = {
				id: e.iPerfilId,
				nombre: e.seg_perfil.cPerfilNombre,
				idModulo: e.seg_perfil.seg_perfiles_modulos[0].seg_modulo.iModuloId,
				nombreModulo: e.seg_perfil.seg_perfiles_modulos[0].seg_modulo.cModuloNombre,
			}
			this.perfiles.push(perfil)
			//console.log(e)
		});
		console.log(this.perfiles)
	}
	stateProfile(boxPerfil){
		let open = 0
		boxPerfil.forEach(b => {
			this.perfiles.forEach( e => {
				if(b == e)
					open++
			});
		});
		
		if(open == 0)
			return false
		else
			return true
	}
	selectItem(item) {
		this.navService.sidebarState.childnavOpen = true;
		this.selectedItem = item;
		this.setActiveMainItem(item);
	}
		
	closeChildNav() {
		this.navService.sidebarState.childnavOpen = false;
		this.setActiveFlag();
	}

	onClickChangeActiveFlag(item) {
		this.setActiveMainItem(item);
	}
	setActiveMainItem(item) {
		this.nav.forEach(item => {
			item.active = false;
		});
		item.active = true;
	}

	setActiveFlag() {
		if (window && window.location) {
            const activeRoute = window.location.hash || window.location.pathname;
			this.nav.forEach(item => {
				item.active = false;
				if (activeRoute.indexOf(item.state) !== -1) {
                    this.selectedItem = item;
					item.active = true;
				}
				if (item.sub) {
					item.sub.forEach(subItem => {
                        subItem.active = false;
						if (activeRoute.indexOf(subItem.state) !== -1) {
                            this.selectedItem = item;
                            item.active = true;
                        }
                        if (subItem.sub) {
                            subItem.sub.forEach(subChildItem => {
                                if (activeRoute.indexOf(subChildItem.state) !== -1) {
                                    this.selectedItem = item;
                                    item.active = true;
                                    subItem.active = true;
                                }
                            });
                        }
					});
				}
            });
		}
    }

	updateSidebar() {
		if (Utils.isMobile()) {
			this.navService.sidebarState.sidenavOpen = false;
			this.navService.sidebarState.childnavOpen = false;
		} else {
			this.navService.sidebarState.sidenavOpen = true;
		}
	}

	@HostListener('window:resize', ['$event'])
	onResize(event) {
		this.updateSidebar();
    }

}
