import { BlankLayoutComponent } from "./shared/components/layouts/blank-layout/blank-layout.component";
import { NgModule } from "@angular/core";
import { Routes, RouterModule } from "@angular/router";
import { AuthLayoutComponent } from "./shared/components/layouts/auth-layout/auth-layout.component";
import { AuthGaurd } from "./shared/services/auth.gaurd";
import { AdminLayoutSidebarLargeComponent } from "./shared/components/layouts/admin-layout-sidebar-large/admin-layout-sidebar-large.component";
//import {ResaltarPipe} from './global/pipes/resaltar.pipes'

const moduleRoutes: Routes = [
  {
    path: "user",
    loadChildren: () =>
      import("./personal/personal.module").then((m) => m.PersonalModule),
  },
  {
    path: "tablas-maestras",
    loadChildren: () =>
      import("./subModulos/tablas-maestras/tablas-maestras.module").then(
        (m) => m.TablasMaestrasModule
      ),
  },
  {
    path: "siga",
    loadChildren: () =>
      import("./subModulos/siga/siga.module").then((m) => m.SigaModule),
  },
  {
    path: "internamiento",
    loadChildren: () =>
      import("./subModulos/internamiento/internamiento.module").then(
        (m) => m.InternamientoModule
      ),
  },
  {
    path: "combustible",
    loadChildren: () =>
      import("./subModulos/combustible/combustible.module").then(
        (m) => m.CombustibleModule
      ),
  },
  {
    path: "reportes",
    loadChildren: () =>
      import("./subModulos/reportes-alm/reportes-alm.module").then(
        (m) => m.ReportesAlmModule
      ),
  },
];
const routes: Routes = [
  {
    path: "",
    redirectTo: "user/perfil",
    pathMatch: "full",
  },

  {
    path: "",
    component: AuthLayoutComponent,
    children: [
      {
        path: "sessions",
        loadChildren: () =>
          import("./views/sessions/sessions.module").then(
            (m) => m.SessionsModule
          ),
      },
    ],
  },
  {
    path: "",
    component: BlankLayoutComponent,
    children: [
      {
        path: "icons1",
        loadChildren: () =>
          import("./views/icons/icons.module").then((m) => m.IconsModule),
      },
    ],
  },
  {
    path: "",
    component: AdminLayoutSidebarLargeComponent,
    canActivate: [AuthGaurd],
    children: moduleRoutes,
  },
  {
    path: "**",
    redirectTo: "others/404",
  },
];

@NgModule({
  imports: [RouterModule.forRoot(routes, { useHash: true })],
  exports: [RouterModule],
})
export class AppRoutingModule {}
