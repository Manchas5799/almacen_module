import { ResetPasswordComponent } from './reset-password/reset-password.component';
import { ProfileComponent } from './profile/profile.component';
import { LOCALE_ID, NgModule } from "@angular/core";
import { CommonModule } from "@angular/common";

import { PersonalRutasModule } from "./personal-rutas.module";

import { NgxEchartsModule } from "ngx-echarts";
import { SharedComponentsModule } from "src/app/shared/components/shared-components.module";
import { NgxDatatableModule } from "@swimlane/ngx-datatable";

import { FilterPipeModule } from "ngx-filter-pipe";
import { NgbModule } from "@ng-bootstrap/ng-bootstrap";
import { MatButtonModule } from "@angular/material/button";
import { MatFormFieldModule } from "@angular/material/form-field";
import { MatIconModule } from "@angular/material/icon";
import { MatInputModule } from "@angular/material/input";
import { MatSelectModule } from "@angular/material/select";
import { FormsModule } from '@angular/forms';

@NgModule({
  declarations: [ProfileComponent, ResetPasswordComponent],
  imports: [
    CommonModule,
    PersonalRutasModule,
    NgxEchartsModule,
    SharedComponentsModule,
    NgxDatatableModule,
    NgbModule,
    MatSelectModule,
    FormsModule,
    MatInputModule,
    MatFormFieldModule,
    MatButtonModule,
    MatIconModule,
  ],
  providers: [ { provide: LOCALE_ID, useValue: 'es' } ],
})
export class PersonalModule {}
