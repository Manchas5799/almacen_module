import { QueryService } from "./../../servicios/query.services";
import { NavigationService } from "./../../shared/services/navigation.service";
import { Component, OnInit, ViewChild } from "@angular/core";
import { LocalService } from "./../../servicios/local.services";
import { NgbModal } from "@ng-bootstrap/ng-bootstrap";
import { EChartOption } from "echarts";
import { echartStyles } from "../../shared/echart-styles";
import { environment } from "src/environments/environment";
import { MatSelectChange } from "@angular/material/select";

@Component({
  selector: "app-profile",
  templateUrl: "./profile.component.html",
  styleUrls: ["./profile.component.scss"],
})
export class ProfileComponent implements OnInit {
  nombreModulo: string;
  info: any;
  loading: boolean = false;
  codigo: string = "";
  names: string = "";
  almacenes: any = [];
  almacen: string = "";
  @ViewChild("almacenModal", {
    static: true,
  })
  modalAlmacen;
  constructor(
    private local: LocalService,
    private query: QueryService,
    private modalService: NgbModal,
    private navService: NavigationService
  ) {
    this.nombreModulo = environment.aplicacion.nombre;
  }

  ngOnInit() {
    let data = this.local.getItem("userInfo");
    this.names = `${data["grl_persona"].cPersNombre} ${data["grl_persona"].cPersPaterno} ${data["grl_persona"].cPersMaterno}`;

    this.info = data["grl_persona"];
    this.modalService.open(this.modalAlmacen, {
      backdrop: "static",
      keyboard: false,
    });
    this.obtenerAccesos();
    this.obtenerAlmacen();
  }
  obtenerAlmacen() {
    const alm = this.local.getItem("almacenElegido");
    this.almacen = alm ? alm.nombre : "";
  }
  public cambiarAlmacen() {
    this.modalService.open(this.modalAlmacen, {
      backdrop: "static",
      keyboard: false,
    });
  }
  elegirAlmacen($event) {
    console.log($event);
    this.navService.almacenElegido = $event.value.nombre;
    this.navService.publishNavigationChange($event.value.id);
    this.local.setItem("almacenElegido", $event.value);
    this.modalService.dismissAll();
    this.obtenerAlmacen();
  }
  obtenerAccesos() {
    this.loading = true;
    this.query.getDatosApi("/acceso").subscribe((res) => {
      this.local.setItem("almacenes", res);
      this.almacenes = res;
      this.loading = false;
    });
  }
}
