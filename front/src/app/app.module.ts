import { BrowserModule } from "@angular/platform-browser";
import { LOCALE_ID, NgModule } from "@angular/core";
import { BrowserAnimationsModule } from "@angular/platform-browser/animations";
import { FormsModule } from "@angular/forms";
import { AppRoutingModule } from "./app-routing.module";
import { AppComponent } from "./app.component";
import { SharedModule } from "./shared/shared.module";
import { InMemoryWebApiModule } from "angular-in-memory-web-api";
import { InMemoryDataService } from "./shared/inmemory-db/inmemory-db.service";
import { HttpClientModule, HTTP_INTERCEPTORS } from "@angular/common/http";
import { FilterPipeModule } from "ngx-filter-pipe";
import { HashLocationStrategy, registerLocaleData } from "@angular/common";
import localeEsPe from "@angular/common/locales/es-PE";
import { MsalModule, MsalInterceptor } from "@azure/msal-angular";
import { OAuthSettings } from "src/oauth";
import { LocationStrategy, PathLocationStrategy } from "@angular/common";
import { AuthService } from "./servicios/GraphMicrosoft/auth.service";
import { CalendarModule, DateAdapter } from "angular-calendar";
import { adapterFactory } from "angular-calendar/date-adapters/date-fns";
import { environment } from "src/environments/environment";
// import { NgxAgoraModule } from 'ngx-agora';

import { RealTimeService } from "./servicios/realtime.service";

import { GlobalModule } from "./global/global.module";

registerLocaleData(localeEsPe, "es");

@NgModule({
  declarations: [AppComponent],
  imports: [
    BrowserModule,
    GlobalModule,
    SharedModule,
    HttpClientModule,
    FilterPipeModule,
    FormsModule,
    BrowserAnimationsModule,
    InMemoryWebApiModule.forRoot(InMemoryDataService, {
      passThruUnknownUrl: true,
    }),
    MsalModule.forRoot({
      clientID: OAuthSettings.appId,
    }),
    CalendarModule.forRoot({
      provide: DateAdapter,
      useFactory: adapterFactory,
    }),
    // SocketIoModule.forRoot(config),
    AppRoutingModule,
    // NgxAgoraModule.forRoot({ AppID: environment.agora.appId }),
  ],
  providers: [
    RealTimeService,
    { provide: LOCALE_ID, useValue: "es" },
    { provide: LocationStrategy, useClass: HashLocationStrategy },
    AuthService,
  ],
  exports: [GlobalModule],
  bootstrap: [AppComponent],
})
export class AppModule {}
