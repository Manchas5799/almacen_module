import { Component, OnInit } from "@angular/core";
import {
  FormBuilder,
  FormControl,
  FormGroup,
  Validators,
} from "@angular/forms";
import { MatCheckboxChange } from "@angular/material/checkbox";
import { ChangeEvent } from "@ckeditor/ckeditor5-angular";
import { NgbModal } from "@ng-bootstrap/ng-bootstrap";
import * as moment from "moment";
import { ToastrService } from "ngx-toastr";
import { forkJoin, Observable, of } from "rxjs";
import { map, startWith } from "rxjs/operators";
import { FiltrosService } from "src/app/global/services/filtros.service";
import { JsPDFService } from "src/app/global/services/jsPDF.service";
import { LocalService } from "src/app/servicios/local.services";
import { QueryService } from "src/app/servicios/query.services";
import { environment } from "src/environments/environment";
import Swal from "sweetalert2";

@Component({
  selector: "app-movimientos",
  templateUrl: "./movimientos.component.html",
  styleUrls: ["./movimientos.component.scss"],
})
export class MovimientosComponent implements OnInit {
  loading = false;
  typeReport = "PDF";
  //
  movimiento: string = "";
  transaccion: string = "";
  tiposNea: any = [];
  ///
  movimientoActual: number;
  movimientoSeleccionado: any;
  fechaDetalles: string = moment().format("YYYY-MM-DD").toString();
  fechaPecosa: string = "";
  lugarDestino = new FormControl("");

  pecosaActual: string = "";
  documentoAprobador: string = "";
  tiposM = [
    {
      value: "I",
      nombre: "Ingreso",
    },
    {
      value: "S",
      nombre: "Salida",
    },
    {
      value: "T",
      nombre: "Salida Temporal",
    },
    {
      value: "P",
      nombre: "Pedido",
    },
    {
      value: "C",
      nombre: "Registro",
    },
    {
      value: "A",
      nombre: "Inventario",
    },
  ];
  tiposT: any = [];
  tiposTotal: any = [];
  ordenActual: any = null;
  detallesOrden: any = [];
  detallesMovimiento: any = [];
  detallesMovimientoGrl: any = [];
  almacenes: any = [];
  almacenesFiltrados: Observable<any>;
  formulario: FormGroup;
  formLugar: FormGroup;
  formDetallesMovimiento: FormGroup;
  formActualizarPecosa: FormGroup;
  formularioNeaSincro: FormGroup;
  ordenForm: FormGroup;
  editNeaForm: FormGroup;
  //
  listado: any = [];
  listadoActualizados: any = [];
  listadoActualizadosTotal: any = [];
  listadoTotal: any = [];
  titulo = "MOVIMIENTOS SIGA";
  tituloForm = "";
  salidaTodos: boolean = false;
  listaFunc: any = [];
  listaFuncGrl: any = [];
  //////
  campoFiltro = {
    id: "",
    tipo_mvto: "",
    nro_mvto: "",
    nro_orden: "",
    fecha_mov: "",
    nro_guia: "",
    obs: "",
    prov: "",
    glosa: "",
    lug_nombre: "",
    fecha_notif: "",
    plazo_entrega: "",
    /// DETALLES ORDEN
    nombre_seec: "",
    id_seec: "",
    // NEA
    clasif: "",
    norden: "",
    n_siaf: "",
    mayor: "",
    sbcta: "",
    arti: "",
    uni_m: "",
    prec: "",
    cant: "",
    v_t: "",
    anioo: "",
  };

  //
  url = {
    select: "/siga_movimientos",
    select_terminados: "/siga_movimientos_terminados",
    tipos: "/siga_tipos_movimientos",
    tipos_nea: "/tipo_neas",
    almacenes: "/almacenes_all",
    select_orden: "/siga_orden/",
    select_detalle: "/siga_movimientos/",
    upd_lugar_movimiento: "/lugar_movimiento",
    sincronizar: "/siga_movimientos_sincro",
    listar_neas_siga: "/siga_neas_listar",
    listar_detalles: "/siga_neas_listar_detalles",
    migrar_nea: "/siga_neas_migrar_nea",
    actualizar_pecosa: "/movimientos/pecosa/aceptar",
    actualizar_item: "/siga_movimientos_item",
    listarFunc: "/siga_sec_func/",
    actualizarMov: "/siga_movimiento",
    descarga_excel: "/siga_nea_excel",
  };
  iPageSize = environment.paginacion.tamañoPag;
  p = 1;
  pActualizados = 1;
  pDO = 1;
  pDM = 1;
  pDMP = 1;
  pENea = 1;
  pSec = 1;
  ////
  formularioActual = 1;
  nroPecosaActual = "";
  //// DATOS PARA NEAS
  numeroMaximo: number = parseInt(moment().format("YYYY").toString());
  neaSeleccionada: any = null;
  listaNeasSiga: any = [];
  listaDetallesNeaSiga: any = [];
  pLNS: number = 1;
  pDNS: number = 1;
  constructor(
    private query: QueryService,
    private filtro: FiltrosService,
    private toastr: ToastrService,
    private modal: NgbModal,
    private fb: FormBuilder,
    private jsPDF: JsPDFService,
    private local: LocalService
  ) {}

  ngOnInit() {
    this.formulario = this.fb.group({
      id: [""],
      orden: ["", Validators.required],
      guia: ["", Validators.required],
      obs: ["", Validators.required],
      noti: ["", Validators.required],
      plazo: ["", Validators.required],
    });
    this.ordenForm = this.fb.group({
      fecha: [{ value: "", disabled: true }],
      ref: [{ value: "", disabled: true }],
      prov: [{ value: "", disabled: true }],
      siaf: [{ value: "", disabled: true }],
      norden: [{ value: "", disabled: true }],
      meta: [{ value: "", disabled: true }],
      sub: [{ value: "", disabled: true }],
      igv: [{ value: "", disabled: true }],
      fac: [{ value: "", disabled: true }],
    });
    this.formLugar = this.fb.group({
      lugar: [null, Validators.required],
      movimiento: [null, Validators.required],
      fecha: [null, Validators.required],
    });
    this.formActualizarPecosa = this.fb.group({
      pecosa: ["", Validators.required],
      fecha: [moment().format("YYYY-MM-DD").toString(), Validators.required],
      lugarDestino: [null, Validators.required],
    });
    this.formularioNeaSincro = this.fb.group({
      tipo: ["", Validators.required],
      año: [
        moment().format("YYYY").toString(),
        [
          Validators.required,
          Validators.max(parseInt(moment().format("YYYY").toString())),
        ],
      ],
    });
    this.cargarTodo();
    this.almacenesFiltrados = this.lugarDestino.valueChanges.pipe(
      startWith(""),
      map((value) => this._filter(value))
    );
  }
  private _filter(value): any {
    console.log("valor", value);
    const filterValue = value; //.toLowerCase();
    if (typeof filterValue === "string") {
      return this.almacenes.filter((option) =>
        option.nombre.includes(filterValue.toUpperCase())
      );
    } else {
      return this.almacenes;
    }
  }
  displayFn(lugar?): string | undefined {
    return lugar ? lugar.lugar_nombre : undefined;
  }
  filtrarSec() {
    this.listaFunc = this.listaFuncGrl;
    this.listaFunc = this.filtro.filtroData(
      this.listaFunc,
      this.campoFiltro.id_seec,
      0
    );
    this.listaFunc = this.filtro.filtroData(
      this.listaFunc,
      this.campoFiltro.nombre_seec,
      1
    );
  }
  filtrarNea() {
    this.detallesMovimiento = this.detallesMovimientoGrl;
    this.detallesMovimiento = this.filtro.filtroData(
      this.detallesMovimiento,
      this.campoFiltro.clasif,
      4
    );
    this.detallesMovimiento = this.filtro.filtroData(
      this.detallesMovimiento,
      this.campoFiltro.norden,
      17
    );
    this.detallesMovimiento = this.filtro.filtroData(
      this.detallesMovimiento,
      this.campoFiltro.n_siaf,
      18
    );
    this.detallesMovimiento = this.filtro.filtroData(
      this.detallesMovimiento,
      this.campoFiltro.mayor,
      5
    );
    this.detallesMovimiento = this.filtro.filtroData(
      this.detallesMovimiento,
      this.campoFiltro.sbcta,
      6
    );
    this.detallesMovimiento = this.filtro.filtroData(
      this.detallesMovimiento,
      this.campoFiltro.arti,
      7
    );
    this.detallesMovimiento = this.filtro.filtroData(
      this.detallesMovimiento,
      this.campoFiltro.uni_m,
      15
    );
    this.detallesMovimiento = this.filtro.filtroData(
      this.detallesMovimiento,
      this.campoFiltro.prec,
      8
    );
    this.detallesMovimiento = this.filtro.filtroData(
      this.detallesMovimiento,
      this.campoFiltro.cant,
      9
    );
    this.detallesMovimiento = this.filtro.filtroData(
      this.detallesMovimiento,
      this.campoFiltro.v_t,
      10
    );
    this.detallesMovimiento = this.filtro.filtroData(
      this.detallesMovimiento,
      this.campoFiltro.anioo,
      19
    );
  }
  filtrar() {
    this.listado = this.listadoTotal;
    this.listado = this.filtro.filtroData(this.listado, this.campoFiltro.id, 0);
    this.listado = this.filtro.filtroData(
      this.listado,
      this.campoFiltro.nro_mvto,
      1
    );
    this.listado = this.filtro.filtroData(
      this.listado,
      this.campoFiltro.nro_orden,
      2
    );
    this.listado = this.filtro.filtroData(
      this.listado,
      this.campoFiltro.glosa,
      3
    );
    this.listado = this.filtro.filtroData(
      this.listado,
      this.campoFiltro.prov,
      4
    );
    this.listado = this.filtro.filtroData(
      this.listado,
      this.campoFiltro.obs,
      5
    );
    this.listado = this.filtro.filtroData(
      this.listado,
      this.campoFiltro.nro_guia,
      6
    );
    this.listado = this.filtro.filtroData(
      this.listado,
      this.campoFiltro.tipo_mvto,
      7
    );
    this.listado = this.filtro.filtroData(
      this.listado,
      this.campoFiltro.fecha_mov,
      12
    );
    this.listado = this.filtro.filtroData(
      this.listado,
      this.campoFiltro.fecha_notif,
      13
    );
    this.listado = this.filtro.filtroData(
      this.listado,
      this.campoFiltro.plazo_entrega,
      14
    );
    ////
    this.listadoActualizados = this.listadoActualizadosTotal;
    this.listadoActualizados = this.filtro.filtroData(
      this.listadoActualizados,
      this.campoFiltro.id,
      0
    );
    this.listadoActualizados = this.filtro.filtroData(
      this.listadoActualizados,
      this.campoFiltro.nro_mvto,
      1
    );
    this.listadoActualizados = this.filtro.filtroData(
      this.listadoActualizados,
      this.campoFiltro.nro_orden,
      2
    );
    this.listadoActualizados = this.filtro.filtroData(
      this.listadoActualizados,
      this.campoFiltro.glosa,
      3
    );
    this.listadoActualizados = this.filtro.filtroData(
      this.listadoActualizados,
      this.campoFiltro.prov,
      4
    );
    this.listadoActualizados = this.filtro.filtroData(
      this.listadoActualizados,
      this.campoFiltro.obs,
      5
    );
    this.listadoActualizados = this.filtro.filtroData(
      this.listadoActualizados,
      this.campoFiltro.nro_guia,
      6
    );
    this.listadoActualizados = this.filtro.filtroData(
      this.listadoActualizados,
      this.campoFiltro.tipo_mvto,
      7
    );
    this.listadoActualizados = this.filtro.filtroData(
      this.listadoActualizados,
      this.campoFiltro.lug_nombre,
      10
    );
    this.listadoActualizados = this.filtro.filtroData(
      this.listadoActualizados,
      this.campoFiltro.fecha_mov,
      12
    );
    this.listadoActualizados = this.filtro.filtroData(
      this.listadoActualizados,
      this.campoFiltro.fecha_notif,
      13
    );
    this.listadoActualizados = this.filtro.filtroData(
      this.listadoActualizados,
      this.campoFiltro.plazo_entrega,
      14
    );
  }
  validarAlgunElemento() {
    return (
      // !this.detallesMovimiento.some((item) => item.elegido === true) ||
      this.lugarDestino.value === "" ||
      this.fechaDetalles === "" ||
      this.documentoAprobador === "" ||
      this.fechaPecosa === "" ||
      this.pecosaActual === ""
    );
  }
  filtrarMovimiento(evento) {
    console.log(evento.value);
    this.tiposT = this.tiposTotal.filter((tipo) => {
      return tipo.tipo_m === evento.value;
    });
    console.log(this.tiposT, this.tiposTotal);
  }
  seleccionarDetalleMovimiento(accion: MatCheckboxChange, idItem: number) {
    this.detallesMovimiento.forEach((item) => {
      if (item.iSIGADetalleId === idItem) {
        item.elegido = accion.checked;
        item.cantidad_seleccionada = accion.checked ? item.CANT_ARTICULO : 0;
      }
    });
    if (accion.checked === false) {
      this.salidaTodos = false;
      return;
    }
    if (!this.detallesMovimiento.some((item) => item.elegido === false)) {
      this.salidaTodos = true;
    }
  }
  abrirModal(modal, sz: "lg" | "sm" = "lg") {
    this.modal.open(modal, {
      backdrop: "static",
      keyboard: false,
      size: sz,
    });
  }
  abrirModalActualizarPecosa(modal, registro) {
    console.log(registro);
    this.formularioActual = 2;
    this.formActualizarPecosa = this.fb.group({
      pecosa: ["", Validators.required],
      fecha: [
        { value: moment().format("YYYY-MM-DD").toString(), disabled: true },
        Validators.required,
      ],
      lugarDestino: [
        { value: registro.iLugarId ? registro.iLugarId : null, disabled: true },
      ],
    });
    this.abrirModal(modal);
    this.cargarDetalleMovimiento(registro);
    this.movimientoActual = registro.id;
    this.movimientoSeleccionado = registro;
  }
  actualizarPecosa() {
    if (this.formActualizarPecosa.valid) {
      this.formActualizarPecosa.disable();
      let formData = {
        movimiento: this.movimientoActual,
        n_pecosa_new: this.formActualizarPecosa.value.pecosa,
      };
      this.query.saveDatosApi(this.url.actualizar_pecosa, formData).subscribe(
        (data: any) => {
          this.formActualizarPecosa.enable();
          this.modal.dismissAll();
          Swal.fire("Éxito!", data.mensaje, "success");
          this.cargarDatos();
        },
        (error) => {
          console.log(formData);
          this.modal.dismissAll();
          this.formActualizarPecosa.enable();
          Swal.fire(
            "Error!",
            error["error"]["message"].substring(
              71,
              error["error"]["message"].indexOf("(SQL: EXEC")
            ),
            "error"
          );
        }
      );
    } else {
      this.toastr.warning("Complete los campos requeridos", "Importante");
    }
  }
  cargarTodo() {
    this.loading = true;
    forkJoin({
      tipos: this.query.getDatosApi(this.url.tipos),
      alm: this.query.getDatosApi(this.url.almacenes),
      tiposNea: this.query.getDatosApi(this.url.tipos_nea),
    }).subscribe({
      next: (res) => {
        this.tiposTotal = res.tipos;
        this.almacenes = res.alm;
        this.tiposNea = res.tiposNea;
      },
      complete: () => {
        this.loading = false;
      },
    });
  }
  mostrarModalSec(modal) {
    this.cargarSecFunc();
    this.modal.open(modal, { size: "lg" }).result.then(
      (result) => {
        console.log(result);
        this.editNeaForm.controls["sec_func"].setValue(
          parseInt(result[0].sec_func, 10)
        );
        this.editNeaForm.controls["sec_func_nombre"].setValue(result[0].nombre);
      },
      (reason) => {
        //this.toastr.info(reason)
      }
    );
  }
  mostrarDetalles(modal, registro) {
    this.formularioActual = 1;
    this.abrirModal(modal);
    this.cargarDetalleMovimiento(registro);
    this.movimientoActual = registro.id;
    this.movimientoSeleccionado = registro;
  }
  editarNEA(modal, list) {
    this.formularioActual = 1;
    this.abrirModal(modal);
    this.cargarDetalleMovimiento(list[0]);
    this.movimientoActual = list[0].id;
    this.movimientoSeleccionado = list[0];
    this.editNeaForm = this.fb.group({
      sec_func: [this.movimientoSeleccionado.sec_func, Validators.required],
      sec_func_nombre: [
        this.movimientoSeleccionado.sec_func_nombre,
        Validators.required,
      ],
    });
  }
  guardarActualizacion() {
    this.editNeaForm.disable();
    const data = this.editNeaForm.value;
    data.id = this.movimientoSeleccionado.id;
    this.query.saveDatosApi(this.url.actualizarMov, data).subscribe(
      (data) => {
        this.formulario.enable();
        this.modal.dismissAll();
        Swal.fire(
          "Éxito!",
          "Se actualizaron los datos Exitosamente!",
          "success"
        );
        this.cargarDatos();
      },
      (error) => {
        console.log(error.error);
        // this.modal.dismissAll();
        this.editNeaForm.enable();
        Swal.fire(
          "Error!",
          error["message"].substring(
            71,
            error["message"].indexOf("(SQL: EXEC")
          ),
          "error"
        );
      }
    );
  }
  actualizarDato(item) {
    console.log(item);
    this.loading = true;
    this.query
      .saveDatosApi(this.url.actualizar_item, {
        id: item.iSIGADetalleId,
        clasificador: item.CLASIFICADOR,
        nro_orden: item.nro_oc,
        exp_siaf: item.nro_siaf,
        anio: item.anio_orden
      })
      .subscribe(
        (data: any) => {
          this.loading = false;
          this.toastr.success(data.mensaje, "Éxito!");
        },
        (error) => {
          // console.log(formData);
          // this.modal.dismissAll();
          this.loading = false;
          this.toastr.info(
            error["error"]["message"].substring(
              71,
              error["error"]["message"].indexOf("(SQL: EXEC")
            ),
            "Error!"
          );
        }
      );
  }
  cargarSecFunc() {
    this.query
      .getDatosApi(this.url.listarFunc + this.local.getItem("anio"))
      .subscribe((res) => {
        this.listaFunc = res;
        this.listaFuncGrl = res;
      });
  }
  mostrarOrden(modal, registro) {
    this.abrirModal(modal);
    this.cargarOrden(registro);
  }
  cargarDatos() {
    this.loading = true;
    forkJoin({
      movimientos: this.query.getDatosPostApi(this.url.select, {
        ano_eje: this.local.getItem("anio"),
        tipo_mvto: this.movimiento,
        tipo_trans: this.transaccion,
      }),
      movs_t: this.query.getDatosPostApi(this.url.select_terminados, {
        ano_eje: this.local.getItem("anio"),
        tipo_mvto: this.movimiento,
        tipo_trans: this.transaccion,
      }),
    }).subscribe({
      next: (data: any) => {
        let dt: any = data.movimientos;
        dt.map((e) => {
          e.fecha_mov = moment(e.fecha_mov).format("DD-MM-YYYY");
          if (!e.norden) {
            return [];
          }
          let tamDato = e.norden.toString().length;
          let ceros = ``;
          for (let i = 0; i < 7 - tamDato; i++) {
            ceros += "0";
          }
          e.norden = `${ceros}${e.norden}`;
        });
        this.listado = dt;
        this.listadoTotal = dt;
        let mt: any = data.movs_t;
        mt.map((e) => {
          e.fecha_mov = moment(e.fecha_mov).format("DD-MM-YYYY");
          if (!e.norden) {
            return [];
          }
          let tamDato = e.norden.toString().length;
          let ceros = ``;
          for (let i = 0; i < 7 - tamDato; i++) {
            ceros += "0";
          }
          e.norden = `${ceros}${e.norden}`;
        });
        this.listadoActualizados = mt;
        this.listadoActualizadosTotal = mt;
      },
      complete: () => {
        this.loading = false;
      },
      error: () => {
        this.loading = false;
      },
    });
  }
  cargarOrden(movimiento) {
    this.loading = true;
    this.query.getDatosApi(this.url.select_orden + movimiento.id).subscribe(
      (res) => {
        let data: any = res;
        this.detallesOrden = data.detalles;
        const fechaOc = moment(data.orden.FECHA_ORDEN).format("YYYY");
        data.orden.nro_orden = `${movimiento.norden} - ${fechaOc}`;
        data.orden.meta_m = data.detalles[0].func[0].meta;
        this.ordenActual = data.orden;
        this.asignarDatosForm(data.orden, 2);
        this.loading = false;
      },
      (error) => {
        this.loading = false;
        console.log(error);
      }
    );
  }
  cargarDetalleMovimiento(movimiento) {
    this.loading = true;
    this.salidaTodos = false;
    // this.lugarDestino.value(movimiento.iLugarId ? movimiento.iLugarId : null);
    this.pecosaActual = "";
    this.detallesMovimiento = [];
    this.pDM = 1;
    this.pDMP = 1;
    this.pENea = 1;
    this.query.getDatosApi(this.url.select_detalle + movimiento.id).subscribe(
      (res) => {
        this.detallesMovimiento = res;
        this.detallesMovimiento.map((item) => {
          item.elegido = false;
          item.cantidad_seleccionada = 0;
          item.CANT_ARTICULO = parseFloat(item.CANT_ARTICULO).toFixed(2);
          item.CANT_MOVIDA = parseFloat(item.CANT_MOVIDA).toFixed(2);
          item.VALOR_TOTAL = parseFloat(item.VALOR_TOTAL).toFixed(2);
          item.PRECIO_UNIT = parseFloat(item.PRECIO_UNIT).toFixed(2);
          item.zz = `${item.MAYOR}.${item.SUB_CTA}`;
        });
        console.log(this.detallesMovimiento);
        this.detallesMovimientoGrl = this.detallesMovimiento;
        if (this.formularioActual == 2) {
          this.formActualizarPecosa.controls["pecosa"].setValue(
            this.detallesMovimiento[0].n_pecosa
          );
          // this.nroPecosaActual = this.detallesMovimiento[0].n_pecosa;
        }
        this.loading = false;
      },
      (error) => {
        this.loading = false;
        console.log(error);
      }
    );
  }
  /**
   * evento: evento changed del input
   * id: id del detalle
   * maximo: valor maximo del detalle
   */
  modificarCantidad(evento, id: number, maximo: string) {
    const nmax: number = parseFloat(maximo);
    const nuevoValor: number = parseFloat(evento.target.value);
    let valorChecked = true;
    if (nuevoValor > nmax) {
      evento.target.value = maximo;
    }
    if (nuevoValor <= 0) {
      evento.target.value = 0;
      valorChecked = false;
    }
    this.detallesMovimiento.forEach((item) => {
      if (item.iSIGADetalleId === id) {
        item.cantidad_seleccionada = evento.target.value;
        item.elegido = valorChecked;
      }
    });
    if (valorChecked === false) {
      this.salidaTodos = false;
      return;
    }
    if (!this.detallesMovimiento.some((item) => item.elegido === false)) {
      this.salidaTodos = true;
    }
  }
  // generarFormularioParaDetallesMovimiento():FormGroup {
  //   const data = {};
  //   this.detallesMovimiento.forEach(detalle => {
  //     const nombre = detalle.iSIGADetalleId.toString() + 'detalle';
  //     const detalleControl = {
  //        nombre.toString(): ['', Validators.required]
  //     }
  //   })
  //   return this.fb.group({});
  // }
  guardar() {
    //  console.log ('Guardando')
    if (this.formulario.valid) {
      this.formulario.disable();
      let id_movimiento = this.formulario.value.id;
      let formData = {
        plazo: this.formulario.value.plazo,
        fecha: this.formulario.value.noti,
      };
      this.query
        .saveDatosApi(this.url.select + `/${id_movimiento}`, formData)
        .subscribe(
          (data) => {
            this.formulario.enable();
            this.modal.dismissAll();
            Swal.fire(
              "Éxito!",
              "Se actualizaron los datos Exitosamente!",
              "success"
            );
            this.cargarDatos();
          },
          (error) => {
            console.log(formData);
            this.modal.dismissAll();
            this.formulario.enable();
            Swal.fire(
              "Error!",
              error["error"]["message"].substring(
                71,
                error["error"]["message"].indexOf("(SQL: EXEC")
              ),
              "error"
            );
          }
        );
    } else {
      this.toastr.warning("Complete los campos requeridos", "Importante");
    }
  }
  asignarDatosForm(dato, tipo = 1) {
    switch (tipo) {
      /// FORMULARIO PARA ACTUALIZAR MOVIMIENTO
      case 1:
        this.formulario = this.fb.group({
          id: dato[0].id,
          orden: [
            { value: dato[0].norden, disabled: true },
            Validators.required,
          ],
          guia: [{ value: dato[0].nguia, disabled: true }, Validators.required],
          obs: [{ value: dato[0].obs, disabled: true }, Validators.required],
          noti: ["", Validators.required],
          plazo: ["", Validators.required],
        });
        break;
      /// FORMULARIO DE LA ORDEN
      case 2:
        this.ordenForm = this.fb.group({
          fecha: [moment(dato.FECHA_ORDEN).format("YYYY-MM-DD")],
          ref: [dato.DOCUM_REFERENCIA],
          norden: [dato.nro_orden],
          siaf: ["" === "" ? "0000000000" : ""],
          prov: [
            dato.proveedor_data.NRO_RUC +
              ": " +
              dato.proveedor_data.NOMBRE_PROV,
          ],
          meta: [dato.meta_m],
          sub: [dato.SUBTOTAL_SOLES],
          igv: [dato.TOTAL_IGV_SOLES],
          fac: [dato.TOTAL_FACT_SOLES],
        });
        break;
      case 3:
        this.formLugar = this.fb.group({
          lugar: [null, Validators.required],
          movimiento: [dato.id, Validators.required],
          fecha: [moment().format("YYYY-MM-DD"), Validators.required],
        });
        break;
    }
  }
  editar(formulario, list) {
    this.tituloForm = "Editando Estado";
    this.modal.open(formulario, {
      backdrop: "static",
      keyboard: false,
    });
    this.asignarDatosForm(list);
  }

  editarLugar(formulario, list) {
    this.abrirModal(formulario, "sm");
    this.asignarDatosForm(list, 3);
  }
  actualizarLugar() {
    this.loading = true;
    const data = this.formLugar.value;
    this.query.saveDatosApi(this.url.upd_lugar_movimiento, data).subscribe({
      next: (res: any) => {
        if (res.error) {
          this.toastr.error(
            "No se pudieron registrar los datos correctamente.",
            "Error"
          );
          return;
        }
        this.toastr.success(res.msg, "Éxito!");
      },
      complete: () => {
        this.loading = false;
        this.modal.dismissAll();
      },
      error: () => {
        this.loading = false;
      },
    });
  }
  generarPDFMovimientos() {
    this.jsPDF.generaPDF(
      this.prepararMovimientosActualizados(),
      1
      // 1,
      // this.prepararMovimientosSinActualizados()
    );
  }
  generarPDFMovimientosSinActualizar() {
    this.jsPDF.generaPDF(this.prepararMovimientosSinActualizados(), 1);
  }
  private prepararMovimientosSinActualizados() {
    // Verificar this.listado
    let listadoPDF: any = [];
    let index: number = 1;
    this.listado.forEach((element) => {
      listadoPDF.push([
        index++,
        element.fecha_mov,
        element.norden,
        element.nguia,
        element.namemvto,
        element.prov,
        element.glosa,
        element.obs,
      ]);
    });
    let estilosColumnas = {
      0: { halign: "center" },
      1: { halign: "left" },
      2: { halign: "left" },
      3: { halign: "left" },
      4: { halign: "left" },
      5: { halign: "left" },
      6: { halign: "left" },
      7: { halign: "left" },
    };
    let datosPDF = {
      cabeceras: [
        [
          "ITEM",
          "FECHA",
          "NRO ORDEN",
          "NRO GUÍA",
          "MOVIMIENTO",
          "PROVEEDOR",
          "GLOSA",
          "OBSERVACIÓN",
        ],
      ],
      titulo: "MOVIMIENTOS POR ACTUALIZAR",
      orientacion: "l",
      datos: listadoPDF,
      columnaStyles: estilosColumnas,
    };
    //console.log(datosPDF)
    return datosPDF;
  }
  private prepararMovimientosActualizados() {
    // Verificar this.listado
    let listadoPDF: any = [];
    let index: number = 1;
    this.listadoActualizados.forEach((element) => {
      listadoPDF.push([
        index++,
        element.fecha_mov,
        element.norden,
        element.nguia,
        element.namemvto,
        element.prov,
        element.glosa,
        element.obs,
        element.lug_nombre,
      ]);
    });
    let estilosColumnas = {
      0: { halign: "center" },
      1: { halign: "left" },
      2: { halign: "left" },
      3: { halign: "left" },
      4: { halign: "left" },
      5: { halign: "left" },
      6: { halign: "left" },
      7: { halign: "left" },
      8: { halign: "left" },
    };
    let datosPDF = {
      titulo: "MOVIMIENTOS ACTUALIZADOS",
      orientacion: "l",
      cabeceras: [
        [
          "ITEM",
          "FECHA",
          "NRO ORDEN",
          "NRO GUÍA",
          "MOVIMIENTO",
          "PROVEEDOR",
          "GLOSA",
          "OBSERVACIÓN",
          "DESTINO",
        ],
      ],
      datos: listadoPDF,
      columnaStyles: estilosColumnas,
    };
    //console.log(datosPDF)
    return datosPDF;
  }
  // REALIZAR FUNCIONES DE SALIDA
  seleccionarTodo(evento: MatCheckboxChange) {
    this.detallesMovimiento.map((item) => {
      item.elegido = evento.checked;
      item.cantidad_seleccionada = evento.checked ? item.CANT_ARTICULO : 0;
    });
  }
  darSalidaTodo() {
    const data = {
      detalles: this.detallesMovimiento,
      lugar: this.lugarDestino.value.lugar_id,
      movimiento: this.movimientoActual,
      fecha: this.fechaDetalles,
      pecosa: this.pecosaActual,
      fecha_pecosa: this.fechaPecosa,
      documento: this.documentoAprobador,
    };
    this.loading = true;
    this.query.saveDatosApi(this.url.upd_lugar_movimiento, data).subscribe({
      next: (res: any) => {
        if (res.error) {
          this.toastr.error(res.msg, "Error");
          return;
        }
        this.toastr.success(res.msg, "Éxito!");
      },
      complete: () => {
        this.loading = false;
        this.cargarDatos();
        this.modal.dismissAll();
      },
      error: () => {
        this.loading = false;
      },
    });
  }
  sincronizarDatos() {
    this.loading = true;
    this.query.getDatosApi(this.url.sincronizar).subscribe((res) => {
      const rpta: any = res;
      this.loading = false;
      this.toastr.info(rpta.msg, "MOVIMIENTOS SINCRONIZADOS");
    });
  }
  mostrarFormSincronizar(modal) {
    this.abrirModal(modal, "sm");
  }
  buscarNeas(modal = null) {
    this.loading = true;
    this.formularioNeaSincro.disable();
    this.pLNS = 1;
    this.query
      .getDatosPostApi(
        this.url.listar_neas_siga,
        this.formularioNeaSincro.value
      )
      .subscribe(
        (res: any) => {
          this.listaNeasSiga = res;
          this.loading = false;
          this.toastr.success(
            `Se listaron ${res.length} NEAS`,
            "BUSQUEDA DE NEAS"
          );

          modal === null ? null : this.abrirModal(modal);
          this.formularioNeaSincro.enable();
        },
        (error) => {
          this.loading = false;
        }
      );
  }
  buscarDetallesNeaSiga(registro, modal) {
    this.loading = true;
    this.listaDetallesNeaSiga = [];
    this.neaSeleccionada = registro;
    this.pDNS = 1;
    this.query
      .getDatosPostApi(this.url.listar_detalles, { nea: registro })
      .subscribe(
        (res: any) => {
          this.listaDetallesNeaSiga = res;
          this.toastr.success(
            "Detalles listados",
            "Detalles de NEA opción en mantenimiento"
          );
          this.abrirModal(modal);
          this.loading = false;
        },
        (err) => {
          this.toastr.error("Problemas al buscar", "Errores");
        }
      );
  }
  migrarNea(cerrar) {
    this.loading = true;
    this.query
      .saveDatosApi(this.url.migrar_nea, {
        nea: this.neaSeleccionada,
      })
      .subscribe(
        (data) => {
          this.loading = false;
          Swal.fire("Éxito!", "Se migró la NEA Exitosamente!", "success");
          cerrar();
          this.buscarNeas();
        },
        (error) => {
          this.loading = false;
          Swal.fire(
            "Error!",
            error["error"]["message"].substring(
              71,
              error["error"]["message"].indexOf("(SQL: EXEC")
            ),
            "error"
          );
        }
      );
  }
  downloadPdfOC() {
    this.loading = true;
    this.generarPdfOrdenCompra();
    this.loading = false;
  }
  generarPdfOrdenCompra() {
    this.jsPDF.generaPDF(this.prepararbodyOc(), 1, this.prepararCabeceraOC());
  }
  prepararCabeceraOC() {
    let listadoPDF: any = [];

    listadoPDF.push([
      this.ordenActual.proveedor_data.NRO_RUC +
        ": " +
        this.ordenActual.proveedor_data.NOMBRE_PROV,

      this.ordenActual.proveedor_data.DIRECCION,
      this.ordenActual.proveedor_data.GIRO_GENERAL,
      this.detallesOrden[0].func[0].meta,
    ]);
    let estilosColumnas = {
      0: { halign: "left" },
      1: { halign: "left" },
      2: { halign: "left" },
      3: { halign: "left" },
    };
    let datosPDF = {
      headers: [["SEÑOR(ES)", "DIRECCIÓN", "GIRO", "META"]],
      data: listadoPDF,
      columnaStyles: estilosColumnas,
    };
    //console.log(datosPDF)
    return datosPDF;
  }
  prepararbodyOc() {
    // Verificar this.listado
    let listadoPDF: any = [];
    let index: number = 1;

    this.detallesOrden.forEach((element) => {
      listadoPDF.push([
        element.producto.codigo,
        element.clasificador,
        element.CANT_ITEM,
        element.producto.unid_med,
        element.producto.producto + " - " + element.producto.marca,
        element.PREC_UNIT_MONEDA,
        element.PREC_TOT_SOLES,
      ]);
    });
    listadoPDF.push([
      "",
      "",
      "",
      "",
      "",
      "V. VENTA",
      this.ordenActual.SUBTOTAL_SOLES,
    ]);
    listadoPDF.push([
      "",
      "",
      "",
      "",
      "",
      "I.G.V.",
      this.ordenActual.TOTAL_IGV_SOLES,
    ]);
    listadoPDF.push([
      "",
      "",
      "",
      "",
      "",
      "TOTAL",
      this.ordenActual.TOTAL_FACT_SOLES,
    ]);
    let estilosColumnas = {
      0: { halign: "left" },
      1: { halign: "left" },
      2: { halign: "left" },
      3: { halign: "left" },
      4: { halign: "left" },
      5: { halign: "right" },
      6: { halign: "right" },
      // 6: { halign: "left" },
      // 7: { halign: "left" },
      // 8: { halign: "left" },
    };
    let fechaModif = moment(this.detallesOrden[0].FECHA_RECEP, "DD/MM/YYYY")
      .format("YYYY")
      .toString();
    let datosPDF = {
      titulo: `O/C N° ${this.detallesOrden[0].NRO_ORDEN} - ${fechaModif} (SIAF: 0000000000) - ${this.detallesOrden[0].FECHA_RECEP}`,
      orientacion: "l",
      cabeceras: [
        [
          "Código",
          "Clasificador",
          "Cant",
          "Unid. Med.",
          "Descripción",
          "Pre. Unit",
          "Pre. Total",
        ],
      ],
      tipotitulo: 1,
      datos: listadoPDF,
      columnaStyles: estilosColumnas,
    };
    //console.log(datosPDF)
    return datosPDF;
  }
  excelNEA(reg) {
    this.query.getDatosPostApi(this.url.descarga_excel, reg).subscribe({
      next: (res) => {
        console.log(res);
        let data: any = res;
        window.open(environment.rutas.recursos + data.ruta, "_blank");
      },
      complete: () => {},
    });
  }
}
