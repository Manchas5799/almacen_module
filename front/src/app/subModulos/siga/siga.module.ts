import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { SigaRoutingModule } from './siga-routing.module';
import { MovimientosComponent } from './movimientos/movimientos.component';
import { ReactiveFormsModule, FormsModule } from '@angular/forms';
import { MatAutocompleteModule } from '@angular/material/autocomplete';
import { MatButtonModule } from '@angular/material/button';
import { MatDialogModule } from '@angular/material/dialog';
import { MatFormFieldModule } from '@angular/material/form-field';
import { MatIconModule } from '@angular/material/icon';
import { MatInputModule } from '@angular/material/input';
import { MatMenuModule } from '@angular/material/menu';
import { MatSelectModule } from '@angular/material/select';
import { MatStepperModule } from '@angular/material/stepper';
import { MatTooltipModule } from '@angular/material/tooltip';
import {MatCheckboxModule} from '@angular/material/checkbox';
import { NgbModule } from '@ng-bootstrap/ng-bootstrap';
import { NgxDatatableModule } from '@swimlane/ngx-datatable';
import { NgxPaginationModule } from 'ngx-pagination';
import { NgxSpinnerModule } from 'ngx-spinner';
import { GlobalModule } from 'src/app/global/global.module';
import { SharedComponentsModule } from 'src/app/shared/components/shared-components.module';
import {MatTabsModule} from '@angular/material/tabs';


@NgModule({
  declarations: [MovimientosComponent],
  imports: [
    CommonModule,
    SigaRoutingModule,
    SharedComponentsModule,
    MatAutocompleteModule,
    NgbModule,
    GlobalModule,
    ReactiveFormsModule,
    NgxDatatableModule,
    NgxPaginationModule,
    FormsModule,
    MatSelectModule,
    MatInputModule,
    MatFormFieldModule,
    MatInputModule,
    MatDialogModule,
    MatStepperModule,
    MatButtonModule,
    MatMenuModule,
    MatCheckboxModule,
    MatIconModule,
    MatTooltipModule,
    NgxSpinnerModule,
    MatTabsModule
  ]
})
export class SigaModule { }
