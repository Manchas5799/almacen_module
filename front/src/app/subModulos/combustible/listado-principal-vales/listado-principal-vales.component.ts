import { Component, OnInit } from "@angular/core";
import { FormBuilder } from "@angular/forms";
import { ActivatedRoute } from "@angular/router";
import { NgbModal } from "@ng-bootstrap/ng-bootstrap";
import * as moment from "moment";
import { ToastrService } from "ngx-toastr";
import { FiltrosService } from "src/app/global/services/filtros.service";
import { JsPDFService } from "src/app/global/services/jsPDF.service";
import { LocalService } from "src/app/servicios/local.services";
import { QueryService } from "src/app/servicios/query.services";
import { environment } from "src/environments/environment";

@Component({
  selector: "app-listado-principal-vales",
  templateUrl: "./listado-principal-vales.component.html",
  styleUrls: ["./listado-principal-vales.component.scss"],
})
export class ListadoPrincipalValesComponent implements OnInit {
  loading: boolean = false;
  lugarTodo: any = null;
  almacenActual: any = null;
  almacenes: any = [];
  idAlmacenActual: number;
  listaLugares: any = [];
  listaAutorizados: any = [];
  listaPendientes: any = [];
  listaAutorizadosTotal: any = [];
  listaPendientesTotal: any = [];
  titulo: string = "VALES DE COMBUSTIBLE";
  campoFiltro = {
    fecha_vale: "",
    num_vale: "",
    chofer: "",
    brevete: "",
    placa: "",
    t_vehi: "",
    destino: "",
    lugar: "",
  };
  rutas = {
    select_combustible: "/movimientos/salidas/combustible",
  };
  pP: number = 1;
  pA: number = 1;
  iPageSize: number = environment.paginacion.tamañoPag;
  constructor(
    private query: QueryService,
    private modal: NgbModal,
    private toast: ToastrService,
    private filtro: FiltrosService,
    private fb: FormBuilder,
    private jsPDF: JsPDFService,
    private local: LocalService,
    private route: ActivatedRoute
  ) {}

  ngOnInit() {
    this.cargarValesCombustible();
  }
  obtenerAlmacen() {
    let almacen = this.local.getItem("almacenElegido");
    this.almacenActual = almacen;
    almacen.lugares = JSON.parse(almacen.lugares);
    this.idAlmacenActual = almacen.id;
    this.listaLugares = almacen.lugares;
  }
  cargarValesCombustible() {
    this.loading = true;
    this.query.getDatosApi(this.rutas.select_combustible).subscribe({
      next: (res: any) => {
        this.listaAutorizados = res.autorizados;
        this.listaAutorizados.map(
          (e) => (e.fecha_vale = moment(e.fecha_vale).format("YYYY-MM-DD"))
        );
        this.listaAutorizadosTotal = this.listaAutorizados;
        this.listaPendientes = res.porAutorizar;
        this.listaPendientes.map(
          (e) => (e.fecha_vale = moment(e.fecha_vale).format("YYYY-MM-DD"))
        );
        this.listaPendientesTotal = this.listaPendientes;
      },
      complete: () => {
        this.loading = false;
      },
      error: () => {
        this.loading = false;
      },
    });
  }
  filtrar() {
    this.listaAutorizados = this.listaAutorizadosTotal;
    this.listaAutorizados = this.filtro.filtroData(
      this.listaAutorizados,
      this.campoFiltro.fecha_vale,
      1
    );
    this.listaAutorizados = this.filtro.filtroData(
      this.listaAutorizados,
      this.campoFiltro.chofer,
      7
    );
    this.listaAutorizados = this.filtro.filtroData(
      this.listaAutorizados,
      this.campoFiltro.num_vale,
      0
    );
    this.listaAutorizados = this.filtro.filtroData(
      this.listaAutorizados,
      this.campoFiltro.brevete,
      2
    );
    this.listaAutorizados = this.filtro.filtroData(
      this.listaAutorizados,
      this.campoFiltro.placa,
      5
    );
    this.listaAutorizados = this.filtro.filtroData(
      this.listaAutorizados,
      this.campoFiltro.t_vehi,
      6
    );
    this.listaAutorizados = this.filtro.filtroData(
      this.listaAutorizados,
      this.campoFiltro.destino,
      3
    );

    this.listaPendientes = this.listaPendientesTotal;
    this.listaPendientes = this.filtro.filtroData(
      this.listaPendientes,
      this.campoFiltro.fecha_vale,
      1
    );
    this.listaPendientes = this.filtro.filtroData(
      this.listaPendientes,
      this.campoFiltro.chofer,
      7
    );
    this.listaPendientes = this.filtro.filtroData(
      this.listaPendientes,
      this.campoFiltro.brevete,
      2
    );
    this.listaPendientes = this.filtro.filtroData(
      this.listaPendientes,
      this.campoFiltro.placa,
      5
    );
    this.listaPendientes = this.filtro.filtroData(
      this.listaPendientes,
      this.campoFiltro.t_vehi,
      6
    );
    this.listaPendientes = this.filtro.filtroData(
      this.listaPendientes,
      this.campoFiltro.destino,
      3
    );
  }
}
