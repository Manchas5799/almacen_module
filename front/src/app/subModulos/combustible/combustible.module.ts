import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { CombustibleRoutingModule } from './combustible-routing.module';
import { ReactiveFormsModule, FormsModule } from '@angular/forms';
import { MatAutocompleteModule } from '@angular/material/autocomplete';
import { MatButtonModule } from '@angular/material/button';
import { MatCheckboxModule } from '@angular/material/checkbox';
import { MatDialogModule } from '@angular/material/dialog';
import { MatFormFieldModule } from '@angular/material/form-field';
import { MatIconModule } from '@angular/material/icon';
import { MatInputModule } from '@angular/material/input';
import { MatMenuModule } from '@angular/material/menu';
import { MatSelectModule } from '@angular/material/select';
import { MatStepperModule } from '@angular/material/stepper';
import { MatTabsModule } from '@angular/material/tabs';
import { MatTooltipModule } from '@angular/material/tooltip';
import { NgbModule } from '@ng-bootstrap/ng-bootstrap';
import { NgxDatatableModule } from '@swimlane/ngx-datatable';
import { NgxPaginationModule } from 'ngx-pagination';
import { NgxSpinnerModule } from 'ngx-spinner';
import { GlobalModule } from 'src/app/global/global.module';
import { SharedComponentsModule } from 'src/app/shared/components/shared-components.module';
import { SolicitudCombustibleComponent } from './solicitud-combustible/solicitud-combustible.component';
import { SolicitudesDeCombustibleComponent } from './solicitudes-de-combustible/solicitudes-de-combustible.component';
import { ListadoPrincipalValesComponent } from './listado-principal-vales/listado-principal-vales.component';

@NgModule({
  declarations: [SolicitudCombustibleComponent, SolicitudesDeCombustibleComponent, ListadoPrincipalValesComponent],
  imports: [
    CommonModule,
    CombustibleRoutingModule,
    SharedComponentsModule,
    MatAutocompleteModule,
    NgbModule,
    GlobalModule,
    ReactiveFormsModule,
    NgxDatatableModule,
    NgxPaginationModule,
    FormsModule,
    MatSelectModule,
    MatInputModule,
    MatFormFieldModule,
    MatInputModule,
    MatDialogModule,
    MatStepperModule,
    MatButtonModule,
    MatMenuModule,
    MatCheckboxModule,
    MatIconModule,
    MatTooltipModule,
    NgxSpinnerModule,
    MatTabsModule
  ]
})
export class CombustibleModule { }
