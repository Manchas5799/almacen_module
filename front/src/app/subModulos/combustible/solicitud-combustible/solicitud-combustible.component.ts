import { Component, OnInit } from "@angular/core";
import {
  FormGroup,
  FormControl,
  FormBuilder,
  Validators,
} from "@angular/forms";
import { ActivatedRoute, ParamMap } from "@angular/router";
import { NgbModal } from "@ng-bootstrap/ng-bootstrap";
import * as moment from "moment";
import { ToastrService } from "ngx-toastr";
import { Observable, forkJoin } from "rxjs";
import { startWith, map } from "rxjs/operators";
import { FiltrosService } from "src/app/global/services/filtros.service";
import { JsPDFService } from "src/app/global/services/jsPDF.service";
import { LocalService } from "src/app/servicios/local.services";
import { QueryService } from "src/app/servicios/query.services";
import { environment } from "src/environments/environment";
import Swal from "sweetalert2";
import { ListaBienesMovimiento } from "../../internamiento/bienes-por-entradas/lista-bienes-movimiento";

@Component({
  selector: "app-solicitud-combustible",
  templateUrl: "./solicitud-combustible.component.html",
  styleUrls: ["./solicitud-combustible.component.scss"],
})
export class SolicitudCombustibleComponent implements OnInit {
  loading: boolean = false;
  loadingKardex: boolean = false;
  buscandoPersona: boolean = false;
  lugarTodo: any;
  almacenes: any = [];
  bienesActuales: any = [];
  listaLugares: any = [];
  listaTotalBienes: any = [];
  listadoPer: any = [];
  listadoPersonas: any = [];
  listaBienes: any = [];
  listaTipoMovs: any = [];
  listaTipoMovsGrl: any = [];
  listaEstados: any = [];
  listaTipoV: any = [];
  registrosPersonas: number;
  idAlmacenActual: number;
  bienActual: any = null;
  kardexActual: any = null;
  fechaDetalle: string = moment().format("YYYY-MM-DD").toString();
  rutas = {
    select_bienes: "/secundario/combustible",
    select_almacenes: "/almacenes_all",
    personas: "/almacenes_get_personas",
    select_tipos: "/tipo_movimiento",
    select_estados: "/estados_nota",
    save_mov: "/secundario/movimientos/salida/combustible",
    select_tipos_nea: "/tipo_neas",
    tiposV: "/tipos_vehiculos",
    select_kardex_bien: "/bienes/kardex",
  };
  titulo = "COMBUSTIBLE";
  msgBuscando = "Buscando ...";
  formularioMovimiento: FormGroup;
  formBuscarPersona: FormGroup;
  //////
  campoFiltro = {
    articulo: "",
    saldo: "",
    ///
    idPer: "",
    unidad: "",
    nombrePer: "",
  };
  tipoMovimientoDefecto = "1";
  tipoMovimientoDefectoNombre = "";
  ///////////////////
  iPageSize = environment.paginacion.tamañoPag;
  pB = 1;
  pPer = 1;
  pk = 1;
  /////////
  listaBienesFiltrada: Observable<any>;
  bienSeleccionado = new FormControl();
  items: ListaBienesMovimiento;
  almacenesFiltrados: Observable<any>;
  lugarDestino = new FormControl("");
  constructor(
    private query: QueryService,
    private modal: NgbModal,
    private toast: ToastrService,
    private filtro: FiltrosService,
    private fb: FormBuilder,
    private jsPDF: JsPDFService,
    private local: LocalService,
    private route: ActivatedRoute
  ) {}

  ngOnInit() {
    this.obtenerAlmacen();
    this.cargarDatos();
    this.formularioMovimiento = this.fb.group({
      lugar_origen: ["", Validators.required], //
      lugar_origen_id: ["", Validators.required], //
      lugar_destino: ["", Validators.required], //
      fecha_actual: [
        moment().format("YYYY-MM-DD").toString(),
        Validators.required,
      ], //
      fecha_recepcion: ["", Validators.required], ///
      observacion: ["", Validators.required],
      glosa: ["", Validators.required],
      solicitante_nombre: [{ value: "", disabled: true }, Validators.required],
      solicitante_id: ["", Validators.required],
      recepciona_nombre: [{ value: "", disabled: true }, Validators.required],
      recepciona_id: ["", Validators.required],
      tipo_movimiento: ["", Validators.required],
      estado: ["", Validators.required],
    });
    this.formBuscarPersona = this.fb.group({
      buscar: this.fb.control("", [
        Validators.required,
        Validators.minLength(3),
      ]),
    });
    this.listaBienesFiltrada = this.bienSeleccionado.valueChanges.pipe(
      startWith(""),
      map((value) => this._filter(value))
    );
    this.almacenesFiltrados = this.lugarDestino.valueChanges.pipe(
      startWith(""),
      map((value) => this._filterAlmacenes(value))
    );
  }
  private _filterAlmacenes(value): any {
    console.log("valor", value);
    const filterValue = value; //.toLowerCase();
    if (typeof filterValue === "string") {
      this.formularioMovimiento.controls["lugar_destino"].setValue("");
      return this.almacenes.filter((option) =>
        option.nombre.includes(filterValue.toUpperCase())
      );
    } else {
      return this.almacenes;
    }
  }
  displayFnLugares(lugar?): string | undefined {
    return lugar ? lugar.lugar_nombre : undefined;
  }
  lugarElegido(evento) {
    this.formularioMovimiento.controls["lugar_destino"].setValue(
      evento.option.value.lugar_id
    );
  }
  private _filter(value): any {
    console.log("valor", value);
    const filterValue = value; //.toLowerCase();
    return this.listaTotalBienes.filter((option) =>
      option.articulo.toLowerCase().includes(filterValue)
    );
  }
  obtenerAlmacen() {
    let almacen = this.local.getItem("almacenElegido");
    almacen = almacen;
    almacen.lugares = JSON.parse(almacen.lugares);
    this.idAlmacenActual = almacen.id;
    this.listaLugares = almacen.lugares;
    this.lugarTodo = this.listaLugares[0];
    this.filtrarXLugares({ value: this.lugarTodo });
  }
  cargarDatos() {
    this.loading = true;
    forkJoin({
      almacenes: this.query.getDatosApi(this.rutas.select_almacenes),
      tipos: this.query.getDatosApi(this.rutas.select_tipos),
      estados: this.query.getDatosApi(this.rutas.select_estados),
      tipoV: this.query.getDatosApi(this.rutas.tiposV),
    }).subscribe({
      next: (res) => {
        this.almacenes = res.almacenes;
        this.listaTipoMovs = res.tipos;
        this.listaTipoMovsGrl = res.tipos;
        this.listaEstados = res.estados;
        this.listaTipoV = res.tipoV;
      },
      complete: () => {
        this.route.paramMap.subscribe((params: ParamMap) => {
          this.tipoMovimientoDefecto = params.get("tipo");
          const tipoM = this.listaTipoMovs.find(
            (i) =>
              parseInt(i.tipomov_id, 10) ===
              parseInt(this.tipoMovimientoDefecto, 10)
          );
          this.tipoMovimientoDefectoNombre = tipoM.nombre;
          if (parseInt(this.tipoMovimientoDefecto, 10) === 37) {
            this.cargarTipoMovimientoNea();
          } else {
            this.listaTipoMovs = this.listaTipoMovsGrl;
          }
        });
        this.loading = false;
      },
      error: () => {
        this.loading = false;
      },
    });
  }
  cargarTipoMovimientoNea() {
    this.loading = true;
    this.query.getDatosApi(this.rutas.select_tipos_nea).subscribe((res) => {
      this.listaTipoMovs = res;
      // this.tipoMovimientoDefecto = this.listaTipoMovs[0].tipomov_id;
      this.loading = false;
    });
  }
  filtrarXLugares(event) {
    this.loading = true;
    this.cargarMovimientosPendientes(event.value.lugar_id);
  }
  buscarXLugar(id) {
    this.loading = true;
    this.cargarMovimientosPendientes(id);
  }
  cargarMovimientosPendientes(lugar) {
    this.query.getDatosApi(this.rutas.select_bienes + `/${lugar}`).subscribe({
      next: (res: any) => {
        res.map((i) => (i.saldo = parseFloat(i.saldo).toFixed(2)));
        this.listaTotalBienes = res;
        this.listaBienes = res;
      },
      complete: () => {
        this.loading = false;
      },
    });
  }
  filtrar() {
    this.listaBienes = this.listaTotalBienes;
    this.listaBienes = this.filtro.filtroData(
      this.listaBienes,
      this.campoFiltro.articulo,
      1
    );
    this.listaBienes = this.filtro.filtroData(
      this.listaBienes,
      this.campoFiltro.saldo,
      4
    );
    this.listaBienes = this.filtro.filtroData(
      this.listaBienes,
      this.campoFiltro.unidad,
      5
    );
    ////
  }
  abrirModal(modal, sz: "lg" | "sm" = "lg") {
    this.modal.open(modal, {
      backdrop: "static",
      keyboard: false,
      size: sz,
    });
  }
  modalFormPersona(formulario) {
    this.listadoPer = [];
    this.modal.open(formulario, { size: "lg" }).result.then(
      (result) => {
        console.log(result);
        this.formularioMovimiento.controls["solicitante_id"].setValue(
          result[0].id
        );
        this.formularioMovimiento.controls["solicitante_nombre"].setValue(
          result[0].nombre
        );
      },
      (reason) => {
        //this.toastr.info(reason)
      }
    );
  }
  modalFormChofer(formulario) {
    this.listadoPer = [];
    this.modal.open(formulario, { size: "lg" }).result.then(
      (result) => {
        console.log(result);
        this.formularioMovimiento.controls["iChoferId"].setValue(result[0].id);
        this.formularioMovimiento.controls["chofer_nombre"].setValue(
          result[0].nombre
        );
      },
      (reason) => {
        //this.toastr.info(reason)
      }
    );
  }
  filtrarPersona() {
    //this.listado =this.listadoPersonas
    this.listadoPer = this.listadoPersonas;
    this.listadoPer = this.filtro.filtroData(
      this.listadoPer,
      this.campoFiltro.idPer,
      0
    );
    this.listadoPer = this.filtro.filtroData(
      this.listadoPer,
      this.campoFiltro.nombrePer,
      1
    );
  }
  buscarPersona() {
    this.buscandoPersona = true;
    let formData = {
      //let //const // var
      texto: this.formBuscarPersona.value.buscar,
    };
    console.log("Buscando Persona: " + this.formBuscarPersona.value.buscarr);
    this.query.getDatosPostApi(this.rutas.personas, formData).subscribe(
      (data) => {
        this.listadoPersonas = data;
        this.listadoPer = this.listadoPersonas;
        this.registrosPersonas = this.listadoPersonas.length;
        this.buscandoPersona = false;
      },
      (error) => {
        this.toast.warning(
          error["error"]["message"].substring(
            71,
            error["error"]["message"].indexOf("(SQL:")
          ),
          "Importante"
        );
        this.loading = false;
      }
    );
  }
  abrirFormCrearMovimiento(modal) {
    console.log(this.lugarTodo);
    this.items = new ListaBienesMovimiento(this.listaTotalBienes);
    if (
      parseInt(this.tipoMovimientoDefecto, 10) === 34 ||
      parseInt(this.tipoMovimientoDefecto, 10) === 35 ||
      parseInt(this.tipoMovimientoDefecto, 10) === 36 ||
      parseInt(this.tipoMovimientoDefecto, 10) === 38 ||
      parseInt(this.tipoMovimientoDefecto, 10) === 39
    ) {
      this.formularioMovimiento = this.fb.group({
        lugar_origen: [this.lugarTodo.lugar_nombre, Validators.required], //
        lugar_origen_id: [this.lugarTodo.lugar_id, Validators.required], //
        lugar_destino: [{ value: "", disabled: true }], //
        fecha_actual: [
          moment().format("YYYY-MM-DD").toString(),
          Validators.required,
        ], //
        fecha_recepcion: [{ value: "", disabled: true }], ///
        observacion: [""],
        glosa: [""],
        solicitante_nombre: ["", Validators.required],
        solicitante_id: ["", Validators.required],
        recepciona_nombre: [{ value: "", disabled: true }],
        num_vale: ['', Validators.required],
        recepciona_id: [{ value: "", disabled: true }],
        tipo_movimiento: [
          { value: parseInt(this.tipoMovimientoDefecto, 10), disabled: true },
          Validators.required,
        ],
        estado: [{ value: 4, disabled: true }, Validators.required],
        ////
        fecha_vale: [
          moment().format("YYYY-MM-DD").toString(),
          Validators.required,
        ],
        brevete_vale: ["", [Validators.required, Validators.maxLength(12)]],
        motivo_servicio_vale: ["", Validators.required],
        lugar_destino_vale: ["", Validators.required],
        placa_vale: ["", [Validators.required, Validators.maxLength(7)]],
        tipo_vehiculo: ["", Validators.required],
        iChoferId: ["", Validators.required],
        chofer_nombre: [{ value: "", disabled: true }, Validators.required],
      });
    } else if (parseInt(this.tipoMovimientoDefecto, 10) === 37) {
      this.formularioMovimiento = this.fb.group({
        lugar_origen: [this.lugarTodo.lugar_nombre, Validators.required], //
        lugar_origen_id: [this.lugarTodo.lugar_id, Validators.required], //
        lugar_destino: [{ value: 1, disabled: true }], //
        fecha_actual: [
          moment().format("YYYY-MM-DD").toString(),
          Validators.required,
        ], //
        fecha_recepcion: [{ value: "", disabled: true }], ///
        observacion: [""],
        glosa: [""],
        solicitante_nombre: ["", Validators.required],
        solicitante_id: ["", Validators.required],
        recepciona_nombre: [{ value: "", disabled: true }],
        num_vale: ['', Validators.required],
        recepciona_id: [{ value: "", disabled: true }],
        tipo_movimiento: [
          { value: parseInt(this.tipoMovimientoDefecto, 10), disabled: true },
          Validators.required,
        ],
        estado: [{ value: 4, disabled: true }, Validators.required],
        ////
        fecha_vale: [
          moment().format("YYYY-MM-DD").toString(),
          Validators.required,
        ],
        brevete_vale: ["", [Validators.required, Validators.maxLength(12)]],
        motivo_servicio_vale: ["", Validators.required],
        lugar_destino_vale: ["", Validators.required],
        placa_vale: ["", [Validators.required, Validators.maxLength(7)]],
        tipo_vehiculo: ["", Validators.required],
        iChoferId: ["", Validators.required],
        chofer_nombre: [{ value: "", disabled: true }, Validators.required],
      });
    } else {
      this.formularioMovimiento = this.fb.group({
        lugar_origen: [this.lugarTodo.lugar_nombre, Validators.required], //
        lugar_origen_id: [this.lugarTodo.lugar_id, Validators.required], //
        lugar_destino: ["", Validators.required], //
        fecha_actual: [
          moment().format("YYYY-MM-DD").toString(),
          Validators.required,
        ], //
        fecha_recepcion: [{ value: "", disabled: true }], ///
        observacion: [""],
        glosa: [""],
        solicitante_nombre: ["", Validators.required],
        solicitante_id: ["", Validators.required],
        recepciona_nombre: [{ value: "", disabled: true }],
        num_vale: ['', Validators.required],
        recepciona_id: [{ value: "", disabled: true }],
        tipo_movimiento: [
          { value: parseInt(this.tipoMovimientoDefecto, 10), disabled: true },
          Validators.required,
        ],
        estado: [{ value: 4, disabled: true }, Validators.required],
        ////
        fecha_vale: [
          moment().format("YYYY-MM-DD").toString(),
          Validators.required,
        ],
        brevete_vale: ["", [Validators.required, Validators.maxLength(12)]],
        motivo_servicio_vale: ["", Validators.required],
        lugar_destino_vale: ["", Validators.required],
        placa_vale: ["", [Validators.required, Validators.maxLength(7)]],
        tipo_vehiculo: ["", Validators.required],
        iChoferId: ["", Validators.required],
        chofer_nombre: [{ value: "", disabled: true }, Validators.required],
      });
    }
    this.abrirModal(modal);
  }
  agregarBien() {
    this.items.agregarBien(this.bienSeleccionado.value.bien_id);
    this.bienSeleccionado.setValue("");
  }
  modificarCantidad(evento, id: number, maximo: string) {
    const nmax: number = parseFloat(maximo);
    const nuevoValor: number = parseFloat(evento.target.value);
    let result = nuevoValor;
    if (nuevoValor > nmax) {
      result = nmax;
      evento.target.value = result;
    }
    if (nuevoValor <= 0) {
      evento.target.value = 0;
      result = 0;
    }
    console.log(nmax, nuevoValor, result);
    this.items.cambiarCantidad(id, result);
  }
  moverBienes() {
    this.loading = true;
    console.log(this.formularioMovimiento.value);
    const data = this.formularioMovimiento.getRawValue();
    data.bienes = this.items.mostrarLista();
    this.query.saveDatosApi(this.rutas.save_mov, data).subscribe({
      next: (res: any) => {
        if (res.error) {
          this.toast.warning(
            "Problemas al realizar el registro del movimiento",
            "Error"
          );
        } else {
          this.toast.success("Movimiento registrado correctamente", "¡Éxito!");
        }
      },
      complete: () => {
        this.loading = false;
        this.modal.dismissAll();
        this.cargarMovimientosPendientes(this.lugarTodo.lugar_id);
      },
      error: (error) => {
        this.modal.dismissAll();
        Swal.fire(
          "Error!",
          error["error"]["message"].substring(
            71,
            error["error"]["message"].indexOf("(SQL: EXEC")
          ),
          "error"
        );
      },
    });
  }
  displayFn(bien?): string | undefined {
    return bien ? bien.articulo + " (saldo: " + bien.saldo + ")" : undefined;
  }
  mostrarKardex(modal, bien) {
    this.bienActual = bien;
    this.abrirModal(modal);
    this.cargarKardexBien(bien);
  }
  cargarKardexBien(bien) {
    this.loadingKardex = true;
    this.query
      .getDatosPostApi(this.rutas.select_kardex_bien, {
        bien: bien.bien_id,
        lugar: this.lugarTodo.lugar_id,
      })
      .subscribe((res) => {
        this.kardexActual = res;
        let valorActual = 0;
        this.kardexActual.forEach((item) => {
          valorActual =
            item.entrada !== null
              ? valorActual + parseFloat(item.entrada)
              : valorActual - parseFloat(item.salida);
          item.saldoRestante = valorActual;
        });
        this.loadingKardex = false;
      });
  }
  generarPdfKardex() {
    this.loading = true;
    this.downloadPDF();
    this.loading = false;
  }
  downloadPDF() {
    this.jsPDF.generaPDF(
      this.prepararPDFKardex(),
      1,
      this.generarCabeceraKardex()
    );
  }
  private prepararPDFKardex() {
    // Verificar this.listado
    let listadoPDF: any = [];
    let index: number = 1;
    this.kardexActual.forEach((element) => {
      let motivos = `Salida: ${element.t_mov1} \n Entrada: ${element.t_mov2}`;
      listadoPDF.push([
        index++,
        moment(element.dfecha).format("YYYY-MM-DD hh:mm a"),
        element.mov_observacion || "",
        motivos,
        element.origen || "",
        element.destino || "CONSUMO INTERNO",
        element.entrada ? parseFloat(element.entrada).toFixed(2) : "0.00" || 0,
        element.salida ? parseFloat(element.salida).toFixed(2) : "0.00" || 0,
        parseFloat(element.saldoRestante).toFixed(2),
      ]);
    });
    let estilosColumnas = {
      0: { halign: "center" },
      1: { halign: "center" },
      2: { halign: "left" },
      3: { halign: "left" },
      4: { halign: "center" },
      5: { halign: "center" },
      6: { halign: "right" },
      7: { halign: "right" },
      8: { halign: "right" },
    };
    let datosPDF = {
      orientacion: "L", //Un valor distinto a 'p' hará que sea horizontal (Se sugiere 'l')
      titulo: "KARDEX",

      cabeceras: [
        [
          "ITEM",
          "FECHA",
          "DOCUMENTO QUE APROBÓ",
          "MOTIVOS",
          "ALMACÉN ORIGEN",
          "ALMACÉN DESTINO",
          "ENTRADA",
          "SALIDA",
          "SALDO",
        ],
      ],
      datos: listadoPDF,
      columnaStyles: estilosColumnas,
    };
    //console.log(datosPDF)
    return datosPDF;
  }
  private generarCabeceraKardex() {
    let listadoPDF: any = [];

    listadoPDF.push([
      this.bienActual.codigo,
      this.bienActual.articulo,
      this.bienActual.uni_med,
      this.bienActual.anio,
    ]);
    let estilosColumnas = {
      0: { halign: "left" },
      1: { halign: "left" },
      2: { halign: "left" },
      3: { halign: "left" },
    };
    let datosPDF = {
      headers: [["CÓDIGO", "ARTICULO", "UNI. MEDIDA", "NRO ORDEN - AÑO"]],
      data: listadoPDF,
      columnaStyles: estilosColumnas,
    };
    //console.log(datosPDF)
    return datosPDF;
  }
}
