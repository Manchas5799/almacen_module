import { ListadoPrincipalValesComponent } from "./listado-principal-vales/listado-principal-vales.component";
import { SolicitudCombustibleComponent } from "./solicitud-combustible/solicitud-combustible.component";
import { NgModule } from "@angular/core";
import { Routes, RouterModule } from "@angular/router";
import { SolicitudesDeCombustibleComponent } from "./solicitudes-de-combustible/solicitudes-de-combustible.component";

const routes: Routes = [
  {
    path: "listado-sec",
    component: SolicitudesDeCombustibleComponent,
  },
  {
    path: "listado-prin",
    component: ListadoPrincipalValesComponent,
  },
  {
    path: "generar/:tipo",
    component: SolicitudCombustibleComponent,
  },
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class CombustibleRoutingModule {}
