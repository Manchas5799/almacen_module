import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { EstadosNotaComponent } from './estados-nota.component';

describe('EstadosNotaComponent', () => {
  let component: EstadosNotaComponent;
  let fixture: ComponentFixture<EstadosNotaComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ EstadosNotaComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(EstadosNotaComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
