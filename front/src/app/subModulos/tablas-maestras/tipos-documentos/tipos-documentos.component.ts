import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { QueryService } from './../../../servicios/query.services'
import { FiltrosService} from './../../../global/services/filtros.service'
import { JsPDFService } from './../../../global/services/jsPDF.service'
import { NgbActiveModal, NgbModal, ModalDismissReasons } from '@ng-bootstrap/ng-bootstrap';
import { FormBuilder, FormControl, FormGroup ,Validators } from '@angular/forms';
import { ToastrService } from 'ngx-toastr';
import {formatDate } from '@angular/common';
// import * as jsPDF from 'jspdf'
import swal from 'sweetalert2'

@Component({
  selector: 'app-tipos-documentos',
  templateUrl: './tipos-documentos.component.html',
  styleUrls: ['./tipos-documentos.component.scss']
})
export class TiposDocumentosComponent implements OnInit {
  titulo:string = 'TIPOS DE DOCUMENTOS'
  /** listas */
 listado:any =[]
 listadoGral:any =[]
 listadoG:any =[]
 listadoGGral:any =[]
 /*filtros*/
 campoFiltro = {
       id:'',
       nombre:'',
       sigla:''
     }
 campoFiltroG = {
      id:'',
      nombre:'',
      sigla:''
    }
 /**pagination */
 //rows = [];
 iPageSize=10
 p: number = 1;
 //rowsG = [];
 iPageSizeG=10
 pG: number = 1;

  /** forms */
  formulario: FormGroup
  formFiltroData: FormGroup;
  tituloForm:string = ''

   /* loading */
   loading:boolean = false
  
   /**check */
   checks:any = []
   chkTodos:boolean = false
   
   /** Tipo de reporte */
   typeReport:string =  'PDF'
   pagReport: number =1

   /**auto-inicio filtro*/
   //iOpcionFiltro=0
   /*URLS */
   url = {
     select : '/tipos_documentos',
     update: '/tipos_documentos_save',
     delete: '/tipos_documentos_del'
   }
  constructor(
    private query:QueryService,
    private filtro: FiltrosService,
    private route: ActivatedRoute,
    private toastr: ToastrService,
    private modal:NgbModal,
    private fb: FormBuilder,
    private jsPDF: JsPDFService
  ) { }

  ngOnInit() {
    this.loading=true 
 /*   this.formFiltroData = new FormGroup ({
      iOpcionFiltro:new FormControl(0),
    })
    */
    this.seleccionar()

    this.formulario = this.fb.group({
      id: [''],
      nombre: ['', Validators.required],
    })
    // this.loading = false
  }
 /* modalFormOpen(formulario){
    this.tituloForm = 'Nuevo Tipo Documento'
    
    this.modal.open(formulario, {size : "sm", backdrop: 'static', keyboard: false})
    this.asignarDatosForm(null)
  }
  asignarDatosForm(dato){
    if(dato==null){
      
      this.formulario = this.fb.group({
        id: [''],
        nombre: ['', Validators.required],
      })
    }else{
      
      this.formulario = this.fb.group({
        id: dato[0].id,
        nombre: dato[0].nombre,
      })
    }
  }
*/
  seleccionar(){
    this.loading = true
    let datos:any   
    //if(this.formFiltroData.valid) {
    //  this.formFiltroData.disable()  //Desactivamos los filtros
      this.query.getDatosApi(this.url.select).subscribe(
        data =>{
          console.log(data)
          datos = data
          this.listado = datos.datos
          this.listadoGral = this.listado
          this.listadoG = datos.datosGral
          this.listadoGGral = this.listadoG
          this.loading = false
          //this.formFiltroData.enable()
          if(datos.error == true){
            this.toastr.warning(datos['msg'],'Importante');
          }
        },
        error => {
          //this.formFiltroData.enable()
          this.toastr.warning(error['error']['msg'].substring(71,error['error']['msg'].indexOf("(SQL:")),'Importante');
          this.loading = false
        }
      )        
    //}else{
      //this.validarCamposForm(this.formFiltroData) 
    //}
    // this.loading = false
  }
  filtrar(){
    this.listado =this.listadoGral
    
    this.listado=this.filtro.filtroData(this.listado, this.campoFiltro.id,0)
    this.listado=this.filtro.filtroData(this.listado, this.campoFiltro.nombre,1)
    this.listado=this.filtro.filtroData(this.listado, this.campoFiltro.sigla,2)

   /* this.listado=this.filtro.filtroDataId(this.listado, this.campoFiltro.id)
    this.listado=this.filtro.filtroDataNombre(this.listado, this.campoFiltro.nombre)
    this.listado=this.filtro.filtroDataSigla(this.listado, this.campoFiltro.sigla)
    */
  }
  filtrarGral(){
    this.listadoG =this.listadoGGral
    this.listadoG=this.filtro.filtroData(this.listadoG, this.campoFiltroG.id,0)
    this.listadoG=this.filtro.filtroData(this.listadoG, this.campoFiltroG.nombre,1)
    this.listadoG=this.filtro.filtroData(this.listadoG, this.campoFiltroG.sigla,2)
    /*
    this.listadoG=this.filtro.filtroDataId(this.listadoG, this.campoFiltroG.id)
    this.listadoG=this.filtro.filtroDataNombre(this.listadoG, this.campoFiltroG.nombre)
    this.listadoG=this.filtro.filtroDataSigla(this.listadoG, this.campoFiltroG.sigla)*/
  }
  
  /*validarCamposForm(formGroup: FormGroup) {         
    Object.keys(formGroup.controls).forEach(field => {  
      const control = formGroup.get(field);            
      if (control instanceof FormControl) {             
        control.markAsTouched({ onlySelf: true });
      } else if (control instanceof FormGroup) {        
        this.validarCamposForm(control);            
      }  
    });
  }*/

  guardar(list){
    console.log ('Guardando')
    this.loading = true
    
    console.log (list)
    this.query.saveDatosApi(this.url.update,list[0]).subscribe(
            data => {
              this.toastr.success(data['msg'])
              this.seleccionar()
            },
            error =>{
              this.toastr.warning(error['msg'].substring(71,error['msg'].indexOf("(SQL:")),'Importante');
            })
    this.loading = false
  }

  eliminar(list){
    swal.fire({
      title: '¿Continuar?',
      html: 'Se eliminará el  registro <br><b>N° ' + list[0].id+ '-' + list[0].nombre + '.</b><br> <b>¿Desea continuar?</b>',
      //input: 'textarea',
      inputPlaceholder: 'Observación...',
      icon: 'warning',
      showCancelButton: true,
      confirmButtonColor: '#3085d6',
      cancelButtonColor: '#d33',
      confirmButtonText: 'Continuar',
      cancelButtonText: 'Cancelar'
    }).then((result) => {
        if (!result.dismiss) {
         let idE = list[0].id
          this.query.delDatoApi(this.url.delete,idE).subscribe(
            data => {
              let info:any=data 
              
              if(info.datos[0]['iResult'] == 1)
              {
                swal.fire(
                  'Eliminado',
                  info.msg,
                  'success'
                )              
                
                this.seleccionar()
              }else{
                swal.fire(
                  'Cancelado',
                  info.msg,
                  'error'
                )               
              }
            }, 
          error => {
            swal.fire(
              'Cancelado',
              error['error']['message'].substring(71,error['error']['message'].indexOf("(SQL:")),
              'error'
            )
          });
      } else if (result.dismiss === swal.DismissReason.cancel) {
      
      }
    })
    
  }
  
  downloadReport(){
    this.loading = true
    switch (this.typeReport){
      case 'WORD':
        console.log('Descarga WORD')
        break
      case 'EXCEL':
        console.log('Descarga EXCEL')
        break
      default:  // Por defecto PDF
        console.log('Descarga PDF')
        this.downloadPDF()
        break
    }
    this.loading = false
  }
  downloadPDF(){
    this.jsPDF.generaPDF(this.prepararPDF(),1)
  }
  private prepararPDF(){
  // Verificar this.listado
  let listadoPDF:any =[]
  let index:number=1
  this.listado.forEach(element => {
    listadoPDF.push(
      [
        index++,
        element.nombre
      ]
    )
  });
  let estilosColumnas={ 
    0: { halign: 'center'}, 
    1: { halign: 'left' },
    
    };
  let datosPDF={
    orientacion: 'p',  //Un valor distinto a 'p' hará que sea horizontal (Se sugiere 'l')
    titulo: this.titulo,
    
   cabeceras:[
     ['ITEM','TIPO DE DOCUMENTO']
   ],
    datos: listadoPDF,
    columnaStyles: estilosColumnas
  }
  //console.log(datosPDF)
  return datosPDF
  }
}
