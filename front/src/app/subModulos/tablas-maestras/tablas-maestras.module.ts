import { NgModule } from "@angular/core";
import { CommonModule } from "@angular/common";

import { SharedComponentsModule } from "src/app/shared/components/shared-components.module";

import { NgbModule } from "@ng-bootstrap/ng-bootstrap";
import { TablasMaestrasRoutingModule } from "./tablas-maestras-routing.module";
import { GlobalModule } from "../../global/global.module";
import { ReactiveFormsModule, FormsModule } from "@angular/forms";
import { NgxDatatableModule } from "@swimlane/ngx-datatable";
import { NgxPaginationModule } from "ngx-pagination";
import { MatSelectModule } from "@angular/material/select";
import { MatInputModule } from "@angular/material/input";
import { MatFormFieldModule } from "@angular/material/form-field";
import { MatDialogModule } from "@angular/material/dialog";
import { MatStepperModule } from "@angular/material/stepper";
import { MatButtonModule } from "@angular/material/button";
import { MatMenuModule } from "@angular/material/menu";
import { MatIconModule } from "@angular/material/icon";
import { MatTooltipModule } from "@angular/material/tooltip";
import { MatAutocompleteModule } from "@angular/material/autocomplete";
import { NgxSpinnerModule } from "ngx-spinner";
import { EstadosComponent } from "./estados/estados.component";
import { CargosComponent } from "./cargos/cargos.component";
import { EstadosNotaComponent } from "./estados-nota/estados-nota.component";
import { TiposVehiculosComponent } from "./tipos-vehiculos/tipos-vehiculos.component";
import { TiposDocumentosComponent } from "./tipos-documentos/tipos-documentos.component";
import { AlmacenesComponent } from "./almacenes/almacenes.component";
import { PersonalAlmacenesComponent } from "./personal-almacenes/personal-almacenes.component";

@NgModule({
  declarations: [
    EstadosComponent,
    CargosComponent,
    EstadosNotaComponent,
    TiposVehiculosComponent,
    TiposDocumentosComponent,
    AlmacenesComponent,
    PersonalAlmacenesComponent,
  ],
  imports: [
    CommonModule,
    TablasMaestrasRoutingModule,
    SharedComponentsModule,
    MatAutocompleteModule,
    NgbModule,
    GlobalModule,
    ReactiveFormsModule,
    NgxDatatableModule,
    NgxPaginationModule,
    FormsModule,
    MatSelectModule,
    MatInputModule,
    MatFormFieldModule,
    MatInputModule,
    MatDialogModule,
    MatStepperModule,
    MatButtonModule,
    MatMenuModule,
    MatIconModule,
    MatTooltipModule,
    NgxSpinnerModule,
  ],
})
export class TablasMaestrasModule {}
