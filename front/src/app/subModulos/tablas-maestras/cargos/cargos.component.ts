import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { QueryService } from './../../../servicios/query.services'
import { FiltrosService} from './../../../global/services/filtros.service'
import { JsPDFService } from './../../../global/services/jsPDF.service'
import { NgbActiveModal, NgbModal, ModalDismissReasons } from '@ng-bootstrap/ng-bootstrap';
import { FormBuilder, FormControl, FormGroup ,Validators } from '@angular/forms';
import { ToastrService } from 'ngx-toastr';
import {formatDate } from '@angular/common';
import * as jsPDF from 'jspdf'
import swal from 'sweetalert2'

@Component({
  selector: 'app-cargos',
  templateUrl: './cargos.component.html',
  styleUrls: ['./cargos.component.scss']
})
export class CargosComponent implements OnInit {
  titulo:string = 'CARGOS'
  /** listas */
 listado:any =[]
 listadoGral:any =[]
 /*filtros*/
 campoFiltro = {
       id:'',
       nombre:''
     }
 
 /**pagination */
 rows = [];
 iPageSize=10
 p: number = 1;

  /** forms */
  formulario: FormGroup
  formFiltroData: FormGroup;
  tituloForm:string = ''

   /* loading */
   loading:boolean = false
  

   /**check */
   checks:any = []
   chkTodos:boolean = false
   
   /** Tipo de reporte */
   typeReport:string =  'PDF'
   pagReport: number =1

   /**auto-inicio filtro*/
   iOpcionFiltro=0
   /*URLS */
   url = {
     select : '/cargos',
     update: '/cargos_save',
     delete: '/cargos_del'
   }
  constructor(
    private query:QueryService,
    private filtro: FiltrosService,
    private route: ActivatedRoute,
    private toastr: ToastrService,
    private modal:NgbModal,
    private fb: FormBuilder,
    private jsPDF: JsPDFService
  ) { }

  ngOnInit() {
    this.loading=true 
    this.formFiltroData = new FormGroup ({
      iOpcionFiltro:new FormControl(0),
    })
    
    this.seleccionar()

    this.formulario = this.fb.group({
      id: [''],
      nombre: ['', Validators.required],
    })
    // this.loading = false
  }
  modalFormOpen(formulario){
    this.tituloForm = 'Nuevo Cargo'
    
    this.modal.open(formulario, {size : "sm", backdrop: 'static', keyboard: false})
    this.asignarDatosForm(null)
  }
  asignarDatosForm(dato){
    if(dato==null){
      
      this.formulario = this.fb.group({
        id: [''],
        nombre: ['', Validators.required],
      })
    }else{
      
      this.formulario = this.fb.group({
        id: dato[0].id,
        nombre: dato[0].nombre,
      })
    }
  }

  seleccionar(){
    this.loading = true
        
    if(this.formFiltroData.valid) {
      this.formFiltroData.disable()  //Desactivamos los filtros
      this.query.getDatosApi(this.url.select).subscribe(
        data =>{
          this.rows = []
          this.listado = data
          this.listadoGral = this.listado
          this.loading = false
          this.formFiltroData.enable()
        },
        error => {
          this.formFiltroData.enable()
          this.toastr.warning(error['error']['message'].substring(71,error['error']['message'].indexOf("(SQL:")),'Importante');
          this.loading = false
        }
      )        
    }else{
      this.validarCamposForm(this.formFiltroData) 
    }
    // this.loading = false
  }
  filtrar(){
    this.listado =this.listadoGral
    /*
    this.listado=this.filtro.filtroDataId(this.listado, this.campoFiltro.id)
    this.listado=this.filtro.filtroDataNombre(this.listado, this.campoFiltro.nombre)
    */
  }
  
  validarCamposForm(formGroup: FormGroup) {         
    Object.keys(formGroup.controls).forEach(field => {  
      const control = formGroup.get(field);            
      if (control instanceof FormControl) {             
        control.markAsTouched({ onlySelf: true });
      } else if (control instanceof FormGroup) {        
        this.validarCamposForm(control);            
      }  
    });
  }

  guardar(){
  //  console.log ('Guardando')
    if(this.formulario.valid)
    {
      this.formulario.disable(); 
      let formData = {  //let //const // var
        id:this.formulario.value.id,
        nombre:this.formulario.value.nombre,
      };
      this.query.saveDatosApi(this.url.update,formData).subscribe(
        data => {
          this.formulario.enable(); 
          this.modal.dismissAll()
          swal.fire(
            'Éxito!',
            'Se guardó el Cargo Exitosamente!',
            'success'
          )
          
          this.seleccionar()
        },
        error => {
          console.log(formData)
          this.modal.dismissAll()
          this.formulario.enable(); 
          swal.fire(
            'Error!',
            error['error']['message'].substring(71,error['error']['message'].indexOf("(SQL: EXEC")),
            'error'
          )     
        });
      }
      else{
        this.toastr.warning('Complete los campos requeridos','Importante');
      }
  }
  editar(formulario,list){
    this.tituloForm = 'Editando Cargo'
    this.modal.open(formulario, {size : "sm", backdrop: 'static', keyboard: false})
    this.asignarDatosForm(list)
  }
  eliminar(list){
    swal.fire({
      title: '¿Continuar?',
      html: 'Se eliminará el  registro <br><b>N° ' + list[0].id+ '-' + list[0].nombre + '.</b><br> <b>¿Desea continuar?</b>',
      //input: 'textarea',
      inputPlaceholder: 'Observación...',
      icon: 'warning',
      showCancelButton: true,
      confirmButtonColor: '#3085d6',
      cancelButtonColor: '#d33',
      confirmButtonText: 'Continuar',
      cancelButtonText: 'Cancelar'
    }).then((result) => {
        if (!result.dismiss) {
         let idE = list[0].id
          this.query.delDatoApi(this.url.delete,idE).subscribe(
            data => {
              let info:any=data 
              
              if(info.datos[0]['iResult'] == 1)
              {
                swal.fire(
                  'Eliminado',
                  info.msg,
                  'success'
                )              
                
                this.seleccionar()
              }else{
                swal.fire(
                  'Cancelado',
                  info.msg,
                  'error'
                )               
              }
            }, 
          error => {
            swal.fire(
              'Cancelado',
              error['error']['message'].substring(71,error['error']['message'].indexOf("(SQL:")),
              'error'
            )
          });
      } else if (result.dismiss === swal.DismissReason.cancel) {
      
      }
    })
    
  }
  
  downloadReport(){
    this.loading = true
    switch (this.typeReport){
      case 'WORD':
        console.log('Descarga WORD')
        break
      case 'EXCEL':
        console.log('Descarga EXCEL')
        break
      default:  // Por defecto PDF
        console.log('Descarga PDF')
        this.downloadPDF()
        break
    }
    this.loading = false
  }
  downloadPDF(){
    //this.jsPDF.generaPDF_TB(this.titulo,this.listado,this.campoFiltro)
  }
}
