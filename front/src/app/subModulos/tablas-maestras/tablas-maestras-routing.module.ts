import { NgModule } from "@angular/core";
import { Routes, RouterModule } from "@angular/router";
import { CommonModule } from "@angular/common";

import { FormsModule } from "@angular/forms";
import { NgbModule } from "@ng-bootstrap/ng-bootstrap";
import { ReactiveFormsModule } from "@angular/forms";

import { NgxPaginationModule } from "ngx-pagination";

import { NgxDatatableModule } from "@swimlane/ngx-datatable";
import { ToastrModule } from "ngx-toastr";

import { GlobalModule } from "./../../global/global.module";
import { SharedComponentsModule } from "./../../shared/components/shared-components.module";
import { EstadosComponent } from "./estados/estados.component";
import { CargosComponent } from "./cargos/cargos.component";
import { EstadosNotaComponent } from "./estados-nota/estados-nota.component";
import { TiposVehiculosComponent } from "./tipos-vehiculos/tipos-vehiculos.component";
import { TiposDocumentosComponent } from "./tipos-documentos/tipos-documentos.component";
import { AlmacenesComponent } from "./almacenes/almacenes.component";

import { PersonalAlmacenesComponent } from "./personal-almacenes/personal-almacenes.component";

const routes: Routes = [
  { path: "estados", component: EstadosComponent },
  { path: "tipos_veh", component: TiposVehiculosComponent },
  { path: "estados_nota", component: EstadosNotaComponent },
  { path: "almacenes", component: AlmacenesComponent },
  { path: "tipos_doc", component: TiposDocumentosComponent },
  {
    path: "personal",
    component: PersonalAlmacenesComponent,
  },
];

@NgModule({
  imports: [
    RouterModule.forChild(routes),
    NgbModule.forRoot(),
    ToastrModule.forRoot(),
    CommonModule,
    GlobalModule,
    SharedComponentsModule,
  ],
  exports: [RouterModule],
  declarations: [],
})
export class TablasMaestrasRoutingModule {}
