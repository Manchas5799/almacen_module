import { JsPDFService } from "./../../../global/services/jsPDF.service";
import { environment } from "./../../../../environments/environment";
import { QueryService } from "src/app/servicios/query.services";
import { LocalService } from "./../../../servicios/local.services";
import { Component, OnInit } from "@angular/core";
import { FormBuilder, FormGroup, Validators } from "@angular/forms";
import { endOfMonth, startOfMonth } from "date-fns";
import * as moment from "moment";

@Component({
  selector: "app-bienes-rp",
  templateUrl: "./bienes-rp.component.html",
  styleUrls: ["./bienes-rp.component.scss"],
})
export class BienesRpComponent implements OnInit {
  almacenActual: any = null;
  listaLugares: any = [];
  formularioGeneral: FormGroup;
  rutas = {
    entradas: "/reportes/entradas",
    salidas: "/reportes/salidas",
    saldos: "/reportes/saldos",
    select_neas: "/secundario/neas",
  };
  // NEAS
  loading = false;
  listaNeasFiltradas: any = [];
  listaNeasGrl: any = [];
  listaNeas: any = [];
  constructor(
    private local: LocalService,
    private fb: FormBuilder,
    private query: QueryService,
    private jsPDF: JsPDFService
  ) {}

  ngOnInit() {
    this.formularioGeneral = this.fb.group({
      lugar: [null, Validators.required],
      fechaInicial: [
        moment(startOfMonth(new Date())).format("YYYY-MM-DD"),
        Validators.required,
      ],
      fechaFinal: [
        moment(endOfMonth(new Date())).format("YYYY-MM-DD"),
        [Validators.required],
      ],
    });
    this.obtenerAlmacen();
  }
  obtenerAlmacen() {
    let almacen = this.local.getItem("almacenElegido");
    this.almacenActual = almacen;
    almacen.lugares = JSON.parse(almacen.lugares);
    // this.idAlmacenActual = almacen.id;
    this.listaLugares = almacen.lugares;
  }
  reportesPorEntradas() {
    if (this.formularioGeneral.valid) {
      this.query.getPdfAlmacen(
        `${environment.rutas.backEnd}${this.rutas.entradas}/${this.formularioGeneral.value.lugar.lugar_id}/${this.almacenActual.id}/${this.formularioGeneral.value.fechaInicial}/${this.formularioGeneral.value.fechaFinal}`
      );
    }
  }
  reportesPorSalidas() {
    if (this.formularioGeneral.valid) {
      this.query.getPdfAlmacen(
        `${environment.rutas.backEnd}${this.rutas.salidas}/${this.formularioGeneral.value.lugar.lugar_id}/${this.almacenActual.id}/${this.formularioGeneral.value.fechaInicial}/${this.formularioGeneral.value.fechaFinal}`
      );
    }
  }
  reportesPorSaldos() {
    if (this.formularioGeneral.valid) {
      this.query.getPdfAlmacen(
        `${environment.rutas.backEnd}${this.rutas.saldos}/${this.formularioGeneral.value.lugar.lugar_id}/${this.almacenActual.id}`
      );
    }
  }
  reporteNeas() {
    if (this.formularioGeneral.valid) {
      this.cargarNeas(this.formularioGeneral.value.lugar.lugar_id);
    }
  }
  cargarNeas(lugar) {
    this.query
      .getDatosPostApi(this.rutas.select_neas, {
        lugar: lugar,
        nro: "",
      })
      .subscribe({
        next: (res: any) => {
          res.map(
            (e) => (e.valor_total = parseFloat(e.valor_total).toFixed(4))
          );
          this.listaNeasGrl = res;
          this.listaNeas = res;
        },
        complete: () => {
          this.loading = false;
          this.downloadPdf();
        },
      });
  }
  downloadPdf() {
    this.loading = true;
    this.generarPdf();
    this.loading = false;
  }
  generarPdf() {
    this.jsPDF.generaPDF(this.prepararCuerpoNea(), 1, this.prepararNea());
  }
  prepararCuerpoNea() {
    let listadoPDF: any = [];
    let index: number = 1;

    this.listaNeas.forEach((element, key) => {
      listadoPDF.push([
        key + 1,
        element.NRO_NEA,
        element.codigo,
        element.bien,
        element.und_medida,
        element.cuenta,
        element.tipo_uso,
        element.marca,
        element.cant_total,
        element.precio_unit,
        element.valor_total,
      ]);
    });
    let estilosColumnas = {
      0: { halign: "left" },
      1: { halign: "left" },
      2: { halign: "left" },
      3: { halign: "left" },
      4: { halign: "left" },
      5: { halign: "right" },
      6: { halign: "center" },
      7: { halign: "left" },
      8: { halign: "right" },
      9: { halign: "right" },
      10: { halign: "right" },
    };
    let datosPDF = {
      orientacion: "l",
      titulo: "NOTA DE ENTRADA A ALMACÉN",
      cabeceras: [
        [
          "N°",
          "N° NEA",
          "CÓDIGO",
          "DESCRIPCIÓN",
          "U/M",
          "CUENTA",
          "TIPO USO",
          "MARCA",
          "CANT.",
          "P.U.",
          "COSTO TOTAL",
        ],
      ],
      // tipotitulo: 1,
      datos: listadoPDF,
      columnaStyles: estilosColumnas,
    };
    //console.log(datosPDF)
    return datosPDF;
  }
  prepararNea() {
    let listadoPDF: any = [];
    let index: number = 1;

    listadoPDF[0] = [this.formularioGeneral.value.lugar.lugar_nombre];
    let estilosColumnas = {
      0: { halign: "left" },
      // 6: { halign: "left" },
      // 7: { halign: "left" },
      // 8: { halign: "left" },
    };
    let datosPDF = {
      headers: [["ALMACÉN"]],
      // tipotitulo: 1,
      data: listadoPDF,
      columnaStyles: estilosColumnas,
    };
    //console.log(datosPDF)
    return datosPDF;
  }
}
