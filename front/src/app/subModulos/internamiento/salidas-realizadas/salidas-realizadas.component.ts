import { Component, OnInit } from "@angular/core";
import { FormBuilder } from "@angular/forms";
import { NgbModal } from "@ng-bootstrap/ng-bootstrap";
import * as moment from "moment";
import { ToastrService } from "ngx-toastr";
import { FiltrosService } from "src/app/global/services/filtros.service";
import { JsPDFService } from "src/app/global/services/jsPDF.service";
import { LocalService } from "src/app/servicios/local.services";
import { QueryService } from "src/app/servicios/query.services";
import { environment } from "src/environments/environment";

@Component({
  selector: "app-salidas-realizadas",
  templateUrl: "./salidas-realizadas.component.html",
  styleUrls: ["./salidas-realizadas.component.scss"],
})
export class SalidasRealizadasComponent implements OnInit {
  loading: boolean = false;
  salidaTodos: boolean = false;
  movimientoActual: number;
  lugarTodo: any;
  bienesActuales: any = [];
  listaLugares: any = [];
  listaTotalPendientes: any = [];
  listaPendientes: any = [];
  detallesMovimiento: any = [];
  idAlmacenActual: number;
  fechaDetalle: string = moment().format("YYYY-MM-DD").toString();
  rutas = {
    select_pendientes: "/movimientos/salidas_realizadas/lugar",
    select_detalle: "/movimientos/entradas_pendientes/detalle/",
    aceptar_movimiento: "/movimientos/entradas_pendientes/aceptar",
  };
  titulo = "SALIDAS REALIZADAS";
  campoFiltro = {
    alm_destino: "",
    articulo: "",
    cantidad: "",
    estado_nota: "",
    solicitante: "",
    glosa: "",
    observaciones: "",
    fecha_recep: "",
    recepcionista: "",
  };
  iPageSize = environment.paginacion.tamañoPag;
  p = 1;
  pDM = 1;
  constructor(
    private query: QueryService,
    private modal: NgbModal,
    private toast: ToastrService,
    private filtro: FiltrosService,
    private fb: FormBuilder,
    private jsPDF: JsPDFService,
    private local: LocalService
  ) {}

  ngOnInit() {
    this.obtenerAlmacen();
  }
  obtenerAlmacen() {
    let almacen = this.local.getItem("almacenElegido");
    almacen = almacen;
    almacen.lugares = JSON.parse(almacen.lugares);
    this.idAlmacenActual = almacen.id;
    this.listaLugares = almacen.lugares;
    this.lugarTodo = this.listaLugares[0].lugar_id;
    this.filtrarXLugares({ value: this.lugarTodo });
  }
  filtrarXLugares(event) {
    this.loading = true;
    const data = {
      lugar: event.value,
    };
    this.cargarMovimientosPendientes(data);
  }
  cargarMovimientosPendientes(data) {
    this.query.getDatosPostApi(this.rutas.select_pendientes, data).subscribe({
      next: (res) => {
        this.listaTotalPendientes = res;
        this.listaPendientes = res;
      },
      complete: () => {
        this.loading = false;
      },
    });
  }
  filtrar() {
    this.listaPendientes = this.listaTotalPendientes;
    this.listaPendientes = this.filtro.filtroData(
      this.listaPendientes,
      this.campoFiltro.observaciones,
      1
    );
    this.listaPendientes = this.filtro.filtroData(
      this.listaPendientes,
      this.campoFiltro.glosa,
      2
    );
    this.listaPendientes = this.filtro.filtroData(
      this.listaPendientes,
      this.campoFiltro.solicitante,
      3
    );
    this.listaPendientes = this.filtro.filtroData(
      this.listaPendientes,
      this.campoFiltro.recepcionista,
      4
    );
    this.listaPendientes = this.filtro.filtroData(
      this.listaPendientes,
      this.campoFiltro.fecha_recep,
      5
    );
    this.listaPendientes = this.filtro.filtroData(
      this.listaPendientes,
      this.campoFiltro.articulo,
      6
    );
    this.listaPendientes = this.filtro.filtroData(
      this.listaPendientes,
      this.campoFiltro.cantidad,
      7
    );
    this.listaPendientes = this.filtro.filtroData(
      this.listaPendientes,
      this.campoFiltro.alm_destino,
      9
    );
    this.listaPendientes = this.filtro.filtroData(
      this.listaPendientes,
      this.campoFiltro.estado_nota,
      11
    );
    ////
  }
  mostrarDetalles(modal, registro) {
    this.abrirModal(modal);
    this.cargarDetalleMovimiento(registro);
    this.movimientoActual = registro.id;
    this.bienesActuales = registro.bienes;
  }
  abrirModal(modal, sz: "lg" | "sm" = "lg") {
    this.modal.open(modal, {
      backdrop: "static",
      keyboard: false,
      size: sz,
    });
  }
  cargarDetalleMovimiento(movimiento) {
    this.fechaDetalle = moment().format("YYYY-MM-DD").toString();
    this.loading = true;
    this.detallesMovimiento = [];
    this.query.getDatosApi(this.rutas.select_detalle + movimiento.id).subscribe(
      (res: any) => {
        res.map((item) => {
          item.cant = parseFloat(item.cant).toFixed(2);
          item.precio = parseFloat(item.precio).toFixed(2);
          item.saldo = parseFloat(item.saldo).toFixed(2);
          item.pendiente = parseFloat(item.pendiente).toFixed(2);
        });
        this.detallesMovimiento = res;

        // this.detallesMovimiento.forEach((item, key) => {
        //   item.saldo = this.bienesActuales[key].saldo;
        // })
        this.loading = false;
      },
      (error) => {
        this.loading = false;
        console.log(error);
      }
    );
  }
  aceptarMovimiento() {
    this.loading = true;
    const data = {
      movimiento: this.movimientoActual,
      fecha: this.fechaDetalle,
    };
    this.query.saveDatosApi(this.rutas.aceptar_movimiento, data).subscribe({
      next: (res: any) => {
        if (res.error) {
          this.toast.error(res.msg, "PROBLEMAS AL ACEPTAR");
        } else {
          this.toast.success(res.msg, "OPERACION COMPLETADA");
          this.modal.dismissAll();
        }
      },
      complete: () => {
        this.loading = false;
        this.cargarMovimientosPendientes({ lugar: this.lugarTodo });
      },
      error: (e) => {
        console.log(e);
        this.loading = false;
      },
    });
  }
}
