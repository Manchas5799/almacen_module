import { NeaPrincipalListadoComponent } from './nea-principal-listado/nea-principal-listado.component';
import { NeaListadoComponent } from './nea-listado/nea-listado.component';
import { SalidasRealizadasComponent } from './salidas-realizadas/salidas-realizadas.component';
import { BienesPorEntradasComponent } from './bienes-por-entradas/bienes-por-entradas.component';
import { EntradasPendientesAlmComponent } from './entradas-pendientes-alm/entradas-pendientes-alm.component';
import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

const routes: Routes = [
  {
    path: "",
    component: EntradasPendientesAlmComponent
  },
  {
    path: "bienes/:tipo",
    component: BienesPorEntradasComponent
  },
  {
    path: "salidas",
    component: SalidasRealizadasComponent
  },
  {
    path: "salidas/neas",
    component: NeaListadoComponent
  },
  {
    path: "neas-principal",
    component: NeaPrincipalListadoComponent
  }

];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class InternamientoRoutingModule { }
