import { environment } from "src/environments/environment";
import { Component, OnInit } from "@angular/core";
import { FormBuilder } from "@angular/forms";
import { ActivatedRoute } from "@angular/router";
import { NgbModal } from "@ng-bootstrap/ng-bootstrap";
import { ToastrService } from "ngx-toastr";
import { FiltrosService } from "src/app/global/services/filtros.service";
import { JsPDFService } from "src/app/global/services/jsPDF.service";
import { LocalService } from "src/app/servicios/local.services";
import { QueryService } from "src/app/servicios/query.services";

@Component({
  selector: "app-nea-listado",
  templateUrl: "./nea-listado.component.html",
  styleUrls: ["./nea-listado.component.scss"],
})
export class NeaListadoComponent implements OnInit {
  listaNeasGrl: any = [];
  idAlmacenActual: number;
  listaNeas: any = [];
  listaLugares: any = [];
  lugarTodo: any = null;
  rutas = {
    select_neas: "/secundario/neas",
  };

  campoFiltro = {
    nro_nea: "",
    codigo: "",
    articulo: "",
    uni_medida: "",
    cuenta: "",
    tipo_uso: "",
    marca: "",
    cant_total: "",
    precio: "",
    costo: "",
  };
  nroNea: string = "";
  loading: boolean = false;
  titulo: string = "NEAs REALIZADAS";
  iPageSize = environment.paginacion.tamañoPag;
  p: number = 1;
  constructor(
    private query: QueryService,
    private modal: NgbModal,
    private toast: ToastrService,
    private filtro: FiltrosService,
    private fb: FormBuilder,
    private jsPDF: JsPDFService,
    private local: LocalService,
    private route: ActivatedRoute
  ) {}

  ngOnInit() {
    this.obtenerAlmacen();
  }
  obtenerAlmacen() {
    let almacen = this.local.getItem("almacenElegido");
    almacen = almacen;
    almacen.lugares = JSON.parse(almacen.lugares);
    this.idAlmacenActual = almacen.id;
    this.listaLugares = almacen.lugares;
    this.lugarTodo = this.listaLugares[0];
    this.filtrarXLugares({ value: this.lugarTodo });
  }
  filtrarXLugares(event) {
    this.loading = true;
    this.cargarNeas(event.value.lugar_id);
  }
  cargarNeas(lugar) {
    this.query
      .getDatosPostApi(this.rutas.select_neas, {
        lugar: lugar,
        nro: this.nroNea,
      })
      .subscribe({
        next: (res: any) => {
          res.map((e) => (e.valor_total = parseFloat(e.valor_total).toFixed(4)));
          this.listaNeasGrl = res;
          this.listaNeas = res;
        },
        complete: () => {
          this.loading = false;
        },
      });
  }
  filtrar() {
    this.listaNeas = this.listaNeasGrl;
    this.listaNeas = this.filtro.filtroData(
      this.listaNeas,
      this.campoFiltro.nro_nea,
      0
    );
    this.listaNeas = this.filtro.filtroData(
      this.listaNeas,
      this.campoFiltro.codigo,
      1
    );
    this.listaNeas = this.filtro.filtroData(
      this.listaNeas,
      this.campoFiltro.articulo,
      2
    );
    this.listaNeas = this.filtro.filtroData(
      this.listaNeas,
      this.campoFiltro.uni_medida,
      3
    );
    this.listaNeas = this.filtro.filtroData(
      this.listaNeas,
      this.campoFiltro.cuenta,
      4
    );
    this.listaNeas = this.filtro.filtroData(
      this.listaNeas,
      this.campoFiltro.tipo_uso,
      5
    );
    this.listaNeas = this.filtro.filtroData(
      this.listaNeas,
      this.campoFiltro.marca,
      6
    );
    this.listaNeas = this.filtro.filtroData(
      this.listaNeas,
      this.campoFiltro.cant_total,
      7
    );
    this.listaNeas = this.filtro.filtroData(
      this.listaNeas,
      this.campoFiltro.precio,
      8
    );
    this.listaNeas = this.filtro.filtroData(
      this.listaNeas,
      this.campoFiltro.costo,
      9
    );
  }
  downloadPdf() {
    this.loading = true;
    this.generarPdf();
    this.loading = false;
  }
  generarPdf() {
    this.jsPDF.generaPDF(this.prepararCuerpoNea(), 1, this.prepararNea());
  }
  prepararCuerpoNea() {
    let listadoPDF: any = [];
    let index: number = 1;

    this.listaNeas.forEach((element, key) => {
      listadoPDF.push([
        key + 1,
        element.NRO_NEA,
        element.codigo,
        element.bien,
        element.und_medida,
        element.cuenta,
        element.tipo_uso,
        element.marca,
        element.cant_total,
        element.precio_unit,
        element.valor_total,
      ]);
    });
    let estilosColumnas = {
      0: { halign: "left" },
      1: { halign: "left" },
      2: { halign: "left" },
      3: { halign: "left" },
      4: { halign: "left" },
      5: { halign: "right" },
      6: { halign: "center" },
      7: { halign: "left" },
      8: { halign: "right" },
      9: { halign: "right" },
      10: { halign: "right" },
    };
    let datosPDF = {
      orientacion: "l",
      titulo: "NOTA DE ENTRADA A ALMACÉN",
      cabeceras: [
        [
          "N°",
          "N° NEA",
          "CÓDIGO",
          "DESCRIPCIÓN",
          "U/M",
          "CUENTA",
          "TIPO USO",
          "MARCA",
          "CANT.",
          "P.U.",
          "COSTO TOTAL",
        ],
      ],
      // tipotitulo: 1,
      datos: listadoPDF,
      columnaStyles: estilosColumnas,
    };
    //console.log(datosPDF)
    return datosPDF;
  }
  prepararNea() {
    let listadoPDF: any = [];
    let index: number = 1;

    listadoPDF[0] = [this.lugarTodo.lugar_nombre];
    let estilosColumnas = {
      0: { halign: "left" },
      // 6: { halign: "left" },
      // 7: { halign: "left" },
      // 8: { halign: "left" },
    };
    let datosPDF = {
      headers: [["ALMACÉN"]],
      // tipotitulo: 1,
      data: listadoPDF,
      columnaStyles: estilosColumnas,
    };
    //console.log(datosPDF)
    return datosPDF;
  }
}
