import { MatCheckboxChange } from "@angular/material/checkbox";
import { Component, OnInit } from "@angular/core";
import { FormBuilder, FormGroup, Validators } from "@angular/forms";
import { ActivatedRoute } from "@angular/router";
import { NgbModal } from "@ng-bootstrap/ng-bootstrap";
import * as moment from "moment";
import { ToastrService } from "ngx-toastr";
import { FiltrosService } from "src/app/global/services/filtros.service";
import { JsPDFService } from "src/app/global/services/jsPDF.service";
import { LocalService } from "src/app/servicios/local.services";
import { QueryService } from "src/app/servicios/query.services";
import { environment } from "src/environments/environment";

@Component({
  selector: "app-nea-principal-listado",
  templateUrl: "./nea-principal-listado.component.html",
  styleUrls: ["./nea-principal-listado.component.scss"],
})
export class NeaPrincipalListadoComponent implements OnInit {
  listaNeasGrl: any = [];
  idAlmacenActual: number;
  listaNeas: any = [];
  listaNeasFiltradas: any = [];
  listaNumerosNeas: string[] = [];
  listaLugares: any = [];
  lugarTodo: any = null;
  salidaTodos: boolean = false;
  rutas = {
    select_neas: "/movimientos/neas",
    upd_nea: "/movimientos/neas/aceptar",
    almacenes: "/almacenes_all",
    save_nea_out: "/lugar_movimiento",
    select_detalle: "/siga_movimientos/",
  };

  campoFiltro = {
    nro_nea: "",
    codigo: "",
    articulo: "",
    uni_medida: "",
    cuenta: "",
    tipo_uso: "",
    marca: "",
    cant_total: "",
    precio: "",
    costo: "",
    lugar_nombre: "",
  };
  nroNea: string = "";
  loading: boolean = false;
  titulo: string = "NEAS REALIZADAS";
  iPageSize = environment.paginacion.tamañoPag;
  p: number = 1;
  pDM: number = 1;
  actualizacionForm: FormGroup;
  formatoNea: FormGroup;
  ///////////
  almacenes: any = [];
  pecosaActual: string = "";
  movimientoActual: number;
  movimientoSeleccionado: any;
  lugarDestino: number;
  fechaPecosa: string = moment().format("YYYY-MM-DD").toString();
  fechaDetalles: string = moment().format("YYYY-MM-DD").toString();
  detallesMovimiento: any = [];
  nro_nea_selected: string;
  constructor(
    private query: QueryService,
    private modal: NgbModal,
    private toast: ToastrService,
    private filtro: FiltrosService,
    private fb: FormBuilder,
    private jsPDF: JsPDFService,
    private local: LocalService,
    private route: ActivatedRoute
  ) {}

  ngOnInit() {
    this.cargarNeas();
    this.actualizacionForm = this.fb.group({
      n_nea_old: [null, Validators.required],
      n_nea_new: ["", Validators.required],
    });
    this.formatoNea = this.fb.group({
      n_nea: [null, Validators.required],
    });
  }
  descargarPDF() {
    this.loading = true;
    this.generarFormatoNea();
    this.loading = false;
  }
  generarFormatoNea() {
    const data = this.formatoNea.value;
    this.nro_nea_selected = data.n_nea;
    this.jsPDF.generaPDF(this.prepararPDFNea(), 1);
  }
  private prepararPDFNea() {
    // Verificar this.listado
    let listadoPDF: any = [];
    let index: number = 1;
    this.listaNeasGrl.forEach((element) => {
      if (element.NRO_NEA === this.nro_nea_selected) {
        listadoPDF.push([
          index++,
          element.NRO_NEA,
          element.cuenta,
          element.codigo,
          element.bien,
          element.und_medida,
          element.marca,
          parseFloat(element.cant_total).toFixed(4),
          parseFloat(element.precio_unit).toFixed(4),
          parseFloat(element.valor_total).toFixed(4),
        ]);
      }
    });
    console.log(
      listadoPDF,
      this.listaNeasGrl[0].NRO_NEA,
      this.nro_nea_selected
    );
    let estilosColumnas = {
      0: { halign: "left" },
      1: { halign: "center" },
      2: { halign: "right" },
      3: { halign: "right" },
      4: { halign: "left" },
      5: { halign: "center" },
      6: { halign: "center" },
      7: { halign: "right" },
      8: { halign: "right" },
      9: { halign: "right" },
    };
    let datosPDF = {
      orientacion: "L", //Un valor distinto a 'p' hará que sea horizontal (Se sugiere 'l')
      titulo: "NOTA DE ENTRADA A ALMACÉN",

      cabeceras: [
        [
          "N°",
          "N° NEA",
          "CUENTA",
          "CÓDIGO",
          "DESCRIPCIÓN",
          "U/M",
          "MARCA",
          "CANT.",
          "P.U.",
          "SUB-TOTAL",
        ],
      ],
      datos: listadoPDF,
      columnaStyles: estilosColumnas,
    };
    //console.log(datosPDF)
    return datosPDF;
  }
  // private generarCabeceraKardex() {
  //   let listadoPDF: any = [];

  //   listadoPDF.push([
  //     this.bienActual.codigo,
  //     this.bienActual.articulo,
  //     this.bienActual.uni_med,
  //     this.bienActual.anio,
  //   ]);
  //   let estilosColumnas = {
  //     0: { halign: "left" },
  //     1: { halign: "left" },
  //     2: { halign: "left" },
  //     3: { halign: "left" },
  //   };
  //   let datosPDF = {
  //     headers: [["CÓDIGO", "ARTICULO", "UNI. MEDIDA", "NRO ORDEN - AÑO"]],
  //     data: listadoPDF,
  //     columnaStyles: estilosColumnas,
  //   };
  //   //console.log(datosPDF)
  //   return datosPDF;
  // }
  ///////////////////
  cargarNeas() {
    this.loading = true;
    this.listaNeasFiltradas = [];
    this.query.getDatosApi(this.rutas.select_neas).subscribe({
      next: (res: any) => {
        res.map((e) => (e.vaor_total = parseFloat(e.vaor_total).toFixed(4)));
        this.listaNeasGrl = res;
        this.listaNeas = res;
        this.listaNeasGrl.forEach((e) => {
          this.listaNumerosNeas.push(e.NRO_NEA);
        });
        this.listaNeasFiltradas = this.listaNumerosNeas.filter(
          (el, index) => this.listaNumerosNeas.indexOf(el) === index
        );
      },
      complete: () => {
        this.loading = false;
      },
    });
  }
  filtrar() {
    this.listaNeas = this.listaNeasGrl;
    this.listaNeas = this.filtro.filtroData(
      this.listaNeas,
      this.campoFiltro.nro_nea,
      0
    );
    this.listaNeas = this.filtro.filtroData(
      this.listaNeas,
      this.campoFiltro.codigo,
      1
    );
    this.listaNeas = this.filtro.filtroData(
      this.listaNeas,
      this.campoFiltro.articulo,
      2
    );
    this.listaNeas = this.filtro.filtroData(
      this.listaNeas,
      this.campoFiltro.uni_medida,
      3
    );
    this.listaNeas = this.filtro.filtroData(
      this.listaNeas,
      this.campoFiltro.cuenta,
      4
    );
    this.listaNeas = this.filtro.filtroData(
      this.listaNeas,
      this.campoFiltro.tipo_uso,
      5
    );
    this.listaNeas = this.filtro.filtroData(
      this.listaNeas,
      this.campoFiltro.marca,
      6
    );
    this.listaNeas = this.filtro.filtroData(
      this.listaNeas,
      this.campoFiltro.cant_total,
      7
    );
    this.listaNeas = this.filtro.filtroData(
      this.listaNeas,
      this.campoFiltro.precio,
      8
    );
    this.listaNeas = this.filtro.filtroData(
      this.listaNeas,
      this.campoFiltro.costo,
      9
    );
    this.listaNeas = this.filtro.filtroData(
      this.listaNeas,
      this.campoFiltro.lugar_nombre,
      10
    );
  }
  mostrarFormularioAceptacionNea(modal) {
    this.modal.open(modal, {
      backdrop: "static",
      keyboard: false,
      size: "sm",
    });
  }
  actualizarNea() {
    this.loading = true;
    const data = this.actualizacionForm.value;
    this.query.saveDatosApi(this.rutas.upd_nea, data).subscribe(
      (res: any) => {
        if (!res.error) {
          this.toast.success(res.mensaje, "OPERACION TERMINADA");
        }
        this.modal.dismissAll();
        this.loading = false;
        this.cargarNeas();
      },
      (error) => {
        this.toast.error(error.mensaje, "OPERACION CANCELADA");
      }
    );
  }
  // SALIDA DE LA NEA
  mostrarDetalles(modal, registro) {
    this.mostrarFormularioAceptacionNea(modal);
    this.cargarDetalleMovimiento(registro);
    this.movimientoActual = registro.id;
    this.movimientoSeleccionado = registro;
  }
  cargarDetalleMovimiento(movimiento) {
    this.loading = true;
    this.salidaTodos = false;
    this.lugarDestino = movimiento.iLugarId ? movimiento.iLugarId : null;
    this.pecosaActual = "";
    this.detallesMovimiento = [];
    // this.query.getDatosApi(this.rutas.select_detalle + movimiento.id).subscribe(
    //   (res) => {
    //     this.detallesMovimiento = res;
    //     this.detallesMovimiento.map((item) => {
    //       item.elegido = false;
    //       item.cantidad_seleccionada = 0;
    //     });
    //     this.loading = false;
    //   },
    //   (error) => {
    //     this.loading = false;
    //     console.log(error);
    //   }
    // );
  }
  seleccionarTodo(evento: MatCheckboxChange) {
    this.detallesMovimiento.map((item) => {
      item.elegido = evento.checked;
      item.cantidad_seleccionada = evento.checked ? item.CANT_ARTICULO : 0;
    });
  }
  seleccionarDetalleMovimiento(accion: MatCheckboxChange, idItem: number) {
    this.detallesMovimiento.forEach((item) => {
      if (item.iSIGADetalleId === idItem) {
        item.elegido = accion.checked;
        item.cantidad_seleccionada = accion.checked ? item.CANT_ARTICULO : 0;
      }
    });
    if (accion.checked === false) {
      this.salidaTodos = false;
      return;
    }
    if (!this.detallesMovimiento.some((item) => item.elegido === false)) {
      this.salidaTodos = true;
    }
  }
  modificarCantidad(evento, id: number, maximo: string) {
    const nmax: number = parseFloat(maximo);
    const nuevoValor: number = parseFloat(evento.target.value);
    let valorChecked = true;
    if (nuevoValor > nmax) {
      evento.target.value = maximo;
    }
    if (nuevoValor <= 0) {
      evento.target.value = 0;
      valorChecked = false;
    }
    this.detallesMovimiento.forEach((item) => {
      if (item.iSIGADetalleId === id) {
        item.cantidad_seleccionada = evento.target.value;
        item.elegido = valorChecked;
      }
    });
    if (valorChecked === false) {
      this.salidaTodos = false;
      return;
    }
    if (!this.detallesMovimiento.some((item) => item.elegido === false)) {
      this.salidaTodos = true;
    }
  }
  validarAlgunElemento() {
    return (
      // !this.detallesMovimiento.some((item) => item.elegido === true) ||
      this.lugarDestino === null || this.fechaDetalles === ""
    );
  }
  darSalidaTodo() {
    const data = {
      detalles: this.detallesMovimiento,
      lugar: this.lugarDestino,
      movimiento: this.movimientoActual,
      fecha: this.fechaDetalles,
      pecosa: this.pecosaActual,
      fecha_pecosa: this.fechaPecosa,
    };
    this.loading = true;
    this.query.saveDatosApi(this.rutas.save_nea_out, data).subscribe({
      next: (res: any) => {
        if (res.error) {
          this.toast.error(res.msg, "Error");
          return;
        }
        this.toast.success(res.msg, "Éxito!");
      },
      complete: () => {
        this.loading = false;
        this.cargarNeas();
        this.modal.dismissAll();
      },
      error: () => {
        this.loading = false;
      },
    });
  }
}
