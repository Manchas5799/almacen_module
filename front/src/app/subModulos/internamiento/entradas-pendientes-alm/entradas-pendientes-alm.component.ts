import { forkJoin } from "rxjs";
import { LocalService } from "./../../../servicios/local.services";
import { JsPDFService } from "./../../../global/services/jsPDF.service";
import { FormBuilder } from "@angular/forms";
import { FiltrosService } from "./../../../global/services/filtros.service";
import { QueryService } from "./../../../servicios/query.services";
import { SelectionChange } from "@angular/cdk/collections";
import { Component, OnInit } from "@angular/core";
import { NgbModal } from "@ng-bootstrap/ng-bootstrap";
import { ToastrService } from "ngx-toastr";
import { environment } from "src/environments/environment";
import * as moment from "moment";
import Swal from "sweetalert2";

@Component({
  selector: "app-entradas-pendientes-alm",
  templateUrl: "./entradas-pendientes-alm.component.html",
  styleUrls: ["./entradas-pendientes-alm.component.scss"],
})
export class EntradasPendientesAlmComponent implements OnInit {
  loading: boolean = false;
  loadingDetalle: boolean = false;
  salidaTodos: boolean = false;
  movimientoActual: number;
  lugarTodo: any;
  bienesActuales: any = [];
  listaLugares: any = [];
  listaTotalPendientes: any = [];
  listaPendientes: any = [];
  listaTotalPendientesSecundarios: any = [];
  listaPendientesSecundarios: any = [];
  detallesMovimiento: any = [];
  idAlmacenActual: number;
  fechaDetalle: string = moment().format("YYYY-MM-DD").toString();
  rutas = {
    select_pendientes: "/movimientos/entradas_pendientes/lugar",
    select_pendientes_secuns:
      "/movimientos/entradas_pendientes_secundarios/lugar",
    select_detalle: "/movimientos/entradas_pendientes/detalle/",
    aceptar_movimiento: "/movimientos/entradas_pendientes/aceptar",
  };
  titulo = "ENTRADAS PENDIENTES";
  campoFiltro = {
    nro_orden: "",
    nro_guia: "",
    tipo_mvto: "",
    prov: "",
    glosa: "",
    obs: "",
    /////////
    alm_destino: "",
    alm_origen: "",
    articulo: "",
    cantidad: "",
    estado_nota: "",
    solicitante: "",
    glosa_secun: "",
    observaciones: "",
    fecha_recep: "",
    recepcionista: "",
  };
  iPageSize = environment.paginacion.tamañoPag;
  p = 1;
  pDM = 1;
  pSecun = 1;
  constructor(
    private query: QueryService,
    private modal: NgbModal,
    private toast: ToastrService,
    private filtro: FiltrosService,
    private fb: FormBuilder,
    private jsPDF: JsPDFService,
    private local: LocalService
  ) {}

  ngOnInit() {
    this.obtenerAlmacen();
  }
  obtenerAlmacen() {
    let almacen = this.local.getItem("almacenElegido");
    almacen = almacen;
    almacen.lugares = JSON.parse(almacen.lugares);
    this.idAlmacenActual = almacen.id;
    this.listaLugares = almacen.lugares;
    this.lugarTodo = this.listaLugares[0];
    this.filtrarXLugares({ value: this.lugarTodo });
  }
  filtrarXLugares(event) {
    this.loading = true;
    const data = {
      lugar: event.value.lugar_id,
    };
    this.cargarMovimientosPendientes(data);
  }
  cargarMovimientosPendientes(data) {
    forkJoin({
      pen: this.query.getDatosPostApi(this.rutas.select_pendientes, data),
      pens: this.query.getDatosPostApi(
        this.rutas.select_pendientes_secuns,
        data
      ),
    }).subscribe({
      next: (res) => {
        this.listaTotalPendientes = res.pen;
        this.listaPendientes = res.pen;
        this.listaTotalPendientesSecundarios = res.pens;
        this.listaPendientesSecundarios = res.pens;
      },
      complete: () => {
        this.loading = false;
      },
    });
  }
  filtrar() {
    this.listaPendientes = this.listaTotalPendientes;
    this.listaPendientes = this.filtro.filtroData(
      this.listaPendientes,
      this.campoFiltro.nro_orden,
      2
    );
    this.listaPendientes = this.filtro.filtroData(
      this.listaPendientes,
      this.campoFiltro.glosa,
      3
    );
    this.listaPendientes = this.filtro.filtroData(
      this.listaPendientes,
      this.campoFiltro.prov,
      4
    );
    this.listaPendientes = this.filtro.filtroData(
      this.listaPendientes,
      this.campoFiltro.obs,
      5
    );
    this.listaPendientes = this.filtro.filtroData(
      this.listaPendientes,
      this.campoFiltro.nro_guia,
      6
    );
    this.listaPendientes = this.filtro.filtroData(
      this.listaPendientes,
      this.campoFiltro.tipo_mvto,
      7
    );
    ////
    this.listaPendientesSecundarios = this.listaTotalPendientesSecundarios;
    this.listaPendientesSecundarios = this.filtro.filtroData(
      this.listaPendientesSecundarios,
      this.campoFiltro.observaciones,
      1
    );
    this.listaPendientesSecundarios = this.filtro.filtroData(
      this.listaPendientesSecundarios,
      this.campoFiltro.glosa_secun,
      2
    );
    this.listaPendientesSecundarios = this.filtro.filtroData(
      this.listaPendientesSecundarios,
      this.campoFiltro.solicitante,
      3
    );
    this.listaPendientesSecundarios = this.filtro.filtroData(
      this.listaPendientesSecundarios,
      this.campoFiltro.recepcionista,
      4
    );
    this.listaPendientesSecundarios = this.filtro.filtroData(
      this.listaPendientesSecundarios,
      this.campoFiltro.fecha_recep,
      5
    );
    this.listaPendientesSecundarios = this.filtro.filtroData(
      this.listaPendientesSecundarios,
      this.campoFiltro.articulo,
      6
    );
    this.listaPendientesSecundarios = this.filtro.filtroData(
      this.listaPendientesSecundarios,
      this.campoFiltro.cantidad,
      7
    );
    this.listaPendientesSecundarios = this.filtro.filtroData(
      this.listaPendientesSecundarios,
      this.campoFiltro.alm_origen,
      8
    );
    this.listaPendientesSecundarios = this.filtro.filtroData(
      this.listaPendientesSecundarios,
      this.campoFiltro.estado_nota,
      11
    );
  }
  mostrarDetalles(modal, registro) {
    this.abrirModal(modal);
    this.cargarDetalleMovimiento(registro);
    this.movimientoActual = registro.id;
    this.bienesActuales = registro.bienes;
  }
  abrirModal(modal, sz: "lg" | "sm" = "lg") {
    this.modal.open(modal, {
      backdrop: "static",
      keyboard: false,
      size: sz,
    });
  }
  cargarDetalleMovimiento(movimiento) {
    this.fechaDetalle = moment().format("YYYY-MM-DD").toString();
    this.loadingDetalle = true;
    this.detallesMovimiento = [];
    this.query.getDatosApi(this.rutas.select_detalle + movimiento.id).subscribe(
      (res: any) => {
        res.map((item) => {
          item.cant = parseFloat(item.cant).toFixed(2);
          item.precio = parseFloat(item.precio).toFixed(2);
          item.saldo = parseFloat(item.saldo).toFixed(2);
          item.pendiente = parseFloat(item.pendiente).toFixed(2);
        });
        this.detallesMovimiento = res;

        // this.detallesMovimiento.forEach((item, key) => {
        //   item.saldo = this.bienesActuales[key].saldo;
        // })
        this.loadingDetalle = false;
      },
      (error) => {
        this.loading = false;
        console.log(error);
      }
    );
  }
  aceptarMovimiento() {
    this.loadingDetalle = true;
    const data = {
      movimiento: this.movimientoActual,
      fecha: this.fechaDetalle,
      lugar: this.lugarTodo.lugar_id,
    };
    this.query.saveDatosApi(this.rutas.aceptar_movimiento, data).subscribe({
      next: (res: any) => {
        if (res.error) {
          this.toast.error(res.msg, "PROBLEMAS AL ACEPTAR");
        } else {
          this.toast.success(res.msg, "OPERACION COMPLETADA");
          this.modal.dismissAll();
        }
      },
      complete: () => {
        this.loadingDetalle = false;
        this.cargarMovimientosPendientes({ lugar: this.lugarTodo.lugar_id });
      },
      error: (error) => {
        Swal.fire(error.error.tittle, error.error.msg, "error");
        this.loadingDetalle = false;
      },
    });
  }
}
