interface Bien {
  anio: string;
  articulo: string;
  bien_id: number;
  nro_orden: string;
  saldo: string;
  uni_med: string;
}
interface BienSeleccionado {
  bien: Bien;
  cantidad: number;
}
export class ListaBienesMovimiento {
  private bienes: Bien[];
  private listaTotalBienes: Bien[];
  private listaSeleccionada: BienSeleccionado[];
  constructor(listaBienes) {
    this.bienes = listaBienes;
    this.listaTotalBienes = listaBienes;
    this.listaSeleccionada = [];
  }
  agregarBien(id) {
    const itemOld = this.listaSeleccionada.find(
      (item) => item.bien.bien_id === id
    );
    if (itemOld === undefined) {
      const item = this.bienes.find((item) => item.bien_id === id);
      const itemSeleccionado: BienSeleccionado = {
        bien: item,
        cantidad: parseFloat(item.saldo),
      };
      this.listaSeleccionada.push(itemSeleccionado);
    }
  }
  cambiarCantidad(id, cantidad) {
    this.listaSeleccionada.forEach((item) => {
      if (item.bien.bien_id === id) {
        item.cantidad = cantidad;
      }
    });
  }
  eliminarBien(id) {
    let index = this.listaSeleccionada.findIndex(
      (item) => item.bien.bien_id === id
    );
    this.listaSeleccionada.splice(index, 1);
  }
  mostrarLista() {
    return this.listaSeleccionada;
  }
  totalItems() {
    return this.listaSeleccionada.length;
  }
}
