export const environment = {
  production: false,
  rutas:{
    // RUTAS PARA MANCHEGO
    backEnd : 'http://200.60.83.163/api/almacen', // 200.60.83.163
    auth : 'http://200.60.83.163/api',
    recursos: 'http://200.60.83.163/api/almacen/archivos?src=',
    creport:'http://200.60.83.163',
    asincronico: 'http://200.60.83.163'
    // RUTAS ING WALTER
    // backEnd : 'http://localhost/UNAM_back/public/api/almacen',
    // auth : 'http://localhost/UNAM_back/public/api',
    // recursos: 'http://localhost/UNAM_back/public',
    // creport:'http://localhost/UNAM_back/public',
    // asincronico: 'http://localhost/UNAM_back/public'
  },
  aplicacion:{
    modulo:'120', // 104
    nombre:'MÓDULO DE ALMACENES',
    version: '1.0.0'
  },
  entidad:{
    id:'1',
    secFuncional: '001230',
    nombre:'UNIVERSIDAD NACIONAL DE MOQUEGUA',
    abrev : 'UNAM'
  },
  // urlAPI:'',
  agora: {
    appId: '035272aad85b4990acc731e512d8badd'
  },
  paginacion:{
    pagInicial: 1,
    tamañoPag: 10

  }

};
