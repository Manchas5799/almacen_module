// This file can be replaced during build by using the `fileReplacements` array.
// `ng build --prod` replaces `environment.ts` with `environment.prod.ts`.
// The list of file replacements can be found in `angular.json`.

export const environment = {
  production: false,
  rutas:{
    // RUTAS PARA MANCHEGO
    backEnd : 'http://backsigeun.test/api/almacen',
    auth : 'http://backsigeun.test/api',
    recursos: 'http://backsigeun.test/api/almacen/archivos?src=',
    creport:'http://backsigeun.test',
    asincronico: 'http://backsigeun.test'
    // RUTAS ING WALTER
    // backEnd : 'http://localhost/UNAM_back/public/api/almacen',
    // auth : 'http://localhost/UNAM_back/public/api',
    // recursos: 'http://localhost/UNAM_back/public',
    // creport:'http://localhost/UNAM_back/public',
    // asincronico: 'http://localhost/UNAM_back/public'
  },
  aplicacion:{
    modulo:'120', // 104
    nombre:'MÓDULO DE ALMACENES',
    version: '1.0.0'
  },
  entidad:{
    id:'1',
    secFuncional: '001230',
    nombre:'UNIVERSIDAD NACIONAL DE MOQUEGUA',
    abrev : 'UNAM'
  },
  // urlAPI:'',
  agora: {
    appId: '035272aad85b4990acc731e512d8badd'
  },
  paginacion:{
    pagInicial: 1,
    tamañoPag: 10

  }
 
};

